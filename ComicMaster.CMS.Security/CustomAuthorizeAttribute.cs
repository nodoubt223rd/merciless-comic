﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Security
{
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        private readonly IRoleService _roleService;        

        public CustomAuthorizeAttribute()
            :this(new RoleService())
        {            
        }

        public CustomAuthorizeAttribute(IRoleService roleService)
        {
            _roleService = roleService;
        }

        protected virtual CustomPrincipal CurrentUser
        {
            get { return HttpContext.Current.User as CustomPrincipal; }
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.Request.IsAuthenticated)
            {
                if (CurrentUser.IsInRole("Anonymous"))
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
                }

            }
        }
    }
}
