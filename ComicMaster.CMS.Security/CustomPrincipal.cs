﻿using System;
using System.Linq;
using System.Security.Principal;

namespace ComicMaster.CMS.Security
{
    public class CustomPrincipal : IPrincipal
    {
        public string FirstName { get; set; }
        public IIdentity Identity { get; private set;}
        public bool IsInRole(string role)
        {
            if (roles.Any(r => role.Contains(r)))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string LastName { get; set; }
        public int UserId { get; set; }
        public string[] roles { get; set; }
    }
}
