﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class SearchService : ISearchService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public dtoSearchResults SearchContet(string queryTerms)
        {
            try
            {                                

                using (ComicEntities context = new ComicEntities())
                {
                    var blogResults = context.Posts.Where(b => b.Title.Contains(queryTerms) || b.Title == queryTerms || b.Title.Contains(queryTerms) || b.LongDescription.Contains(queryTerms) || b.ShortDescription.Contains(queryTerms)).OrderByDescending(b => b.PostedOn).ToDTOs();

                    var comicResults = context.Comics.Where(c => c.Title.Contains(queryTerms) || c.Title == queryTerms || c.LongDescription.Contains(queryTerms) || c.ShortDescription.Contains(queryTerms)).OrderByDescending(c => c.ComicID).ToDTOs();

                    var galleryResults = context.MediaLibraries.Where(g => g.Name == queryTerms || g.Name.Contains(queryTerms) || g.Author == queryTerms || g.Author.Contains(queryTerms)).OrderByDescending(g => g.Name).ToDTOs();

                    dtoSearchResults results = new dtoSearchResults()
                        {
                            BlogResults = blogResults,
                            ComicResults = comicResults,
                            MediaResults = galleryResults
                        };

                    return results; 
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: ", e);
                return null;
            }
        }
    }
}
