﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class ComicCastService : IService<dtoComicCast, ComicCast>, IComicCastService
    {
        private readonly IObjectSet<ComicCast> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<ComicCast> ObjectSet
        {
            get { return _objectSet;}
        }

        public ComicCastService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ComicCastService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<ComicCast>();
        }

        public void Add(dtoComicCast entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new Comic Cast - " + entity.Comic.Title + " ", e);
            }
        }

        public string AddNewCast(dtoComicCast entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.ComicCasts.Add(entity.ToEntity());

                    context.SaveChanges();
                }

                return Constants.Success;
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new cast member for comic id " + entity.ComicID.ToString() + " - ", e);

                return Constants.Fail;
            }
        }

        public void Attach(dtoComicCast entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new ComicCast - " + entity.Comic.Title + " ", e);
            }
        }

        public long Count(Expression<Func<ComicCast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the ComicCast count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the ComicCast count - ", e);
                return -1;
            }
        }

        public void Delete(dtoComicCast entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select ComicCast - ", e);
            }
        }

        public bool DeleteRetBool(dtoComicCast entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
                return true;
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Removing the character from the comic - ", e);
                return false;
            }
        }

        public IList<dtoComicCast> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the ComicCasts list - ", e);
                return null;
            }
        }

        public IList<dtoComicCast> GetAll(Expression<Func<ComicCast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of ComicCasts by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComicCast> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of ComicCasts - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComicCast> GetAllEnumerated(Expression<Func<ComicCast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of ComicCasts - ", e);
                return null;
            }
        }

        public IQueryable<dtoComicCast> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of ComicCasts - ", e);
                return null;
            }
        }

        public dtoComicCast Single(Expression<Func<ComicCast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected ComicCast - ", e);
                return null;
            }
        }
    }
}
