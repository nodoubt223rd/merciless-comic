﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class SiteSettingService : IService<dtoSiteSetting, SiteSetting>, ISiteSettingService
    {
        private readonly IObjectSet<SiteSetting> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<SiteSetting> ObjectSet
        {
            get { return _objectSet; }
        }

        public SiteSettingService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public SiteSettingService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<SiteSetting>();
        }

        public void Add(dtoSiteSetting entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new site setting - " , e);
            }
        }

        public void Attach(dtoSiteSetting entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new site setting - ", e);
            }
        }

        public long Count(Expression<Func<SiteSetting, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the site settings count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the site setting count - ", e);
                return -1;
            }
        }

        public void Delete(dtoSiteSetting entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected site setting - ", e);
            }
        }

        public IList<dtoSiteSetting> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of site settings for the associated site - ", e);
                return null;
            }
        }

        public IList<dtoSiteSetting> GetAll(Expression<Func<SiteSetting, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of site settings for the site specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoSiteSetting> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of site settings for the specified site - ", e);
                return null;
            }
        }

        public IEnumerable<dtoSiteSetting> GetAllEnumerated(Expression<Func<SiteSetting, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of site settings for the specified site - ", e);
                return null;
            }
        }

        public IQueryable<dtoSiteSetting> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of site settings for the specified site - ", e);
                return null;
            }
        }

        public dtoSiteSetting Single(Expression<Func<SiteSetting, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected site setting for the specified site - ", e);
                return null;
            }
        }
    }
}
