﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class MediaTypeService : IService<dtoMediaType, MediaType>, IMediaTypeService
    {
        private readonly IObjectSet<MediaType> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<MediaType> ObjectSet
        {
            get { return _objectSet; }
        }
        
        public MediaTypeService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public MediaTypeService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<MediaType>();
        }
        

        public void Add(dtoMediaType entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the media type - ", e);
            }
        }

        public bool AddRetValue(dtoMediaType model)
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    model.Description = model.Description;
                    model.Slug = Slugify.SeoFriendly(true, model.Description);

                    context.MediaTypes.Add(model.ToEntity());
                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the media type - ", e);

                return false;
            }
        }

        public void Attach(dtoMediaType entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the media type - ", e);
            }
        }

        public long Count(Expression<Func<MediaType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the media type count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the media type count - ", e);
                return -1;
            }
        }

        public void Delete(dtoMediaType entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected media type - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    MediaType mediaType = context.MediaTypes.Where(mt => mt.TypeId == id).First();

                    var used = context.MediaLibraries.Where(l => l.MediaType == id).ToList();
                    var misc = context.MediaTypes.Where(mt => mt.TypeId == 1).First();
                    
                    if (used.Count > 0)
                    {
                        foreach (var item in used)
                        {
                            item.MediaType = misc.TypeId;
                            context.SaveChanges();
                        }
                    }

                    context.MediaTypes.Remove(mediaType);
                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the media type - ", e);
                return false;
            }
        }

        public IList<dtoMediaType> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of media types - ", e);
                return null;
            }
        }

        public IList<dtoMediaType> GetAll(Expression<Func<MediaType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of media types specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoMediaType> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of media types - ", e);
                return null;
            }
        }

        public IEnumerable<dtoMediaType> GetAllEnumerated(Expression<Func<MediaType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of media types - ", e);
                return null;
            }
        }

        public IQueryable<dtoMediaType> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of media types - ", e);
                return null;
            }
        }

        public IList<dtoMediaType> MediaTypes(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var mediaTypes = context.MediaTypes.Where(mt => mt.TypeId == id).ToDTOs();

                    foreach (var type in mediaTypes)
                    {
                        type.MediaItems = new List<dtoMediaLibrary>();

                        List<dtoMediaLibrary> items = context.MediaLibraries.Where(ml => ml.MediaId == type.TypeId).ToDTOs();

                        foreach (var item in items)
                        {
                            type.MediaItems.Add(item);
                        }
                    }

                    return mediaTypes;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of media types - ", e);
                return null;
            }
        }

        public dtoMediaType Single(Expression<Func<MediaType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected media type - ", e);
                return null;
            }
        }

        public bool Update(dtoMediaType model)
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    var updated = context.MediaTypes.Where(mt => mt.TypeId == model.TypeId).First();

                    if (updated.Slug == model.Slug && updated.Description != model.Description)
                        updated.Slug = Slugify.SeoFriendly(true, model.Description);

                    if (updated.Slug != model.Slug && updated.Description == model.Description)
                        updated.Slug = Slugify.SeoFriendly(true, model.Slug);
                    
                    if(updated.Slug != model.Slug && updated.Description != model.Description)
                        updated.Slug = Slugify.SeoFriendly(true, model.Slug);

                    updated.Description = model.Description;
                    

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: updating the media type - ", e);
                return false;
            }
        }
    }
}
