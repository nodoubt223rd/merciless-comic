﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class BookChapterService : IService<dtoBookChapter, BookChapter>, IBookChapterService
    {
        private readonly IObjectSet<BookChapter> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<BookChapter> ObjectSet
        {
            get { return _objectSet;}
        }

        public BookChapterService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BookChapterService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<BookChapter>();
        }

        public void Add(dtoBookChapter entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new book chapter - " + entity.Book.BookTitle + " ", e);
            }
        }

        public void Attach(dtoBookChapter entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new book chapter - " + entity.Book.BookTitle + " ", e);
            }
        }

        public long Count(Expression<Func<BookChapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the book chapter count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the book chapter count - ", e);
                return -1;
            }
        }

        public void Delete(dtoBookChapter entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select book chapter - ", e);
            }
        }

        public IList<dtoBookChapter> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the book chapters list - ", e);
                return null;
            }
        }

        public IList<dtoBookChapter> GetAll(Expression<Func<BookChapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of book chapters by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBookChapter> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of book chapters - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBookChapter> GetAllEnumerated(Expression<Func<BookChapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of book chapters - ", e);
                return null;
            }
        }

        public IQueryable<dtoBookChapter> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of book chapters - ", e);
                return null;
            }
        }

        public dtoBookChapter Single(Expression<Func<BookChapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected book chapter - ", e);
                return null;
            }
        }
    }
}
