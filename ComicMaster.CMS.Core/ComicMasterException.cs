﻿using System;

namespace ComicMaster.CMS.Core
{
    [Serializable]
    public class ComicMasterException : Exception
    {
        public ComicMasterException(string message)
            : base(message)
        {
        }
    }
}
