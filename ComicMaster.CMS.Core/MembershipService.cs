﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Security.Membership;
using ComicMaster.CMS.Common.Utils.Uploads;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Membership;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class MembershipService : IService<dtoaspnet_Membership, aspnet_Membership>, IMembershipService
    {
        private readonly IObjectSet<aspnet_Membership> _objectSet;
        private readonly AuthorService _authorService = new AuthorService();
        private readonly AuthorTypeService _authorTypeService = new AuthorTypeService();
        private readonly UserService _userService = new UserService();
        private readonly ComicService _comicService = new ComicService();
        private readonly RoleService _roleService = new RoleService();

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<aspnet_Membership> ObjectSet
        {
            get { return _objectSet; }
        }

        public MembershipService()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public MembershipService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<aspnet_Membership>();
        }

        public void Add(dtoaspnet_Membership entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new author - ", e);
            }
        }

        public bool AddRetBool(dtoaspnet_Users entity)
        {
            try
            {
                var appInfo = GetApplicationInfo();

                if (appInfo == null)
                {
                    Log.Error("[Error]: An error occurred getting the application information during new member creation");
                    return false;
                }

                entity.ApplicationId = appInfo.ApplicationId;
                entity.aspnet_Membership.ApplicationId = appInfo.ApplicationId;
                entity.aspnet_Membership = PasswordHash.SetNewPassword(entity.aspnet_Membership, entity.aspnet_Membership.Password);
                entity.aspnet_Membership.Author.ApplicationName = appInfo.ApplicationName;
                entity.aspnet_Membership.Author.LoweredApplicationName = appInfo.LoweredApplicationName;
                entity.aspnet_Membership.Author.Avatar = Constants.DefaultAvatar;
                entity.aspnet_Membership.Author.UseGravatar = false;

                entity.aspnet_Membership.AuthorId = _authorService.AddRetValue(entity.aspnet_Membership.Author);

                if (entity.aspnet_Membership.AuthorId == 0)
                {
                    Log.Info("[Info]: The author id was returned as 0, please check to logs to find out the issue.");
                    return false;
                }

                aspnet_Users _entity = entity.ToEntity();

                using(ComicEntities context = new ComicEntities())
                {                    
                    context.aspnet_Users.Add(_entity);
                    context.SaveChanges();
                }

                using (ComicEntities context = new ComicEntities())
                {
                    context.aspnet_Membership.Add(entity.aspnet_Membership.ToEntity());
                    context.SaveChanges();
                }

                using(ComicEntities context = new ComicEntities())
                {
                    foreach(var role in entity.aspnet_UsersInRoles)
                    {
                        context.aspnet_UsersInRoles.Add(role.ToEntity());
                        context.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding new member - ", e);
                return false;
            }
        }

        public IList<dtoaspnet_Membership> Authors()
        {
            try
            {
                IList<dtoaspnet_Membership> authors = GetAll(a => a.IsApproved == true);

                if (authors != null)
                {
                    foreach (var author in authors)
                    {
                        author.aspnet_Users = new dtoaspnet_Users();
                        author.Author = new dtoAuthor();

                        var user = _userService.Single(u => u.UserId == author.UserId);
                        author.Author = _authorService.Single(ui => ui.AuthorID == author.AuthorId);

                        if (user != null && author.Author != null)
                        {                            
                            author.aspnet_Users.UserName = user.UserName;
                            author.Author.FullName = string.Format("{0} {1}", author.FirstName, author.LastName);

                            author.Author.AuthorTypes = new dtoAuthorType();

                            var type = _authorTypeService.Single(at => at.TypeId == author.Author.AuthorType);

                            author.Author.TypeName = type.Description;
                        }
                    }
                }

                return authors;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the full authors listing - ", e);
                return null;
            }
        }

        public void Attach(dtoaspnet_Membership entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the new member - ", e);
            }
        }

        public bool CheckEmail(string email)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    if (!string.IsNullOrEmpty(context.aspnet_Membership.Where(u => u.Email == email).FirstOrDefault().ToString()))
                        return true;
                    else
                        return false;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Retrieving the email - ", e);
                return false;
            }
        }        

        public long Count(Expression<Func<aspnet_Membership, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the membership counts using the specified criteria - ", e);
                return -1;                
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the membership count - ", e);
                return -1;
            }
        }        

        public void Delete(dtoaspnet_Membership entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var author = context.aspnet_Membership.Where(x => x.AuthorId == entity.AuthorId).First();

                    author.IsApproved = false;

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the select member - ", e);
            }
        }

        public bool DeleteRetBool(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var author = context.aspnet_Membership.Where(a => a.AuthorId == id).First();

                    author.IsApproved = false;

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the select member - ", e);
                return false;
            }
        }

        public IList<dtoaspnet_Membership> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of members - ", e);
                return null;
            }
        }

        public IList<dtoaspnet_Membership> GetAll(Expression<Func<aspnet_Membership, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of members by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Membership> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of members - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Membership> GetAllEnumerated(Expression<Func<aspnet_Membership, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of members by the selected criteria. - ", e);
                return null;
            }
        }

        public dtoaspnet_Applications GetApplicationInfo()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    return context.aspnet_Applications.Where(a => a.ApplicationName == Constants.ApplicationName).FirstOrDefault().ToDTO();
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the application information - ", e);
                return null;
            }
        }

        public IQueryable<dtoaspnet_Membership> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of members - ", e);
                return null;
            }
        }

        public string GetUserName(string username)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    return context.aspnet_Users.Where(u => u.UserName == username).FirstOrDefault().ToString();
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Retrieving the user name - ", e);
                return string.Empty;
            }        
        }
        
        public dtoaspnet_Membership Single(Expression<Func<aspnet_Membership, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected member - ", e);
                return null;
            }
        }

        public string UpdateRetString(dtoaspnet_Membership entity, HttpPostedFileBase fileupload) 
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var user = context.aspnet_Membership.Where(a => a.AuthorId == entity.AuthorId).FirstOrDefault();
                    var author = context.Authors.Where(a => a.AuthorID == entity.AuthorId).FirstOrDefault();

                    if (entity.Author.Avatar == null)
                        entity.Author.Avatar = author.Avatar;

                    if (user.Bio != entity.Bio)
                        user.Bio = StringExtensions.CleanCommentBody(entity.Bio);

                    if (user.FirstName != entity.FirstName)
                        user.FirstName = entity.FirstName;

                    if (user.LastName != entity.LastName)
                        user.LastName = entity.LastName;

                    if (user.Email != entity.Email)
                    {
                        user.Email = entity.Email;
                        user.LoweredEmail = entity.Email.ToLowerInvariant();
                    }

                    if (entity.Author.UseGravatar != author.UseGravatar)
                    {
                        if(entity.Author.UseGravatar != false)
                            if (!entity.Author.Avatar.Contains(Constants.GavatarUrl))
                            {
                                var uploadedAvatar = FileUpload.SaveUserGravatar(fileupload, GravatarHash.HashEmailForGravatar(entity.Email));
                                author.Avatar = uploadedAvatar;
                            }                   
                    }
                    else
                    {
                        if (entity.Author.Avatar != author.Avatar && fileupload != null)
                        {
                            var uploadedAvatar = FileUpload.SaveUserAvatar(fileupload);
                            author.Avatar = uploadedAvatar;
                        }
                    }
                    context.SaveChanges();

                    return Constants.Success;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected author - ", e);
                return Constants.Fail;
            }
        }

        public bool ValidateUser(LoginModel login)
        {
            bool isValid = false;
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var user = context.aspnet_Membership
                        .Where(u => u.aspnet_Users.UserName.Equals(login.UserName)
                            && u.aspnet_Applications.ApplicationName.Equals(Constants.ApplicationName)
                            && u.IsLockedOut == false).FirstOrDefault();

                    if (user != null)
                    {
                        if (user.IsApproved)
                        {
                            if (PasswordValidation.ValidatePassword(user.ToDTO(), login.Password))
                                isValid = true;
                        }
                    }
                }
                return isValid;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Obtaining the user information - ", e);
                return isValid;
            }
        }
    }
}
