﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class UserService : IService<dtoaspnet_Users, aspnet_Users>, IUserService
    {
        private readonly IObjectSet<aspnet_Users> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<aspnet_Users> ObjectSet
        {
            get { return _objectSet;}
        }

        public UserService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public UserService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<aspnet_Users>();
        }

        public void Add(dtoaspnet_Users entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new user - " + entity.UserName + " ", e);
            }
        }

        public void Attach(dtoaspnet_Users entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new Book - " + entity.UserName + " ", e);
            }
        }

        public long Count(Expression<Func<aspnet_Users, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the user count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the user count - ", e);
                return -1;
            }
        }

        public void Delete(dtoaspnet_Users entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected user - ", e);
            }
        }

        public IList<dtoaspnet_Users> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the user list - ", e);
                return null;
            }
        }

        public IList<dtoaspnet_Users> GetAll(Expression<Func<aspnet_Users, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of users by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Users> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of users - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Users> GetAllEnumerated(Expression<Func<aspnet_Users, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of users - ", e);
                return null;
            }
        }

        public IQueryable<dtoaspnet_Users> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of users - ", e);
                return null;
            }
        }

        public dtoaspnet_Users Single(Expression<Func<aspnet_Users, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected user - ", e);
                return null;
            }
        }
    }
}
