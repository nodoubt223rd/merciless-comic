﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class ComicTagService : IService<dtoComicTag, ComicTag>, IComicTagService
    {
        private readonly IObjectSet<ComicTag> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<ComicTag> ObjectSet
        {
            get { return _objectSet; }
        }

        public ComicTagService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ComicTagService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<ComicTag>();
        }

        public void Add(dtoComicTag entity) 
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.ComicTags.Add(entity.ToEntity());
                    context.SaveChanges();
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new tag to the selected comic - " , e);
            }
        }

        public void Attach(dtoComicTag entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new tag to the selected comic - ", e);
            }
        }

        public long Count(Expression<Func<ComicTag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the tag count for the selected comic - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the tag count for the selected comic - ", e);
                return -1;
            }
        }

        public void Delete(dtoComicTag entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected tag for the associated comic - ", e);
            }
        }

        public IList<dtoComicTag> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of tags for the associated comic - ", e);
                return null;
            }
        }

        public IList<dtoComicTag> GetAll(Expression<Func<ComicTag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of tags for the comic specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComicTag> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of tags for the specified comic - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComicTag> GetAllEnumerated(Expression<Func<ComicTag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of tags for the specified comic - ", e);
                return null;
            }
        }

        public IQueryable<dtoComicTag> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of tags for the specified comic - ", e);
                return null;
            }
        }

        public dtoComicTag Single(Expression<Func<ComicTag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected tag for the specified comic - ", e);
                return null;
            }
        }
    }
}
