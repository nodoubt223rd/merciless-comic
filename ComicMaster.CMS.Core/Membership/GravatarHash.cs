﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace ComicMaster.CMS.Core.Membership
{
    public static class GravatarHash
    {
        /// <summary>
        /// Hashes an email with MD5.  Suitable for use with Gravatar profile
        /// image urls
        /// </summary>
        /// <param name="email"></param>
        /// <returns>A hashed version of the authors email to be consumed by the Gravatar service</returns>
        public static string HashEmailForGravatar(string email)
        {
            // Create a new instance of the MD5CryptoServiceProvider object.
            MD5 md5Hasher = MD5.Create();

            // Convert the input string to a byte array and compute the hash.  
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(email));

            // Create a new String builder to collect the bytes  
            // and create a string.  
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data  
            // and format each one as a hexadecimal string.  
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();  // Return the hexadecimal string. 
        }
    }
}
