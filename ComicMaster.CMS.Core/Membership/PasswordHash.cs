﻿using System;
using SimpleCrypto;

using ComicMaster.CMS.Core.Models;


namespace ComicMaster.CMS.Core.Membership
{
    public static class PasswordHash
    {
        private static readonly ICryptoService cryptoService = new PBKDF2();

        private const int SALT_SIZE = 32;
        private const int HASH_ITERATIONS = 1000;

        public static dtoaspnet_Membership SetNewPassword(dtoaspnet_Membership user, string newPassword)
        {
            user.Password = user.Password = cryptoService.Compute(newPassword, SALT_SIZE, HASH_ITERATIONS);
            user.PasswordSalt = cryptoService.Salt;
            user.PasswordFormat = 1;
            return user;
        }
    }
}
