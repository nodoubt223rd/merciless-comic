﻿using System;
using SimpleCrypto;

using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Core.Membership
{
    public static class PasswordValidation
    {
        private readonly static ICryptoService cryptoService = new PBKDF2();

        public static bool ValidatePassword(dtoaspnet_Membership user, string password)
        {
            string hashed = cryptoService.Compute(password, user.PasswordSalt);

            return hashed == user.Password;
        }
    }
}
