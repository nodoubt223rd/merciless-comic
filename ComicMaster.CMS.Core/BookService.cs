﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class BookService : IService<dtoBook, Book>, IBookService
    {
        private readonly IObjectSet<Book> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Book> ObjectSet
        {
            get { return _objectSet;}
        }

        public BookService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BookService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Book>();
        }

        public void Add(dtoBook entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new Book - " + entity.BookTitle + " ", e);
            }
        }

        public string AddRetString(dtoBook model)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var book = context.Books.Where(b => b.BookTitle == model.BookTitle).FirstOrDefault();

                    if (book == null)
                    {
                        if (model.BookSlug == null)
                            model.BookSlug = Slugify.SeoFriendly(true, model.BookTitle);
                        else
                            model.BookSlug = Slugify.SeoFriendly(true, model.BookSlug);

                        context.Books.Add(model.ToEntity());
                        context.SaveChanges();

                        return Constants.Success;
                    }
                    else
                    {
                        return Constants.Exists;
                    }
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new book - " + model.BookTitle + " ", e);
                return Constants.Fail;
            }
        }

        public void Attach(dtoBook entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new Book - " + entity.BookTitle + " ", e);
            }
        }

        public long Count(Expression<Func<Book, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Book count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Book count - ", e);
                return -1;
            }
        }

        public void Delete(dtoBook entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected Book - ", e);
            }
        }

        public bool Delete(int bookId)
        {
            bool success = false;

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var _book = context.Books.Where(b => b.BookID == bookId).FirstOrDefault();

                    if (_book != null)
                    {
                        context.Books.Remove(_book);
                        context.SaveChanges();
                        return success = true;
                    }
                    else
                        return success;
                }
            }
            catch(Exception e)
            {
                Log.Error("Error deleting selected book - ", e);
                return success;
            }
        }

        public IList<dtoBook> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Books list - ", e);
                return null;
            }
        }

        public IList<dtoBook> GetAll(Expression<Func<Book, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of Books by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBook> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Books - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBook> GetAllEnumerated(Expression<Func<Book, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Books - ", e);
                return null;
            }
        }

        public IQueryable<dtoBook> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of Books - ", e);
                return null;
            }
        }

        public dtoBook Single(Expression<Func<Book, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected Book - ", e);
                return null;
            }
        }

        public string Update(dtoBook entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var book = context.Books.Where(b => b.BookID == entity.BookID).FirstOrDefault();

                    book.AuthorID = entity.AuthorID;

                    var slugCheck = Slugify.SeoFriendly(true, entity.BookTitle);

                    if (book.BookSlug == entity.BookSlug && book.BookSlug  !=  slugCheck)
                        book.BookSlug = slugCheck;
                    else
                        book.BookSlug = Slugify.SeoFriendly(true, entity.BookSlug);

                    if (!string.IsNullOrEmpty(entity.BookTitle))
                        book.BookTitle = entity.BookTitle;

                    var check = context.Books.Where(b => b.BookTitle == book.BookTitle).FirstOrDefault();

                    if (check == null)
                    {
                        context.SaveChanges();
                        return Constants.Success;
                    }
                    else if ( check != null && check.BookSlug == book.BookSlug)
                    {                        
                        return Constants.Exists;
                    }
                    else
                    {
                        context.SaveChanges();
                        return Constants.Success;
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the selected book - ", e);
                return Constants.Fail;
            }
        }
    }
}
