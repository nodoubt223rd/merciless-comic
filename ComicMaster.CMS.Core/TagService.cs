﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class TagService : IService<dtoTag, Tag>, ITagService
    {
        private readonly IObjectSet<Tag> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Tag> ObjectSet
        {
            get { return _objectSet; }
        }

        public TagService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public TagService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Tag>();
        }

        public bool Add(string newTags)
        {
            string[] tags = newTags.Split(',');

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    foreach (string tag in tags)
                    {
                        Tag _tag = new Tag();

                        _tag.TagCount = 0;
                        _tag.TagName = StringExtensions.CleanText(tag);
                        _tag.TagSlug = Slugify.SeoFriendly(true, tag);

                        context.Tags.Add(_tag);
                        context.SaveChanges();                        
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: creating the new tag(s) - ", e);
                return false;
            }
        }

        public void Add(dtoTag entity) 
        {
            try
            {                
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new tag - " , e);
            }
        }

        public void Attach(dtoTag entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the tag - ", e);
            }
        }

        public long Count(Expression<Func<Tag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the tags count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the tag count - ", e);
                return -1;
            }
        }

        public void Delete(dtoTag entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected tag - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Tag entity = context.Tags.Where(t => t.TagId == id).First();
                    List<PostTagMap> postedTags = context.PostTagMaps.Where(ptm => ptm.TagId == id).ToList();

                    foreach (var tagMap in postedTags)
                    {
                        context.PostTagMaps.Remove(tagMap);
                        context.SaveChanges();
                    }

                    context.Tags.Remove(entity);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected tag - ", e);
                return false;
            }
        }

        public IList<dtoTag> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of tags - ", e);
                return null;
            }
        }

        public IList<dtoTag> GetAll(Expression<Func<Tag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of tags specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoTag> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of tags - ", e);
                return null;
            }
        }

        public IEnumerable<dtoTag> GetAllEnumerated(Expression<Func<Tag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of tags - ", e);
                return null;
            }
        }

        public IQueryable<dtoTag> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of tags - ", e);
                return null;
            }
        }

        public IList<dtoTag> MostUsed()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var tags = context.Tags.ToDTOs();

                    foreach (var tag in tags)
                    {
                        var tagMap = context.PostTagMaps.Where(ptm => ptm.TagId == tag.TagId).ToDTOs();

                        tag.TagCount = tagMap.Count();

                        foreach (var map in tagMap)
                        {
                            tag.Posts = new List<dtoPost>();

                            var post = context.Posts.Where(p => p.Id == map.PostId).FirstOrDefault().ToDTO();

                            tag.Posts.Add(post);
                        }
                    }

                    return tags.OrderByDescending(t =>t.TagCount).Take(5).ToList();
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the tag list - ", e);
                return null;
            }
        }

        public dtoTag Single(Expression<Func<Tag, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected tag - ", e);
                return null;
            }
        }

        public IList<dtoTag> Tags()
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    var tags = context.Tags.ToDTOs();

                    foreach (var tag in tags)
                    {
                        var tagMap = context.PostTagMaps.Where(ptm => ptm.TagId == tag.TagId).ToDTOs();

                        tag.TagCount = tagMap.Count();

                        foreach (var map in tagMap)
                        {
                            tag.Posts = new List<dtoPost>();

                            var post = context.Posts.Where(p => p.Id == map.PostId).FirstOrDefault().ToDTO();
                            var cat = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();

                            post.Category = new dtoCategory()
                            {
                                Description = cat.Description,
                                Id = cat.Id,
                                Name = cat.Name,
                                UrlSlug = cat.UrlSlug,
                                Used = cat.Used
                            };

                            tag.Posts.Add(post);
                        }
                    }

                    return tags;
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the tag list - ", e);
                return null;
            }
        }

        public dtoTag Tag(string name)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var tag = context.Tags.Where(t => t.TagSlug == name).First().ToDTO();
                    var tagMap = context.PostTagMaps.Where(ptm => ptm.TagId == tag.TagId).ToDTOs();

                    tag.TagCount = tagMap.Count();
                    tag.Posts = new List<dtoPost>();

                    foreach (var map in tagMap)
                    {
                        var post = context.Posts.Where(p => p.Id == map.PostId).FirstOrDefault().ToDTO();
                        var cat = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();
                        var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();                        

                        post.Category = new dtoCategory()
                        {
                            Description = cat.Description,
                            Id = cat.Id,
                            Name = cat.Name,
                            UrlSlug = cat.UrlSlug,
                            Used = cat.Used
                        };

                        post.Tags = new List<dtoTag>();

                        foreach (var _map in tagMaps)
                        {
                            var _tag = context.Tags.Where(t => t.TagId == _map.TagId).First().ToDTO();
                            post.Tags.Add(_tag);
                        }

                        tag.Posts.Add(post);
                    }

                    tag.Posts.OrderByDescending(p => p.PostedOn);

                    return tag;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the tag list - ", e);
                return null;
            }
        }

        public IList<dtoTag> TagCloud()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var tags = context.Tags.Where(t => t.TagCount > 0).OrderByDescending(t => t.TagCount).ToDTOs();
                    var startingLevel = 10;

                    for (var i = 0; i < tags.Count; i++)
                    {
                        tags[i].FontSize = "tag-item" + startingLevel;

                        if (startingLevel > 1)
                        {
                            startingLevel--;
                        }
                    }

                    return tags.OrderByDescending(t => t.TagCount).ToList();
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the tag list - ", e);
                return null;
            }
        }

        public bool Update(dtoTag tag)
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    var updated = context.Tags.Where(t => t.TagId == tag.TagId).First();

                    updated.TagName = tag.TagName;
                    updated.TagSlug = Slugify.SeoFriendly(true, tag.TagName);

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the tag - ", e);
                return false;
            }
        }
    }
}
