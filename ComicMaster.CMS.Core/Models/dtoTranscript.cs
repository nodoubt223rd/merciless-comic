using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoTranscript
    {
        [DataMember()]
        public Int32 TranscriptId { get; set; }

        [DataMember()]
        public String ScriptText { get; set; }

        [DataMember()]
        public Int32 ParentComic { get; set; }

        [DataMember()]
        public Int32 AuthorId { get; set; }

        [DataMember()]
        public String Slug { get; set; }

        [DataMember()]
        public dtoComic Comic { get; set; }

        [DataMember()]
        public List<dtoComic> Comics { get; set; }

        [DataMember()]
        public dtoaspnet_Membership Author { get; set; }

        [DataMember()]
        public List<dtoaspnet_Membership> Authors { get; set; }

        public dtoTranscript()
        {
        }

        public dtoTranscript(Int32 transcriptId, String scriptText, Int32 parentComic, Int32 authorId, string slug, dtoComic comic, dtoaspnet_Membership author, List<dtoaspnet_Membership> authors, List<dtoComic> comics)
        {
			this.TranscriptId = transcriptId;
			this.ScriptText = scriptText;
			this.ParentComic = parentComic;
			this.AuthorId = authorId;
            this.Slug = slug;
			this.Comic = comic;
            this.Comics = comics;
            this.Author = author;
            this.Authors = authors;
        }
    }
}
