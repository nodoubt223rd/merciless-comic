using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Roles
    {
        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public Guid RoleId { get; set; }

        [DataMember()]
        public String RoleName { get; set; }

        [DataMember()]
        public String LoweredRoleName { get; set; }

        [DataMember()]
        public String Description { get; set; }

        [DataMember()]
        public dtoaspnet_Applications aspnet_Applications { get; set; }

        [DataMember()]
        public List<dtoaspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }

        public dtoaspnet_Roles()
        {
        }

        public dtoaspnet_Roles(Guid applicationId, Guid roleId, String roleName, String loweredRoleName, String description, dtoaspnet_Applications aspnet_Applications, List<dtoaspnet_UsersInRoles> aspnet_UsersInRoles)
        {
			this.ApplicationId = applicationId;
			this.RoleId = roleId;
			this.RoleName = roleName;
			this.LoweredRoleName = loweredRoleName;
			this.Description = description;
			this.aspnet_Applications = aspnet_Applications;
			this.aspnet_UsersInRoles = aspnet_UsersInRoles;
        }
    }
}
