using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoSiteSetting
    {
        [DataMember()]
        public String ConfigurationKey { get; set; }

        [DataMember()]
        public String Description { get; set; }

        [DataMember()]
        public String Value { get; set; }

        public dtoSiteSetting()
        {
        }

        public dtoSiteSetting(String configurationKey, String description, String value)
        {
			this.ConfigurationKey = configurationKey;
			this.Description = description;
			this.Value = value;
        }
    }
}
