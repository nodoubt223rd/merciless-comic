﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoPostArchive
    {
        [DataMember()]
        public string Month { get; set; }

        [DataMember()]
        public int Year { get; set; }

        [DataMember()]
        public List<dtoPost> Posts { get; set; }

        public dtoPostArchive()
        {            
        }

        public dtoPostArchive(string month, int year, List<dtoPost> posts)
        {
            this.Month = month;
            this.Year = year;
            this.Posts = posts;
        }
    }
}
