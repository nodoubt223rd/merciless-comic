﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoCurrentPage
    {
        [DataMember()]
        public string ComicSlug { get; set; }

        [DataMember()]
        public string ComicTitle { get; set; }

        [DataMember()]
        public int FirstPage { get; set; }

        [DataMember()]
        public string ImageUrl { get; set; }

        public dtoCurrentPage()
        {
        }

        public dtoCurrentPage(string comicSlug, string comicTitle, int firstPage, string imageUrl)
        {
            this.ComicSlug = comicSlug;
            this.ComicTitle = comicTitle;
            this.FirstPage = firstPage;
            this.ImageUrl = imageUrl;
        }
    }
}
