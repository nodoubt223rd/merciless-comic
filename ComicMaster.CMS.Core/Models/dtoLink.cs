using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoLink
    {
        [DataMember()]
        public Int32 LinkId { get; set; }

        [DataMember()]
        public String AlternateName { get; set; }

        [DataMember()]
        public String ExternalUrl { get; set; }

        [DataMember()]
        public String ImagePath { get; set; }

        [DataMember()]
        public Boolean IsActive { get; set; }

        public dtoLink()
        {
        }

        public dtoLink(Int32 linkId, String alternateName, String externalUrl, String imagePath, Boolean isActive)
        {
			this.LinkId = linkId;
			this.AlternateName = alternateName;
			this.ExternalUrl = externalUrl;
			this.ImagePath = imagePath;
			this.IsActive = isActive;
        }
    }
}
