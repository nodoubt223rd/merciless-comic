﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoSearchResults
    {
        [DataMember()]
        public IList<dtoPost> BlogResults { get; set; }

        [DataMember()]
        public IList<dtoComic> ComicResults { get; set; }

        [DataMember()]
        public IList<dtoMediaLibrary> MediaResults { get; set; }

        public dtoSearchResults()
        {
        }

        public dtoSearchResults(IList<dtoPost> blogResults, IList<dtoComic> comicResults, IList<dtoMediaLibrary> mediaResults)
        {
            this.BlogResults = blogResults;
            this.ComicResults = comicResults;
            this.MediaResults = mediaResults;
        }
    }
}
