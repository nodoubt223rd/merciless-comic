using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_PersonalizationAllUsers
    {
        [DataMember()]
        public Guid PathId { get; set; }

        [DataMember()]
        public Byte[] PageSettings { get; set; }

        [DataMember()]
        public DateTime LastUpdatedDate { get; set; }

        [DataMember()]
        public dtoaspnet_Paths aspnet_Paths { get; set; }

        public dtoaspnet_PersonalizationAllUsers()
        {
        }

        public dtoaspnet_PersonalizationAllUsers(Guid pathId, Byte[] pageSettings, DateTime lastUpdatedDate, dtoaspnet_Paths aspnet_Paths)
        {
			this.PathId = pathId;
			this.PageSettings = pageSettings;
			this.LastUpdatedDate = lastUpdatedDate;
			this.aspnet_Paths = aspnet_Paths;
        }
    }
}
