using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result
    {
        [DataMember()]
        public String Column1 { get; set; }

        [DataMember()]
        public String Name { get; set; }

        public dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result()
        {
        }

        public dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result(String column1, String name)
        {
			this.Column1 = column1;
			this.Name = name;
        }
    }
}
