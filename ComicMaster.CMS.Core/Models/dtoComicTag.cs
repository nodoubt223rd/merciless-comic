using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoComicTag
    {
        [DataMember()]
        public Int32 RowId { get; set; }

        [DataMember()]
        public Int32 ComicId { get; set; }

        [DataMember()]
        public Int32 TagId { get; set; }

        public dtoComicTag()
        {
        }

        public dtoComicTag(Int32 rowId, Int32 comicId, Int32 tagId)
        {
			this.RowId = rowId;
			this.ComicId = comicId;
			this.TagId = tagId;
        }
    }
}
