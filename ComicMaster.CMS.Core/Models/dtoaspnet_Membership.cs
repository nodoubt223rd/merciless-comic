using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Membership
    {
        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public Guid UserId { get; set; }

        [DataMember()]
        public Int32 AuthorId { get; set; }

        [DataMember(IsRequired = true)]
        public String Password { get; set; }

        [DataMember()]
        public Int32 PasswordFormat { get; set; }

        [DataMember()]
        public String PasswordSalt { get; set; }

        [DataMember()]
        public String MobilePIN { get; set; }

        [DataMember(IsRequired = true)]
        public String Email { get; set; }

        [DataMember()]
        public String LoweredEmail { get; set; }

        [DataMember()]
        public String PasswordQuestion { get; set; }

        [DataMember()]
        public String PasswordAnswer { get; set; }

        [DataMember(IsRequired = true)]
        public String FirstName { get; set; }

        [DataMember(IsRequired = true)]
        public String LastName { get; set; }

        [DataMember()]
        public Boolean IsApproved { get; set; }

        [DataMember()]
        public Boolean IsLockedOut { get; set; }        

        [DataMember()]
        public String Comment { get; set; }

        [DataMember()]
        public String Bio { get; set; }

        [DataMember]
        public dtoAuthor Author { get; set; }

        [DataMember()]
        public dtoaspnet_Users aspnet_Users { get; set; }

        public List<dtoaspnet_Roles> Aspnet_Roles { get; set; }

        public dtoaspnet_Membership()
        {
        }

        public dtoaspnet_Membership(Guid applicationId, Guid userId, Int32 authorId, String password, Int32 passwordFormat, String passwordSalt, String mobilePIN, String email, String loweredEmail, String passwordQuestion, String passwordAnswer, String firstName, String lastName, Boolean isApproved, Boolean isLockedOut,  String comment, String bio, dtoAuthor author, dtoaspnet_Users aspnet_Users, List<dtoaspnet_Roles> aspnet_roles)
        {
			this.ApplicationId = applicationId;
			this.UserId = userId;
			this.AuthorId = authorId;
			this.Password = password;
			this.PasswordFormat = passwordFormat;
			this.PasswordSalt = passwordSalt;
			this.MobilePIN = mobilePIN;
			this.Email = email;
			this.LoweredEmail = loweredEmail;
			this.PasswordQuestion = passwordQuestion;
			this.PasswordAnswer = passwordAnswer;
			this.FirstName = firstName;
			this.LastName = lastName;
			this.IsApproved = isApproved;
			this.IsLockedOut = isLockedOut;
			this.Comment = comment;
			this.Bio = bio;
            this.Author = author;
			this.aspnet_Users = aspnet_Users;
            this.Aspnet_Roles = aspnet_roles;
        }
    }
}
