using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoPostTagMap
    {
        [DataMember()]
        public Int32 RowId { get; set; }

        [DataMember()]
        public Int32 PostId { get; set; }

        [DataMember()]
        public Int32 TagId { get; set; }

        [DataMember()]
        public dtoPost Post { get; set; }

        public dtoPostTagMap()
        {
        }

        public dtoPostTagMap(Int32 rowId, Int32 postId, Int32 tagId, dtoPost post)
        {
			this.RowId = rowId;
			this.PostId = postId;
			this.TagId = tagId;
			this.Post = post;
        }
    }
}
