using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoCategory
    {
        [DataMember()]
        public Int32 Id { get; set; }

        [DataMember()]
        public String Name { get; set; }

        [DataMember()]
        public String UrlSlug { get; set; }

        [DataMember()]
        public String Description { get; set; }

        [DataMember()]
        public Int32 Used { get; set; }

        [DataMember()]
        public List<dtoPost> Posts { get; set; }

        public dtoCategory()
        {
        }

        public dtoCategory(Int32 id, String name, String urlSlug, String description, Int32 used, List<dtoPost> posts)
        {
			this.Id = id;
			this.Name = name;
			this.UrlSlug = urlSlug;
			this.Description = description;
            this.Used = used;
			this.Posts = posts;
        }
    }
}
