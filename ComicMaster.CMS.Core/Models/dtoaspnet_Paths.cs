using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Paths
    {
        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public Guid PathId { get; set; }

        [DataMember()]
        public String Path { get; set; }

        [DataMember()]
        public String LoweredPath { get; set; }

        [DataMember()]
        public dtoaspnet_Applications aspnet_Applications { get; set; }

        [DataMember()]
        public dtoaspnet_PersonalizationAllUsers aspnet_PersonalizationAllUsers { get; set; }

        [DataMember()]
        public List<dtoaspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser { get; set; }

        public dtoaspnet_Paths()
        {
        }

        public dtoaspnet_Paths(Guid applicationId, Guid pathId, String path, String loweredPath, dtoaspnet_Applications aspnet_Applications, dtoaspnet_PersonalizationAllUsers aspnet_PersonalizationAllUsers, List<dtoaspnet_PersonalizationPerUser> aspnet_PersonalizationPerUser)
        {
			this.ApplicationId = applicationId;
			this.PathId = pathId;
			this.Path = path;
			this.LoweredPath = loweredPath;
			this.aspnet_Applications = aspnet_Applications;
			this.aspnet_PersonalizationAllUsers = aspnet_PersonalizationAllUsers;
			this.aspnet_PersonalizationPerUser = aspnet_PersonalizationPerUser;
        }
    }
}
