﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicMaster.CMS.Core.Models
{
    public class dtoNewComicModel
    {
        public Int32 AuthorId { get; set; }
        public Int32 IllustratorId { get; set; }
        public Int32 WriterId { get; set; }
        public Int32 ChapterId { get; set; }
        public Int32 BookId { get; set; }
        public Boolean IsArchived { get; set; }
        public Boolean IsLastComic { get; set; }
        public Boolean OnHomePage { get; set; }
        public DateTime ComicDate { get; set; }
        public TimeSpan PublishTime { get; set; }
        public String ComicSlug { get; set; }
        public String Transcript { get; set; }

        [Required(ErrorMessage = "A title is required.")]
        [Display(Name = "Comic Title")]
        public String Title { get; set; }

        [Display(Name = "Banner Image")]
        public String BannerImage { get; set; }

        [Required(ErrorMessage = "A comic image is required to post a new comic.")]
        [Display(Name = "Comic Image")]
        public String ComicImage { get; set; }

        [Required(ErrorMessage = "Thumbnail image is required.")]
        [Display(Name = "Thumbnail Image")]
        public String ThumbnailImage { get; set; }

        [Required(ErrorMessage = "Long description is required.")]
        [Display(Name = "Long Description")]
        public String LongDescription { get; set; }

        [Display(Name = "Short Description")]
        public String ShortDescription { get; set; }

        public List<dtoTag> Tags { get; set; }

        public List<dtoChapter> Chapters { get; set; }

        public List<dtoBook> Books { get; set; }

        public List<dtoaspnet_Membership> Authors { get; set;}

        public List<dtoCast> CastMembers { get; set; }

        public dtoNewComicModel()
        {
        }

        public dtoNewComicModel(Int32 authorId, Int32 illustratorId, Int32 writerId, Int32 chapterId, Int32 bookId, Boolean isArchived, Boolean isLastComic, Boolean onHomePage, DateTime comicDate, TimeSpan publishTime, String comicSlug, String transcript, String title, String bannerImage, String comicImage, String thumbnailImage, String longDescription, String shortDescription, List<dtoTag> tags, List<dtoChapter> chapters, List<dtoBook> books, List<dtoaspnet_Membership> authors, List<dtoCast> castMembers)
        {
            this.AuthorId = authorId;
            this.Authors = authors;
            this.BannerImage = bannerImage;
            this.BookId = bookId;
            this.Books = books;
            this.CastMembers = castMembers;
            this.ChapterId = chapterId;
            this.Chapters = chapters;
            this.ComicDate = comicDate;
            this.ComicImage = comicImage;
            this.ComicSlug = comicSlug;
            this.IllustratorId = illustratorId;
            this.IsArchived = isArchived;
            this.IsLastComic = isLastComic;
            this.LongDescription = longDescription;
            this.OnHomePage = onHomePage;
            this.PublishTime = publishTime;
            this.ShortDescription = shortDescription;
            this.Tags = tags;
            this.ThumbnailImage = thumbnailImage;
            this.Title = title;
            this.Transcript = transcript;
            this.WriterId = writerId;
        }
    }    
}
