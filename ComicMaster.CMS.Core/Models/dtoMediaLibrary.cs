﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoMediaLibrary
    {
        [DataMember()]
        public string Author { get; set; }
        [DataMember()]
        public int MediaId { get; set; }

        [DataMember()]
        public int MediaType { get; set; }

        [DataMember()]
        public string Name { get; set; }

        [DataMember()]
        public string Slug { get; set; }

        [DataMember()]
        public string ImagePath { get; set; }

        [DataMember()]
        public string ThumbPath { get; set; }

        [DataMember()]
        public string Extention { get; set; }

        [DataMember()]
        public string Dimensions { get; set; }

        [DataMember()]
        public DateTime Uploaded { get; set; }

        [DataMember()]
        public string GalleryName { get; set; }

        public dtoMediaLibrary()
        {            
        }

        public dtoMediaLibrary(string author, int mediaId, int mediaType, string name, string slug, string imagePath, string thumbPath, string extention, string dimensions, DateTime uploaded, string galleryName)
        {
            this.Author = author;
            this.MediaId = mediaId;
            this.MediaType = mediaType;
            this.Name = name;
            this.Slug = slug;
            this.ImagePath = imagePath;
            this.ThumbPath = thumbPath;
            this.Extention = extention;
            this.Dimensions = dimensions;
            this.Uploaded = uploaded;
            this.GalleryName = galleryName;
        }
    }
}
