﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoAuthorType
    {
        [DataMember()]
        public Int32 TypeID { get; set; }

        [DataMember()]
        public String Description { get; set; }

        public dtoAuthorType()
        {
        }

        public dtoAuthorType(Int32 typeID, string description)
        {
            this.TypeID = typeID;
            this.Description = description;
        }
    }
}
