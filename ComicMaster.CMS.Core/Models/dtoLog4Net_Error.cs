using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoLog4Net_Error
    {
        [DataMember()]
        public Int32 Id { get; set; }

        [DataMember()]
        public DateTime Date { get; set; }

        [DataMember()]
        public String Thread { get; set; }

        [DataMember()]
        public String Level { get; set; }

        [DataMember()]
        public String Logger { get; set; }

        [DataMember()]
        public String Message { get; set; }

        [DataMember()]
        public String Exception { get; set; }

        public dtoLog4Net_Error()
        {
        }

        public dtoLog4Net_Error(Int32 id, DateTime date, String thread, String level, String logger, String message, String exception)
        {
			this.Id = id;
			this.Date = date;
			this.Thread = thread;
			this.Level = level;
			this.Logger = logger;
			this.Message = message;
			this.Exception = exception;
        }
    }
}
