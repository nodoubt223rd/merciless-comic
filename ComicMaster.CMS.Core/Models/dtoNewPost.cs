﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ComicMaster.CMS.Core.Models
{
    public class dtoNewPost
    {
        [Required(ErrorMessage = "A title is required.")]
        [Display(Name = "Title")]
        public string Title { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Post content required.")]
        [Display(Name = "Long Description")]
        public string LongDescription { get; set; }

        [Display(Name = "Meta Description")]
        public String Meta { get; set; }

        public int Category { get; set; }

        public Dictionary<int, int> Tags { get; set; }

        public string Status { get; set; }

        public string Visibility { get; set; }

        public string Publish { get; set; }

        public dtoNewPost()
        {        
        }

        public dtoNewPost(string title, string longDescription, string meta, int category, Dictionary<int, int> tags, string status, string visibility, string publish)
        {
            this.Title = title;
            this.LongDescription = longDescription;
            this.Meta = meta;
            this.Category = category;
            this.Tags = tags;
            this.Status = status;
            this.Visibility = visibility;
            this.Publish = publish;
        }
    }
}
