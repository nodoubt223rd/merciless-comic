using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_UsersInRoles
    {
        [DataMember()]
        public Int32 RowId { get; set; }

        [DataMember()]
        public Guid UserId { get; set; }

        [DataMember()]
        public Guid RoleId { get; set; }

        [DataMember()]
        public dtoaspnet_Roles aspnet_Roles { get; set; }

        public dtoaspnet_UsersInRoles()
        {
        }

        public dtoaspnet_UsersInRoles(Int32 rowId, Guid userId, Guid roleId, dtoaspnet_Roles aspnet_Roles)
        {
			this.RowId = rowId;
			this.UserId = userId;
			this.RoleId = roleId;
			this.aspnet_Roles = aspnet_Roles;
        }
    }
}
