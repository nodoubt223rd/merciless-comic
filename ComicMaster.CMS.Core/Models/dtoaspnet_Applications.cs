using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Applications
    {
        [DataMember()]
        public String ApplicationName { get; set; }

        [DataMember()]
        public String LoweredApplicationName { get; set; }

        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public String Description { get; set; }

        [DataMember()]
        public List<dtoaspnet_Paths> aspnet_Paths { get; set; }

        [DataMember()]
        public List<dtoaspnet_Roles> aspnet_Roles { get; set; }

        public dtoaspnet_Applications()
        {
        }

        public dtoaspnet_Applications(String applicationName, String loweredApplicationName, Guid applicationId, String description, List<dtoaspnet_Paths> aspnet_Paths, List<dtoaspnet_Roles> aspnet_Roles)
        {
			this.ApplicationName = applicationName;
			this.LoweredApplicationName = loweredApplicationName;
			this.ApplicationId = applicationId;
			this.Description = description;
			this.aspnet_Paths = aspnet_Paths;
			this.aspnet_Roles = aspnet_Roles;
        }
    }
}
