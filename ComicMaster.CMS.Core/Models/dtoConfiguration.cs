﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoConfiguration
    {
        [DataMember()]
        public int AdminAvatarSize { get; set; }

        [DataMember()]
        public string Characters { get; set; }

        [DataMember()]
        public string CharacterThumbs { get; set; }

        [DataMember()]
        public string ComicBanner { get; set; }

        [DataMember()]
        public string ComicPath { get; set; }

        [DataMember()]
        public string ComicThumbNailPath { get; set; }

        [DataMember()]
        public string DefaultPostTime { get; set; }

        [DataMember()]
        public string LinkImages { get; set; }

        [DataMember()]
        public string SiteName { get; set; }

        [DataMember()]
        public string SiteSlogan { get; set; }

        [DataMember()]
        public string SiteTheme { get; set; }

        [DataMember()]
        public string SiteVersion { get; set; }

        [DataMember()]
        public string UserAvatar { get; set; }

        public dtoConfiguration()
        {            
        }

        public dtoConfiguration(int adminAvatarSize, string characters, string characterThumbs, string comicBanner, string comicPath, string comicThumbPathNailPath, string defaultPostTime, string linkImages, string siteName, string siteSlogan, string siteTheme, string siteVersion, string userAvatar)
        {
            this.AdminAvatarSize = adminAvatarSize;
            this.Characters = characters;
            this.CharacterThumbs = characterThumbs;
            this.ComicBanner = comicBanner;
            this.ComicPath = comicPath;
            this.ComicThumbNailPath = comicThumbPathNailPath;
            this.DefaultPostTime = defaultPostTime;
            this.LinkImages = linkImages;
            this.SiteName = siteName;
            this.SiteSlogan = siteSlogan;
            this.SiteTheme = siteTheme;
            this.SiteVersion = siteVersion;
            this.UserAvatar = userAvatar;
        }
    }
}
