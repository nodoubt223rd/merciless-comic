using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Users
    {
        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public Guid UserId { get; set; }

        [DataMember(IsRequired = true)]
        public String UserName { get; set; }

        [DataMember()]
        public String LoweredUserName { get; set; }

        [DataMember()]
        public String MobileAlias { get; set; }

        [DataMember()]
        public Boolean IsAnonymous { get; set; }

        [DataMember()]
        public DateTime LastActivityDate { get; set; }

        [DataMember()]
        public dtoaspnet_Membership aspnet_Membership { get; set; }

        [DataMember()]
        public IList<dtoaspnet_UsersInRoles> aspnet_UsersInRoles { get; set; }

        public dtoaspnet_Users()
        {
        }

        public dtoaspnet_Users(Guid applicationId, Guid userId, String userName, String loweredUserName, String mobileAlias, Boolean isAnonymous, DateTime lastActivityDate, dtoaspnet_Membership aspnet_Membership, IList<dtoaspnet_UsersInRoles> aspnet_UsersInRoles)
        {
			this.ApplicationId = applicationId;
			this.UserId = userId;
			this.UserName = userName;
			this.LoweredUserName = loweredUserName;
			this.MobileAlias = mobileAlias;
			this.IsAnonymous = isAnonymous;
			this.LastActivityDate = lastActivityDate;
			this.aspnet_Membership = aspnet_Membership;
            this.aspnet_UsersInRoles = aspnet_UsersInRoles;
        }
    }
}
