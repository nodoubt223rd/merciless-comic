﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract]
    public class dtoBlog
    {
        [DataMember()]
        public bool Active { get; set; }

        [DataMember()]
        public int AuthorId { get; set; }

        [DataMember()]
        public string AuthorName { get; set; }

        [DataMember()]
        public string BlogSlug { get; set; }

        [DataMember()]
        public string BlogTitle { get; set; }         

        [DataMember()]
        public int Id { get; set; }

        [DataMember()]
        public List<dtoPost> Posts { get; set; }

        [DataMember()]
        public int PostCount { get; set; }

        public dtoBlog()
        {        
        }

        public dtoBlog(bool active, int authorId, string authorName, string blogSlug, string blogTitle, int id, List<dtoPost> posts, int postCount)
        {
            this.Active = active;
            this.AuthorId = authorId;
            this.AuthorName = authorName;
            this.BlogSlug = blogSlug;
            this.BlogTitle = blogTitle;
            this.Id = id;
            this.Posts = posts;
            this.PostCount = postCount;
        }
    }
}
