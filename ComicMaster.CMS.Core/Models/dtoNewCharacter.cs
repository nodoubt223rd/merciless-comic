﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ComicMaster.CMS.Core.Models
{
    public class dtoNewCharacter
    {
        public string Bio { get; set; }

        [Display(Name = "Blood Type")]
        public string BloodType { get; set; }

        public string Bust { get; set; }

        public List<dtoCast> CastMembers { get; set; }

        [Required(ErrorMessage = "The characters first name is required.")]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }

        public string Gender { get; set; }

        public string Height { get; set; }

        public string Hips { get; set; }

        [Required(ErrorMessage = "The characters last name is required.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        public string Slug { get; set; }

        public string Waist { get; set; }

        public string Weight { get; set; }

        public List<dtoBloodType> BloodTypes { get; set; }

        public string Updated { get; set; }

        public string Success { get; set; }

        public dtoNewCharacter()
        {
        }

        public dtoNewCharacter(string bio, string bloodType, List<dtoBloodType> bloodTypes, string bust, List<dtoCast> castMembers, string firstName, string gender, string height, string hips, string lastName, string slug, string waist, string weight, string updated, string success)
        {
            this.Bio = bio;
            this.BloodType = bloodType;
            this.BloodTypes = bloodTypes;
            this.Bust = bust;
            this.CastMembers = castMembers;
            this.FirstName = firstName;
            this.Gender = gender;
            this.Height = height;
            this.Hips = hips;
            this.LastName = lastName;
            this.Slug = slug;
            this.Waist = waist;
            this.Weight = weight;
            this.Updated = updated;
            this.Success = success;
        }
    }
}
