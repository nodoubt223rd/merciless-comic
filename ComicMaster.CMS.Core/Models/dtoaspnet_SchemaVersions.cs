using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_SchemaVersions
    {
        [DataMember()]
        public String Feature { get; set; }

        [DataMember()]
        public String CompatibleSchemaVersion { get; set; }

        [DataMember()]
        public Boolean IsCurrentVersion { get; set; }

        public dtoaspnet_SchemaVersions()
        {
        }

        public dtoaspnet_SchemaVersions(String feature, String compatibleSchemaVersion, Boolean isCurrentVersion)
        {
			this.Feature = feature;
			this.CompatibleSchemaVersion = compatibleSchemaVersion;
			this.IsCurrentVersion = isCurrentVersion;
        }
    }
}
