using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_PersonalizationPerUser
    {
        [DataMember()]
        public Guid Id { get; set; }

        [DataMember()]
        public Nullable<Guid> PathId { get; set; }

        [DataMember()]
        public Nullable<Guid> UserId { get; set; }

        [DataMember()]
        public Byte[] PageSettings { get; set; }

        [DataMember()]
        public DateTime LastUpdatedDate { get; set; }

        [DataMember()]
        public dtoaspnet_Paths aspnet_Paths { get; set; }

        public dtoaspnet_PersonalizationPerUser()
        {
        }

        public dtoaspnet_PersonalizationPerUser(Guid id, Nullable<Guid> pathId, Nullable<Guid> userId, Byte[] pageSettings, DateTime lastUpdatedDate, dtoaspnet_Paths aspnet_Paths)
        {
			this.Id = id;
			this.PathId = pathId;
			this.UserId = userId;
			this.PageSettings = pageSettings;
			this.LastUpdatedDate = lastUpdatedDate;
			this.aspnet_Paths = aspnet_Paths;
        }
    }
}
