using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Membership_GetUserByName_Result
    {
        [DataMember()]
        public String Email { get; set; }

        [DataMember()]
        public String PasswordQuestion { get; set; }

        [DataMember()]
        public String Comment { get; set; }

        [DataMember()]
        public Boolean IsApproved { get; set; }

        [DataMember()]
        public DateTime CreateDate { get; set; }

        [DataMember()]
        public DateTime LastLoginDate { get; set; }

        [DataMember()]
        public DateTime LastActivityDate { get; set; }

        [DataMember()]
        public DateTime LastPasswordChangedDate { get; set; }

        [DataMember()]
        public Guid UserId { get; set; }

        [DataMember()]
        public Boolean IsLockedOut { get; set; }

        [DataMember()]
        public DateTime LastLockoutDate { get; set; }

        public dtoaspnet_Membership_GetUserByName_Result()
        {
        }

        public dtoaspnet_Membership_GetUserByName_Result(String email, String passwordQuestion, String comment, Boolean isApproved, DateTime createDate, DateTime lastLoginDate, DateTime lastActivityDate, DateTime lastPasswordChangedDate, Guid userId, Boolean isLockedOut, DateTime lastLockoutDate)
        {
			this.Email = email;
			this.PasswordQuestion = passwordQuestion;
			this.Comment = comment;
			this.IsApproved = isApproved;
			this.CreateDate = createDate;
			this.LastLoginDate = lastLoginDate;
			this.LastActivityDate = lastActivityDate;
			this.LastPasswordChangedDate = lastPasswordChangedDate;
			this.UserId = userId;
			this.IsLockedOut = isLockedOut;
			this.LastLockoutDate = lastLockoutDate;
        }
    }
}
