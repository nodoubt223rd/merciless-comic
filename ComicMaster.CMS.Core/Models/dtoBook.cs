using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoBook
    {
        [DataMember()]
        public Int32 BookID { get; set; }

        [DataMember(IsRequired = true)]
        public Int32 AuthorID { get; set; }

        [DataMember(IsRequired = true)]
        public String BookTitle { get; set; }

        [DataMember()]
        public String BookSlug { get; set; }

        [DataMember()]
        public List<dtoBookChapter> BookChapters { get; set; }

        public dtoBook()
        {
        }

        public dtoBook(Int32 bookID, Int32 authorID, String bookTitle, String bookSlug, List<dtoBookChapter> bookChapters)
        {
			this.BookID = bookID;
            this.AuthorID = authorID;
			this.BookTitle = bookTitle;
            this.BookSlug = bookSlug;
			this.BookChapters = bookChapters;
        }
    }
}
