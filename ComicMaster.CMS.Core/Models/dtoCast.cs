using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoCast
    {
        [DataMember()]
        public Int32 CastID { get; set; }

        [DataMember()]
        public String Bio { get; set; }

        [DataMember()]
        public Nullable<Int32> Bust { get; set; }

        [DataMember()]
        public Nullable<Int32> Height { get; set; }

        [DataMember()]
        public Nullable<Int32> Hips { get; set; }

        [DataMember()]
        public Nullable<Int32> Waist { get; set; }

        [DataMember()]
        public Nullable<Int32> Weight { get; set; }

        [DataMember()]
        public String Sex { get; set; }

        [DataMember()]
        public String BloodType { get; set; }

        [DataMember(IsRequired = true)]
        public String FirstName { get; set; }

        [DataMember()]
        public String LastName { get; set; }

        [DataMember()]
        public String CastImage { get; set; }

        [DataMember()]
        public String ThumbnailImage { get; set; }

        [DataMember()]
        public String CastSlug { get; set; }

        [DataMember()]
        public List<dtoComicCast> ComicCasts { get; set; }

        [DataMember()]
        public List<dtoComic> Apperances { get; set; }

        public dtoCast()
        {
        }

        public dtoCast(Int32 castID, String bio, Nullable<Int32> bust, Nullable<Int32> height, Nullable<Int32> hips, Nullable<Int32> weight, Nullable<Int32> waist, String sex, String bloodType, String firstName, String lastName, String castImage, String thumbnailImage, String castSlug, List<dtoComicCast> comicCasts, List<dtoComic> apperances)
        {
			this.CastID = castID;
			this.Bio = bio;
            this.Bust = bust;
			this.Height = height;
            this.Hips = hips;
			this.Weight = weight;
            this.Waist = waist;
            this.Sex = sex;
			this.BloodType = bloodType;
			this.FirstName = firstName;
			this.LastName = lastName;
            this.CastImage = castImage;
            this.ThumbnailImage = thumbnailImage;
            this.CastSlug = castSlug;
			this.ComicCasts = comicCasts;
            this.Apperances = apperances;
        }
    }
}
