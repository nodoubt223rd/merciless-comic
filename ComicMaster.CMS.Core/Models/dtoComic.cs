using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoComic
    {
        [DataMember()]
        public Int32 ComicID { get; set; }

        [DataMember()]
        public Int32 AuthorID { get; set; }

        [DataMember()]
        public Int32 IllustratorID { get; set; }

        [DataMember()]
        public Int32 WriterID { get; set; }

        [DataMember()]
        public Int32 ChapterID { get; set; }

        [DataMember()]
        public Int32 BookID { get; set; }

        [DataMember(IsRequired = true)]
        public String Title { get; set; }

        [DataMember(IsRequired = true)]
        public String LongDescription { get; set; }

        [DataMember()]
        public String ShortDescription { get; set; }

        [DataMember(IsRequired = true)]
        public String ComicImage { get; set; }

        [DataMember(IsRequired = true)]
        public String ThumbImage { get; set; }

        [DataMember(IsRequired = true)]
        public String BannerImage { get; set; }

        [DataMember()]
        public Boolean IsArchived { get; set; }

        [DataMember()]
        public Boolean IsLastComic { get; set; }

        [DataMember()]
        public Boolean OnHomePage { get; set; }

        [DataMember()]
        public DateTime ComicDate { get; set; }

        [DataMember()]
        public TimeSpan PublishTime { get; set; }

        [DataMember()]
        public dtoAuthor Author { get; set; }

        [DataMember()]
        public string WriterName { get; set; }

        [DataMember()]
        public string IllustratorName { get; set; }

        [DataMember()]
        public dtoChapter Chapter { get; set; }

        [DataMember()]
        public String ComicSlug { get; set; }

        [DataMember()]
        public dtoBook Book { get; set; }

        [DataMember()]
        public List<dtoCast> CastMembers { get; set; }

        [DataMember()]
        public List<dtoCast> Characters { get; set; }

        [DataMember()]
        public List<dtoChapter> Chapters { get; set; }

        [DataMember()]
        public List<dtoComicCast> ComicCasts { get; set; }

        [DataMember()]
        public List<dtoNews> News { get; set; }

        [DataMember()]
        public dtoTranscript Transcript { get; set; }

        [DataMember()]
        public List<dtoComicTag> ComicTags { get; set; }

        [DataMember()]
        public List<dtoTag> Tags { get; set; }

        public dtoComic()
        {
        }

        public dtoComic(Int32 comicID, Int32 authorID, Int32 illustratorID, Int32 writerID, Int32 chapterID, Int32 bookID, String title, String longDescription, String shortDescription, String comicImage, String thumbImage, String bannerImage, Boolean isArchived, Boolean isLastComic, Boolean onHomePage, DateTime comicDate, TimeSpan publishTime, dtoAuthor author, dtoChapter chapter, String comicSlug, dtoBook book, List<dtoCast> castMembers, List<dtoCast> characters, List<dtoChapter> chapters, List<dtoComicCast> comicCasts, List<dtoNews> news, dtoTranscript transcript, List<dtoComicTag> comicTags, List<dtoTag> tags)
        {
			this.ComicID = comicID;
			this.AuthorID = authorID;
            this.IllustratorID = illustratorID;
            this.WriterID = writerID;
			this.ChapterID = chapterID;
			this.BookID = bookID;
			this.Title = title;
			this.LongDescription = longDescription;
			this.ShortDescription = shortDescription;
			this.ComicImage = comicImage;
			this.ThumbImage = thumbImage;
			this.BannerImage = bannerImage;
			this.IsArchived = isArchived;
			this.IsLastComic = isLastComic;
			this.OnHomePage = onHomePage;
			this.ComicDate = comicDate;
            this.PublishTime = publishTime;
			this.Author = author;
			this.Chapter = chapter;
            this.ComicSlug = comicSlug;
            this.Book = book;
            this.CastMembers = castMembers;
            this.Characters = characters;
            this.Chapters = chapters;
			this.ComicCasts = comicCasts;
			this.News = news;
			this.Transcript = transcript;
            this.ComicTags = comicTags;
            this.Tags = tags;
        }
    }
}
