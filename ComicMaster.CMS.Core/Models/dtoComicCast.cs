using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoComicCast
    {
        [DataMember()]
        public Int32 RowID { get; set; }

        [DataMember()]
        public Int32 CastID { get; set; }

        [DataMember()]
        public Int32 ComicID { get; set; }

        [DataMember()]
        public dtoCast Cast { get; set; }

        [DataMember()]
        public dtoComic Comic { get; set; }

        public dtoComicCast()
        {
        }

        public dtoComicCast(Int32 rowID, Int32 castID, Int32 comicID, dtoCast cast, dtoComic comic)
        {
			this.RowID = rowID;
			this.CastID = castID;
			this.ComicID = comicID;
			this.Cast = cast;
			this.Comic = comic;
        }
    }
}
