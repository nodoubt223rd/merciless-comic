using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoBookChapter
    {
        [DataMember()]
        public Int32 RowID { get; set; }

        [DataMember()]
        public Int32 BookID { get; set; }

        [DataMember()]
        public Int32 ChapterID { get; set; }

        [DataMember()]
        public dtoBook Book { get; set; }

        public dtoBookChapter()
        {
        }

        public dtoBookChapter(Int32 rowID, Int32 bookID, Int32 chapterID, dtoBook book)
        {
			this.RowID = rowID;
			this.BookID = bookID;
			this.ChapterID = chapterID;
			this.Book = book;
        }
    }
}
