using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Profile
    {
        [DataMember()]
        public Guid UserId { get; set; }

        [DataMember()]
        public String PropertyNames { get; set; }

        [DataMember()]
        public String PropertyValuesString { get; set; }

        [DataMember()]
        public Byte[] PropertyValuesBinary { get; set; }

        [DataMember()]
        public DateTime LastUpdatedDate { get; set; }

        public dtoaspnet_Profile()
        {
        }

        public dtoaspnet_Profile(Guid userId, String propertyNames, String propertyValuesString, Byte[] propertyValuesBinary, DateTime lastUpdatedDate)
        {
			this.UserId = userId;
			this.PropertyNames = propertyNames;
			this.PropertyValuesString = propertyValuesString;
			this.PropertyValuesBinary = propertyValuesBinary;
			this.LastUpdatedDate = lastUpdatedDate;
        }
    }
}
