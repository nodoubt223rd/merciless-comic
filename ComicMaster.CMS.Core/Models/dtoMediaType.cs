﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoMediaType
    {
        [DataMember()]
        public int TypeId { get; set; }

        [DataMember()]
        public string Description { get; set; }

        [DataMember()]
        public string Slug { get; set; }

        [DataMember()]
        public List<dtoMediaLibrary> MediaItems { get; set; }

        public dtoMediaType()
        {            
        }

        public dtoMediaType(int typeId, string description, string slug, List<dtoMediaLibrary> mediaItems)
        {
            this.TypeId = typeId;
            this.Description = description;
            this.Slug = slug;
            this.MediaItems = mediaItems;
        }
    }
}
