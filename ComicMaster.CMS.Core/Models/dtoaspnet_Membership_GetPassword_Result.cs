using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Membership_GetPassword_Result
    {
        [DataMember()]
        public String Column1 { get; set; }

        [DataMember()]
        public Nullable<Int32> Column2 { get; set; }

        public dtoaspnet_Membership_GetPassword_Result()
        {
        }

        public dtoaspnet_Membership_GetPassword_Result(String column1, Nullable<Int32> column2)
        {
			this.Column1 = column1;
			this.Column2 = column2;
        }
    }
}
