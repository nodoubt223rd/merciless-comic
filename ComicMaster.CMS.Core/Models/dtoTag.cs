using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoTag
    {
        [DataMember()]
        public Int32 TagId { get; set; }

        [DataMember(IsRequired=true)]
        public String TagName { get; set; }

        [DataMember()]
        public string TagSlug { get; set; }

        [DataMember()]
        public Int32 TagCount { get; set; }

        [DataMember()]
        public IList<dtoPost> Posts { get; set; }

        [DataMember()]
        public bool Used { get; set; }

        [DataMember()]
        public string FontSize { get; set; }

        public dtoTag()
        {
        }

        public dtoTag(string fontSize, Int32 tagId, String tagName, String tagSlug, Int32 tagCount, IList<dtoPost> posts, bool used)
        {
            this.FontSize = fontSize;
			this.TagId = tagId;
			this.TagName = tagName;
            this.TagSlug = tagSlug;
            this.TagCount = tagCount;
            this.Posts = posts;
            this.Used = used;
        }
    }
}
