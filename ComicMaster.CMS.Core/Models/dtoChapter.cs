using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoChapter
    {
        [DataMember()]
        public Int32 ChapterID { get; set; }

        [DataMember()]
        public Int32 ChapterNumber { get; set; }

        [DataMember()]
        public String ChapterTitle { get; set; }

        [DataMember()]
        public String ChapterSlug { get; set; }

        [DataMember()]
        public List<dtoComic> Comics { get; set; }

        public dtoChapter()
        {
        }

        public dtoChapter(Int32 chapterID, Int32 chapterNumber, String chapterTitle, String chapterSlug, List<dtoComic> comics)
        {
			this.ChapterID = chapterID;
            this.ChapterNumber = chapterNumber;
			this.ChapterTitle = chapterTitle;
            this.ChapterSlug = chapterSlug;
			this.Comics = comics;
        }
    }
}
