using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_Membership_GetPasswordWithFormat_Result
    {
        [DataMember()]
        public String Column1 { get; set; }

        [DataMember()]
        public Nullable<Int32> Column2 { get; set; }

        [DataMember()]
        public String Column3 { get; set; }

        [DataMember()]
        public Nullable<Int32> Column4 { get; set; }

        [DataMember()]
        public Nullable<Int32> Column5 { get; set; }

        [DataMember()]
        public Nullable<Boolean> Column6 { get; set; }

        [DataMember()]
        public Nullable<DateTime> Column7 { get; set; }

        [DataMember()]
        public Nullable<DateTime> Column8 { get; set; }

        public dtoaspnet_Membership_GetPasswordWithFormat_Result()
        {
        }

        public dtoaspnet_Membership_GetPasswordWithFormat_Result(String column1, Nullable<Int32> column2, String column3, Nullable<Int32> column4, Nullable<Int32> column5, Nullable<Boolean> column6, Nullable<DateTime> column7, Nullable<DateTime> column8)
        {
			this.Column1 = column1;
			this.Column2 = column2;
			this.Column3 = column3;
			this.Column4 = column4;
			this.Column5 = column5;
			this.Column6 = column6;
			this.Column7 = column7;
			this.Column8 = column8;
        }
    }
}
