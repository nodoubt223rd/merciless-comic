﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public class dtoBloodType
    {
        [DataMember()]
        public String Type { get; set; }
    }
}
