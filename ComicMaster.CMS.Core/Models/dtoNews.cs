using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoNews
    {
        [DataMember()]
        public Int32 NewsId { get; set; }

        [DataMember()]
        public Int32 ComicId { get; set; }

        [DataMember()]
        public String Title { get; set; }

        [DataMember()]
        public String Text { get; set; }

        [DataMember()]
        public DateTime NewsDate { get; set; }

        [DataMember()]
        public String NewsSlug { get; set; }

        [DataMember()]
        public dtoComic Comic { get; set; }

        [DataMember()]
        public List<dtoComic> Comics { get; set; }

        public dtoNews()
        {
        }

        public dtoNews(Int32 newsId, Int32 comicId, String title, String text, DateTime newsDate, String newsSlug, dtoComic comic, List<dtoComic> comics)
        {
			this.NewsId = newsId;
			this.ComicId = comicId;
			this.Title = title;
			this.Text = text;
			this.NewsDate = newsDate;
            this.NewsSlug = newsSlug;
			this.Comic = comic;
            this.Comics = comics;
        }
    }
}
