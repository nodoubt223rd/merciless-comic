using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoaspnet_WebEvent_Events
    {
        [DataMember()]
        public String EventId { get; set; }

        [DataMember()]
        public DateTime EventTimeUtc { get; set; }

        [DataMember()]
        public DateTime EventTime { get; set; }

        [DataMember()]
        public String EventType { get; set; }

        [DataMember()]
        public Decimal EventSequence { get; set; }

        [DataMember()]
        public Decimal EventOccurrence { get; set; }

        [DataMember()]
        public Int32 EventCode { get; set; }

        [DataMember()]
        public Int32 EventDetailCode { get; set; }

        [DataMember()]
        public String Message { get; set; }

        [DataMember()]
        public String ApplicationPath { get; set; }

        [DataMember()]
        public String ApplicationVirtualPath { get; set; }

        [DataMember()]
        public String MachineName { get; set; }

        [DataMember()]
        public String RequestUrl { get; set; }

        [DataMember()]
        public String ExceptionType { get; set; }

        [DataMember()]
        public String Details { get; set; }

        public dtoaspnet_WebEvent_Events()
        {
        }

        public dtoaspnet_WebEvent_Events(String eventId, DateTime eventTimeUtc, DateTime eventTime, String eventType, Decimal eventSequence, Decimal eventOccurrence, Int32 eventCode, Int32 eventDetailCode, String message, String applicationPath, String applicationVirtualPath, String machineName, String requestUrl, String exceptionType, String details)
        {
			this.EventId = eventId;
			this.EventTimeUtc = eventTimeUtc;
			this.EventTime = eventTime;
			this.EventType = eventType;
			this.EventSequence = eventSequence;
			this.EventOccurrence = eventOccurrence;
			this.EventCode = eventCode;
			this.EventDetailCode = eventDetailCode;
			this.Message = message;
			this.ApplicationPath = applicationPath;
			this.ApplicationVirtualPath = applicationVirtualPath;
			this.MachineName = machineName;
			this.RequestUrl = requestUrl;
			this.ExceptionType = exceptionType;
			this.Details = details;
        }
    }
}
