﻿using System;
using System.Collections.Generic;

namespace ComicMaster.CMS.Core.Models
{
    public class dtoMenu
    {
        public List<dtoaspnet_Membership> Authors { get; set; }
        public List<dtoNews> Articles { get; set; }
        public List<dtoBlog> Blogs { get; set; } 
        public List<dtoBook> Books { get; set; }
        public List<dtoCast> CastMembers { get; set; }
        public List<dtoCategory> Categories { get; set; }
        public List<dtoChapter> Chapters { get; set; }
        public List<dtoComic> Comics { get; set; }
        public List<dtoLink> Links { get; set; }
        public List<dtoaspnet_Roles> Roles { get; set; }
        public List<dtoTag> Tags { get; set; }
        public List<dtoTranscript> Transcripts { get; set; }
    }
}
