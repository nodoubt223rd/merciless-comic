using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Web.Mvc;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoPost
    {
        [DataMember()]
        public Int32 Id { get; set; }

        [DataMember()]
        public Int32 BlogId { get; set; }

        [DataMember()]
        public String Title { get; set; }

        [DataMember()]
        [AllowHtml]
        public String ShortDescription { get; set; }

        [DataMember()]
        [AllowHtml]
        public String LongDescription { get; set; }

        [DataMember()]
        public String Meta { get; set; }

        [DataMember()]
        public String UrlSlug { get; set; }

        [DataMember()]
        public Nullable<Boolean> Published { get; set; }

        [DataMember()]
        public Nullable<DateTime> PostedOn { get; set; }

        [DataMember()]
        public String Modified { get; set; }

        [DataMember()]
        public Int32 CategoryId { get; set; }

        [DataMember()]
        public dtoCategory Category { get; set; }

        [DataMember()]
        public string Status { get; set; }

        [DataMember()]
        public List<dtoTag> Tags { get; set; }

        [DataMember()]
        public String Visibility { get; set; }

        [DataMember()]
        public String BlogName { get; set; }

        [DataMember()]
        public String BlogSlug { get; set; }

        public dtoPost()
        {
        }

        public dtoPost(Int32 id, Int32 blogId, String title, String shortDescription, String longDescription, String meta, String urlSlug, Nullable<Boolean> published, Nullable<DateTime> postedOn, String modified, Int32 categoryId, dtoCategory category, string status, List<dtoTag> tags, String visibility, String blogName, String blogSlug)
        {
			this.Id = id;
            this.BlogId = blogId;
			this.Title = title;
			this.ShortDescription = shortDescription;
			this.LongDescription = longDescription;
			this.Meta = meta;
			this.UrlSlug = urlSlug;
			this.Published = published;
			this.PostedOn = postedOn;
			this.Modified = modified;
			this.CategoryId = categoryId;
			this.Category = category;
            this.Status = status;
			this.Tags = tags;
            this.Visibility = visibility;
            this.BlogName = blogName;
            this.BlogSlug = blogSlug;
        }
    }
}
