using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Core.Models
{
    [DataContract()]
    public partial class dtoAuthor
    {
        [DataMember()]
        public Int32 AuthorID { get; set; }

        [DataMember()]
        public Guid ApplicationId { get; set; }

        [DataMember()]
        public String ApplicationName { get; set; }

        [DataMember()]
        public String LoweredApplicationName { get; set; }

        [DataMember()]
        public Int32 AuthorType { get; set; }

        [DataMember()]
        public String TypeName { get; set; }

        [DataMember()]
        public String Avatar { get; set; }

        [DataMember()]
        public bool UseGravatar { get; set; }

        [DataMember()]
        public dtoAuthorType AuthorTypes { get; set; }

        [DataMember()]
        public int ComicCount { get; set; }

        [DataMember()]
        public List<dtoComic> Comics { get; set; }

        [DataMember()]
        public List<dtoBook> Books { get; set; }

        [DataMember()]
        public string FullName { get; set; }

        public dtoAuthor()
        {
        }

        public dtoAuthor(Int32 authorID, Guid applicationId, String applicationName, String loweredApplicationName, Int32 authorType, String avatar, bool useGravatar, dtoAuthorType authorTypes, int comicCount, List<dtoComic> comics, List<dtoBook> books, string fullName)
        {
			this.AuthorID = authorID;
			this.ApplicationId = applicationId;
			this.ApplicationName = applicationName;
			this.LoweredApplicationName = loweredApplicationName;
            this.AuthorType = authorType;
            this.Avatar = avatar;
            this.AuthorTypes = authorTypes;
            this.UseGravatar = useGravatar;
            this.ComicCount = comicCount;
			this.Comics = comics;
            this.Books = books;
            this.FullName = fullName;
        }
    }
}
