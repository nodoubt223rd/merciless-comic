﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class ChapterService : IService<dtoChapter, Chapter>, IChapterService
    {
        private readonly IObjectSet<Chapter> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Chapter> ObjectSet
        {
            get { return _objectSet;}
        }

        public ChapterService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ChapterService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Chapter>();
        }

        public void Add(dtoChapter entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new Chapter - " + entity.ChapterTitle + " ", e);
            }
        }

        public string Add(dtoChapter model, int bookId)
        {
            int number = 0;
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Chapter check = context.Chapters.Where(c => c.ChapterTitle == model.ChapterTitle).FirstOrDefault();

                    if (check != null)
                        return Constants.Exists;

                    Chapter chapter = model.ToEntity();
                    Chapter chapterNumber = new Chapter();

                   chapterNumber = context.Chapters.OrderByDescending(c => c.ChapterNumber).FirstOrDefault();

                    if (chapterNumber != null)
                        number = chapterNumber.ChapterNumber;

                    chapter.ChapterNumber = number + 1;

                    if (string.IsNullOrEmpty(model.ChapterSlug))
                        model.ChapterSlug = Slugify.SeoFriendly(true, model.ChapterTitle);
                    else
                        model.ChapterSlug = Slugify.SeoFriendly(true, model.ChapterSlug);

                    context.Chapters.Add(chapter);
                    context.SaveChanges();

                    BookChapter bookChapter = new BookChapter()
                        {
                            BookID = bookId,
                            ChapterID = chapter.ChapterID
                        };

                    context.BookChapters.Add(bookChapter);
                    context.SaveChanges();
                }

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding new chapter - ", e);
                return Constants.Fail;
            }
        }

        public void Attach(dtoChapter entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new Chapter - " + entity.ChapterTitle + " ", e);
            }
        }

        public long Count(Expression<Func<Chapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Chapter count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Chapter count - ", e);
                return -1;
            }
        }

        public void Delete(dtoChapter entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select Chapter - ", e);
            }
        }

        public bool Delete(int id)
        {
            bool success = false;

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var _chapter = context.Chapters.Where(c => c.ChapterID == id).FirstOrDefault();

                    if(_chapter != null)
                    {
                        var bookChapter = context.BookChapters.Where(b => b.ChapterID == id).FirstOrDefault();

                        if (bookChapter != null)
                            context.BookChapters.Remove(bookChapter);

                        context.Chapters.Remove(_chapter);

                        context.SaveChanges();

                        return success = true;

                    }

                    return success;
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected chapter - ", e);
                return success;
            }
        }

        public IList<dtoChapter> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Chapters list - ", e);
                return null;
            }
        }

        public IList<dtoChapter> GetAll(Expression<Func<Chapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of Chapters by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoChapter> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Chapters - ", e);
                return null;
            }
        }

        public IEnumerable<dtoChapter> GetAllEnumerated(Expression<Func<Chapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Chapters - ", e);
                return null;
            }
        }

        public IQueryable<dtoChapter> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of Chapters - ", e);
                return null;
            }
        }

        public dtoChapter Single(Expression<Func<Chapter, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected Chapter - ", e);
                return null;
            }
        }

        public string Update(dtoChapter model, int bookId)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Chapter check = context.Chapters.Where(c => c.ChapterTitle == model.ChapterTitle).FirstOrDefault();
                    Chapter chapter = context.Chapters.Where(c => c.ChapterID == model.ChapterID).First();

                    if (check != null && chapter.ChapterSlug == model.ChapterSlug)
                        return Constants.Exists;

                    
                    BookChapter bookChapter = context.BookChapters.Where(bc => bc.BookID == bookId && bc.ChapterID == model.ChapterID).First();

                    var alteredTitle = Slugify.SeoFriendly(true, model.ChapterTitle);

                    var alteredSlug = string.Empty;

                    if (chapter.ChapterSlug != null)
                        chapter.ChapterSlug = null;

                    if (chapter.ChapterSlug != model.ChapterSlug)
                        alteredSlug = Slugify.SeoFriendly(true, model.ChapterSlug);

                    if (chapter.ChapterSlug == model.ChapterSlug && chapter.ChapterSlug != alteredTitle && string.IsNullOrEmpty(alteredSlug))
                        chapter.ChapterSlug = alteredTitle;
                    else
                        chapter.ChapterSlug = alteredSlug;

                    chapter.ChapterNumber = model.ChapterNumber;
                    chapter.ChapterTitle = model.ChapterTitle;

                    if (bookChapter.BookID != bookId)
                        bookChapter.BookID = bookId;

                    context.SaveChanges();
                }

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the book - ", e);
                return Constants.Fail;
            }
        }
    }
}
