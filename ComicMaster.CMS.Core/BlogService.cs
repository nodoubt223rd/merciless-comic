﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class BlogService : IService<dtoBlog, Blog>, IBlogService
    {
        private readonly IObjectSet<Blog> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Blog> ObjectSet
        {
            get { return _objectSet; }
        }

        public BlogService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BlogService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Blog>();
        }

        public void Add(dtoBlog entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new blog - " + entity.BlogTitle + " ", e);
            }
        }

        public bool AddRetVal(dtoBlog blog)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    blog.BlogTitle = StringExtensions.CleanText(blog.BlogTitle);
                    blog.BlogSlug = Slugify.SeoFriendly(true, blog.BlogTitle);
                    context.Blogs.Add(blog.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new blog - " + blog.BlogTitle + " ", e);
                return false;
            }
        }

        public void Attach(dtoBlog entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the new blog - " + entity.BlogTitle + " ", e);
            }
        }

        public List<dtoBlog> Blogs()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var blogs = context.Blogs.ToDTOs();

                    foreach (var blog in blogs)
                    {
                        blog.Posts = new List<dtoPost>();
                        
                        var posts = context.Posts.Where(p => p.BlogId == blog.Id).ToDTOs();
                        blog.PostCount = posts.Count();

                        foreach (var post in posts)
                        {
                            var category = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();

                            post.Category = new dtoCategory()
                            {
                                Description = category.Description,
                                Id = category.Id,
                                Name = category.Name,
                                UrlSlug = category.UrlSlug,
                                Used = category.Used
                            };

                            var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                            post.Tags = new List<dtoTag>();

                            foreach (var tagMap in tagMaps)
                            {
                                var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                                post.Tags.Add(tag);
                            }

                            blog.Posts.Add(post);
                        }
                    }

                    return blogs;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of blogs - ", e);
                return null;
            }
        }

        public dtoBlog Blog(string name)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var blog = context.Blogs.Where(b => b.BlogSlug == name).First().ToDTO();

                    blog.Posts = new List<dtoPost>();

                    var posts = context.Posts.Where(p => p.BlogId == blog.Id).ToDTOs();
                    blog.PostCount = posts.Count();

                    foreach (var post in posts)
                    {
                        var category = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();

                        post.Category = new dtoCategory()
                        {
                            Description = category.Description,
                            Id = category.Id,
                            Name = category.Name,
                            UrlSlug = category.UrlSlug,
                            Used = category.Used
                        };

                        var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                        post.Tags = new List<dtoTag>();

                        foreach (var tagMap in tagMaps)
                        {
                            var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                            post.Tags.Add(tag);
                        }

                        blog.Posts.Add(post);
                    }

                    return blog;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of blogs - ", e);
                return null;
            }
        }

        public List<dtoBlog> Blogs(int authorId)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var blogs = context.Blogs.Where(b => b.AuthorId == authorId).ToDTOs();

                    foreach (var blog in blogs)
                    {
                        blog.Posts = new List<dtoPost>();

                        var posts = context.Posts.Where(p => p.BlogId == blog.Id);

                        foreach (var post in posts)
                        {
                            blog.Posts.Add(post.ToDTO());
                        }
                    }

                    return blogs;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of blogs by the specified id - ", e);
                return null;
            }
        }

        public long Count(Expression<Func<Blog, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the blog count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the blog count - ", e);
                return -1;
            }
        }

        public void Delete(dtoBlog entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected blog - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Blog blog = context.Blogs.Where(l => l.Id == id).FirstOrDefault();

                    context.Blogs.Remove(blog);

                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected blog - ", e);
                return false;
            }
        }

        public IList<dtoBlog> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the blog list - ", e);
                return null;
            }
        }

        public IList<dtoBlog> GetAll(Expression<Func<Blog, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of blogs by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBlog> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of blogs - ", e);
                return null;
            }
        }

        public IEnumerable<dtoBlog> GetAllEnumerated(Expression<Func<Blog, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of blogs by the specified criteria - ", e);
                return null;
            }
        }

        public IQueryable<dtoBlog> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of blogs - ", e);
                return null;
            }
        }

        public dtoBlog Single(Expression<Func<Blog, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected blog - ", e);
                return null;
            }
        }

        public dtoBlog Single(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var blog = context.Blogs.Where(b => b.Id == id).First().ToDTO();
                    var author = context.aspnet_Membership.Where(a => a.AuthorId == blog.AuthorId).First();
                    var user = context.aspnet_Users.Where(u => u.UserId == author.UserId).First();

                    blog.Posts = new List<dtoPost>();
                    blog.AuthorName = user.UserName;

                    var posts = context.Posts.Where(p => p.BlogId == blog.Id).ToDTOs();
                    blog.PostCount = posts.Count();

                    foreach (var post in posts)
                    {
                        var category = context.Categories.Where(c => c.Id == post.CategoryId).FirstOrDefault().ToDTO();

                        post.Category = new dtoCategory();

                        post.Category.Description = category.Description;
                        post.Category.Id = category.Id;
                        post.Category.Name = category.Name;
                        post.Category.UrlSlug = category.UrlSlug;

                        blog.Posts.Add(post);
                    }

                    return blog;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the of blog for id "+ id +" - ", e);
                return null;
            }
        }

        public bool Update(dtoBlog blog)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Blog _blog = context.Blogs.Where(b => b.Id == blog.Id).FirstOrDefault();
                    
                    if(_blog.BlogSlug == blog.BlogSlug && _blog.BlogTitle != blog.BlogTitle)
                    {
                        _blog.BlogSlug = Slugify.SeoFriendly(true, blog.BlogTitle);
                    }
                    else if (_blog.BlogSlug != blog.BlogSlug && _blog.BlogTitle == blog.BlogTitle)
                    {
                        _blog.BlogSlug = Slugify.SeoFriendly(true, blog.BlogSlug);
                    }
                    else if (_blog.BlogSlug != blog.BlogSlug && _blog.BlogTitle != blog.BlogTitle)
                    {
                        _blog.BlogSlug = Slugify.SeoFriendly(true, blog.BlogSlug);
                    }
                    else
                    {                    
                    }


                    _blog.BlogTitle = blog.BlogTitle;

                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the blog - ", e);
                return false;
            }
        }
    }
}
