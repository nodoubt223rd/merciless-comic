﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Utils.Sorting;
using ComicMaster.CMS.Common.Utils.Uploads;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class CastService : IService<dtoCast, Cast>, ICastService
    {
        private readonly IObjectSet<Cast> _objectSet;
        private readonly ComicCastService _comicCastService = new ComicCastService();

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Cast> ObjectSet
        {
            get { return _objectSet;}
        }

        public CastService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public CastService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Cast>();
        }

        public void Add(dtoCast entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new Cast - " + entity.FirstName + " ", e);
            }
        }

        public string Add(dtoCast dto, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbUpload)
        {
            try
            {
                if (imageUpload != null)
                {
                    var _image = FileUpload.SaveCharacter(imageUpload);
                    dto.CastImage = _image;
                }

                if (thumbUpload != null)
                {
                    var _image = FileUpload.SaveCharacterThumb(thumbUpload);
                    dto.ThumbnailImage = _image;
                }

                if (dto.CastSlug == null)
                {
                    dto.CastSlug = Slugify.SeoFriendly(true, dto.FirstName + " " + dto.LastName);
                }

                var exists = Single(c => c.FirstName == dto.FirstName && c.LastName == dto.LastName);

                if (exists == null)
                {

                    using (ComicEntities context = new ComicEntities())
                    {
                        context.Casts.Add(dto.ToEntity());
                        context.SaveChanges();
                    }

                    return Constants.Success;
                }
                else
                {
                    return Constants.CharacterName;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new cast member - " + dto.FirstName + " " + dto.LastName + " ", e);
                return Constants.Fail;
            }
        }

        public void Attach(dtoCast entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new Cast - " + entity.FirstName + " ", e);
            }
        }

        public long Count(Expression<Func<Cast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Cast count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Cast count - ", e);
                return -1;
            }
        }

        public void Delete(dtoCast entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select Cast - ", e);
            }
        }

        public bool DeleteRetBool(dtoCast entity)
        {
            try
            {
                var apperances = _comicCastService.GetAll(a => a.CastID == entity.CastID);

                bool removeApperances = false;

                try
                {
                    foreach (var apperance in apperances)
                    {
                        _comicCastService.Delete(apperance);
                    }
                    removeApperances = true;
                }
                catch
                {
                }

                if (removeApperances)
                {
                    this.ObjectSet.DeleteObject(entity.ToEntity());
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected character - ", e);
                return false;
            }
        }

        public IList<dtoCast> GetAll() 
        {
            try
            {
                return this.ObjectSet.OrderBy(x => x.LastName).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Casts list - ", e);
                return null;
            }
        }

        public IList<dtoCast> GetAll(string sortOrder)
        {
            try
            {
                return OrderByHelper.OrderBy(this.ObjectSet.ToDTOs(), "sortOrder").ToList();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the Casts list - ", e);
                return null;
            }
        }

        public IList<dtoCast> GetAll(Expression<Func<Cast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of Casts by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoCast> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Casts - ", e);
                return null;
            }
        }

        public IEnumerable<dtoCast> GetAllEnumerated(Expression<Func<Cast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Casts - ", e);
                return null;
            }
        }

        public IQueryable<dtoCast> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of Casts - ", e);
                return null;
            }
        }

        public dtoCast Single(Expression<Func<Cast, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected Cast - ", e);
                return null;
            }
        }

        public string Update(dtoCast dto, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbUpload)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var cast = context.Casts.Where(c => c.CastID == dto.CastID).FirstOrDefault();

                if (cast == null)
                    return Constants.Fail;

                    cast.Bio = dto.Bio;
                    cast.BloodType = dto.BloodType;
                    cast.Bust = dto.Bust;

                    if (imageUpload != null)
                    {
                        var image = FileUpload.SaveCharacter(imageUpload);
                        cast.CastImage = image;
                    }

                    if (cast.CastSlug != dto.CastSlug)
                        cast.CastSlug = Slugify.SeoFriendly(true, dto.CastSlug);

                    cast.FirstName = dto.FirstName;
                    cast.LastName = dto.LastName;
                    cast.Sex = dto.Sex;

                    if (thumbUpload != null)
                    {
                        var thumb = FileUpload.SaveCharacterThumb(thumbUpload);
                        cast.ThumbnailImage = thumb;
                    }

                    cast.Waist = dto.Waist;
                    cast.Weight = dto.Weight;

                    context.SaveChanges();
                }

                return Constants.Success;
            }                
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the new cast member - " + dto.FirstName + " " + dto.LastName + " ", e);
                return Constants.Fail;
            }
        }
    }
}
