﻿using System;
using System.Collections.Generic;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class MenuService :  IMenuService
    {
        private readonly MembershipService _membershipService = new MembershipService();
        private readonly BlogService _blogService = new BlogService();
        private readonly BookService _bookService = new BookService();
        private readonly CastService _castService = new CastService();
        private readonly CategoryService _catergoryService = new CategoryService();
        private readonly ChapterService _chapterService = new ChapterService();
        private readonly ComicService _comicService = new ComicService();
        private readonly LinkService _linkService = new LinkService();
        private readonly NewsService _newsService = new NewsService();
        private readonly RoleService _roleService = new RoleService();
        private readonly TagService _tagService = new TagService();
        private readonly TranscriptService _transcriptService = new TranscriptService();

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public dtoMenu GetMenu()
        {
            try
            {
                dtoMenu mainMenu = new dtoMenu();

                var articles = _newsService.GetAll();
                var authors = _membershipService.GetAll(a => a.IsApproved == true);
                var blogs = _blogService.GetAll();
                var books = _bookService.GetAll();
                var cast = _castService.GetAll();
                var categories = _catergoryService.GetAll();
                var comics = _comicService.GetAll();
                var chapters = _chapterService.GetAll();
                var links = _linkService.GetAll();
                var roles = _roleService.GetAll(r => r.RoleName != "Anonymous");
                var tags = _tagService.GetAll();
                var transcripts = _transcriptService.GetAll();               

                mainMenu.Articles = new List<dtoNews>(articles);
                mainMenu.Authors = new List<dtoaspnet_Membership>(authors);
                mainMenu.Blogs = new List<dtoBlog>(blogs);
                mainMenu.Books = new List<dtoBook>(books);
                mainMenu.CastMembers = new List<dtoCast>(cast);
                mainMenu.Categories = new List<dtoCategory>(categories);
                mainMenu.Chapters = new List<dtoChapter>(chapters);
                mainMenu.Comics = new List<dtoComic>(comics);
                mainMenu.Links = new List<dtoLink>(links);
                mainMenu.Roles = new List<dtoaspnet_Roles>(roles);
                mainMenu.Tags = new List<dtoTag>(tags);
                mainMenu.Transcripts = new List<dtoTranscript>(transcripts);

                foreach(var transcript in mainMenu.Transcripts)
                {
                    var obj = _comicService.Single(c => c.ComicID == transcript.ParentComic);

                    transcript.Comic = new dtoComic()
                    {
                        Title = obj.Title + " Transcript"
                    };
                }

                return mainMenu;
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Generating the main menu - ", e);
                return null;
            }
        }
    }
}
