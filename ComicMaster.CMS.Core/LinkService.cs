﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Utils.Uploads;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class LinkService : IService<dtoLink, Link>, ILinkService
    {
        private readonly IObjectSet<Link> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Link> ObjectSet
        {
            get { return _objectSet; }
        }

        public LinkService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public LinkService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Link>();
        }

        public void Add(dtoLink entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new link - " + entity.AlternateName + " ", e);
            }
        }

        public bool AddRetVal(dtoLink entity, HttpPostedFileBase imagePath)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    entity.ImagePath = FileUpload.SaveLink(imagePath);

                    context.Links.Add(entity.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new link - " + entity.AlternateName + " ", e);
                return false;
            }
        }

        public void Attach(dtoLink entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new link - " + entity.AlternateName + " ", e);
            }
        }

        public long Count(Expression<Func<Link, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the link count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the link count - ", e);
                return -1;
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                     Link link = context.Links.Where(l => l.LinkId == id).FirstOrDefault();

                    context.Links.Remove(link);
                    context.SaveChanges();

                    return true;
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected link - ", e);
                return false;
            }
        }

        public void Delete(dtoLink entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the selected link - ", e);
            }
        }

        public IList<dtoLink> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the links list - ", e);
                return null;
            }
        }

        public IList<dtoLink> GetAll(Expression<Func<Link, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of links by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoLink> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of links - ", e);
                return null;
            }
        }

        public IEnumerable<dtoLink> GetAllEnumerated(Expression<Func<Link, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of links by the specified criteria - ", e);
                return null;
            }
        }

        public IQueryable<dtoLink> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of links - ", e);
                return null;
            }
        }

        public bool Update(dtoLink model)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Link link = context.Links.Where(l => l.LinkId == model.LinkId).FirstOrDefault();

                    link.AlternateName = model.AlternateName;
                    link.ExternalUrl = model.ExternalUrl;

                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the link - ", e);
                return false;
            }
        }

        public bool Update(dtoLink model, HttpPostedFileBase imagePath)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Link link = context.Links.Where(l => l.LinkId == model.LinkId).FirstOrDefault();

                    link.AlternateName = model.AlternateName;
                    link.ExternalUrl = model.ExternalUrl;
                    link.ImagePath = FileUpload.SaveLink(imagePath);

                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the link - ", e);
                return false;
            }
        }

        public dtoLink Single(Expression<Func<Link, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected link - ", e);
                return null;
            }
        }
    }
}
