﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Utils.Uploads;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class ComicService : IService<dtoComic, Comic>, IComicService
    {
        private readonly IObjectSet<Comic> _objectSet;
        private readonly TranscriptService _transcriptService = new TranscriptService();
        private readonly ComicCastService _comicCastService = new ComicCastService();
        private readonly ComicTagService _comicTagService = new ComicTagService();

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Comic> ObjectSet
        {
            get { return _objectSet; }
        }

        public ComicService()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public ComicService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Comic>();
        }

        public void Add(dtoComic entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new comic - " + entity.Title + " ", e);
            }
        }

        public string Add(dtoComic entity, HttpPostedFileBase bannerImage, HttpPostedFileBase comicImage, HttpPostedFileBase thumbImage)
        {
            Comic comic = new Comic();

            try
            {
                UpdateHomePageComics();

                using (ComicEntities context = new ComicEntities())
                {
                    comic = entity.ToEntity();
                    dtoTranscript transcript = new dtoTranscript();

                    if (bannerImage != null)
                        comic.BannerImage = FileUpload.SaveComicBanner(bannerImage);

                    comic.ComicImage = FileUpload.SaveComic(comicImage);

                    if (comic.ComicImage == "fail")
                        return Constants.Fail;

                    comic.ThumbImage = FileUpload.SaveComicThumbNail(thumbImage);

                    if (comic.ThumbImage == "fail")
                        return Constants.Fail;

                    comic.Title = StringExtensions.CleanText(comic.Title);
                     
                    if (comic.ComicSlug == null)
                        comic.ComicSlug = Slugify.SeoFriendly(true, comic.Title);
                    else
                        comic.ComicSlug = Slugify.SeoFriendly(true, StringExtensions.CleanText(comic.ComicSlug));

                    comic.LongDescription = StringExtensions.CleanText(comic.LongDescription);
                    comic.ShortDescription = StringExtensions.CleanText(comic.ShortDescription);

                    //Add comic
                    context.Comics.Add(comic);
                    context.SaveChanges();
                }

                if (comic.ComicID > 0)
                    AddTranscriptToComic(comic);
                else
                    return Constants.Fail;

                if (!AddCastToComic(comic, entity))
                    return Constants.Fail;

                if (!AddTagsToComic(comic, entity))
                    return Constants.Fail;

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new comic " + entity.Title + " - ", e);
                return Constants.Fail;
            }
        }

        public void Attach(dtoComic entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the new comic - " + entity.Title + " ", e);
            }
        }

        public long Count(Expression<Func<Comic, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Returning the comic count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the comic count - ", e);
                return -1;
            }
        }

        public dtoCurrentPage CurrentComic()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var comic = context.Comics.Where(c => c.OnHomePage == true).FirstOrDefault().ToDTO();

                    dtoCurrentPage current = new dtoCurrentPage()
                    {
                        ComicSlug = comic.ComicSlug,
                        ComicTitle = comic.Title,
                        ImageUrl = comic.ComicImage
                    };

                    return current;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: There was a problem getting the current comic page - ", e);
                return null;
            }
        }

        public void Delete(dtoComic entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected comic - ", e);
            }
        }

        public bool Delete(int id)
        {
            bool success = false;

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var comic = context.Comics.Where(c => c.ComicID == id).FirstOrDefault();

                    if (comic != null)
                    {
                        context.Comics.Remove(comic);

                        return success = true;
                    }
                }

                return success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected comic - ", e);
                return success;
            }
        }

        public IQueryable<dtoComic> ComicDisplay()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    return context.Comics.OrderByDescending(c => c.ComicID).ToDTOs().AsQueryable();

                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting comic list - ", e);
                return null;
            }
        }

        public IList<dtoComic> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the comic list - ", e);
                return null;
            }
        }

        public IList<dtoComic> GetAll(Expression<Func<Comic, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of comics by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComic> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of comics - ", e);
                return null;
            }
        }

        public IEnumerable<dtoComic> GetAllEnumerated(Expression<Func<Comic, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of comics using the specified criteria - ", e);
                return null;
            }
        }

        public IQueryable<dtoComic> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of comics - ", e);
                return null;
            }
        }

        public string PublishComic(dtoComic entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var lastComic = context.Comics.Where(c => c.IsLastComic == true).First();

                    lastComic.IsLastComic = false;

                    context.SaveChanges();
                }

                using (ComicEntities context = new ComicEntities())
                {
                    var homePageComic = context.Comics.Where(c => c.OnHomePage == true).First();

                    homePageComic.OnHomePage = false;
                    homePageComic.IsArchived = true;
                    homePageComic.IsLastComic = true;

                    context.SaveChanges();
                }

                using (ComicEntities context = new ComicEntities())
                {
                    var comic = context.Comics.Where(c => c.ComicID == entity.ComicID).First();

                    comic.OnHomePage = entity.OnHomePage;

                    context.SaveChanges();
                }

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the selected comic - ", e);
                return Constants.Fail;
            }
        }

        public dtoComic Single(Expression<Func<Comic, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected comic - ", e);
                return null;
            }
        }

        public string Update(dtoComic entity, HttpPostedFileBase bannerImagefile, HttpPostedFileBase comicImageFile, HttpPostedFileBase thumbFile)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var comic = context.Comics.Where(c => c.ComicID == entity.ComicID).FirstOrDefault();

                    if (bannerImagefile != null)
                    {
                        var uploadedBannerImage = FileUpload.SaveComicBanner(bannerImagefile);
                        comic.BannerImage = uploadedBannerImage;
                    }

                    if (comicImageFile != null)
                    {
                        var uploadedComicImage = FileUpload.SaveComic(comicImageFile);
                        comic.ComicImage = uploadedComicImage;
                    }

                    if (thumbFile != null)
                    {
                        var uploadedThumbImage = FileUpload.SaveComicThumbNail(thumbFile);
                        comic.ThumbImage = uploadedThumbImage;
                    }

                    comic.ChapterID = entity.ChapterID;

                    if (entity.ComicSlug != comic.ComicSlug)
                        comic.ComicSlug = Slugify.SeoFriendly(true, entity.ComicSlug);
                    else
                        comic.ComicSlug = entity.ComicSlug;

                    comic.IsArchived = entity.IsArchived;
                    comic.IsLastComic = entity.IsLastComic;
                    comic.OnHomePage = entity.OnHomePage;
                    comic.Title = entity.Title;
                    comic.ShortDescription = entity.ShortDescription;
                    comic.LongDescription = entity.LongDescription;

                    context.SaveChanges();
                }

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the selected comic - ", e);
                return Constants.Fail;
            }
        }

        private bool UpdateHomePageComics()
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {

                    var currentComic = context.Comics.Where(c => c.OnHomePage == true && c.IsArchived == false && c.IsLastComic == false).FirstOrDefault();
                    var lastComic = context.Comics.Where(c => c.IsLastComic == true).FirstOrDefault();

                    if (currentComic != null)
                    {
                        currentComic.OnHomePage = false;
                        currentComic.IsArchived = true;
                        currentComic.IsLastComic = true;
                    }

                    if (lastComic != null)
                    {
                        lastComic.IsLastComic = false;
                    }

                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                return false;
            }
        }

        private string AddTranscriptToComic(Comic comic)
        {
            dtoTranscript transcript = new dtoTranscript();
            transcript.ParentComic = comic.ComicID;

            transcript.Comic = new dtoComic()
            {
                Title = comic.Title
            };

            //Add transcript to comic.
            return _transcriptService.AddRetString(transcript);
        }

        private bool AddCastToComic(Comic comic, dtoComic entity)
        {
            //Add comic cast
            try
            {
                using (ComicEntities db = new ComicEntities())
                {
                    foreach (var cast in entity.CastMembers)
                    {
                        var castMember = new ComicCast();
                        castMember.CastID = cast.CastID;
                        castMember.ComicID = comic.ComicID;

                        db.ComicCasts.Add(castMember);
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch(Exception e)
            {
                Log.Error("[ERROR] - Adding cast to new comic", e);
                return false;
            }
        }

        private bool AddTagsToComic(Comic comic, dtoComic entity)
        {
            try
            {
                foreach (var tag in entity.Tags)
                {
                    dtoComicTag cTag = new dtoComicTag()
                    {
                        ComicId = comic.ComicID,
                        TagId = tag.TagId
                    };

                    _comicTagService.Add(cTag);
                }

                return true;
            }
            catch 
            {
                return false;
            }
        }
    }
}

