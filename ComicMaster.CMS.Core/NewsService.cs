﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Controls.Pager.NewsPager;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class NewsService : IService<dtoNews, News>, INewsService
    {
        private readonly IObjectSet<News> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<News> ObjectSet
        {
            get { return _objectSet;}
        }

        public NewsService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public NewsService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<News>();
        }

        public void Add(dtoNews entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new News - " + entity.Title + " ", e);
            }
        }

        public string AddNew(dtoNews dto)
        {
            var exists = Single(n => n.Title == dto.Title);            

            if (exists != null)
                return Constants.NewsExists;

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    dto.Title = dto.Title;
                    dto.Text = dto.Text;
                    dto.NewsSlug = Slugify.SeoFriendly(true, dto.Title);
                    context.News.Add(dto.ToEntity());
                    context.SaveChanges();

                    return Constants.Success;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding news article - ", e);
                return Constants.Fail;
            }
        }

        public void Attach(dtoNews entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new News - " + entity.Title + " ", e);
            }
        }

        public long Count(Expression<Func<News, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the News count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the News count - ", e);
                return -1;
            }
        }

        public void Delete(dtoNews entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select News - ", e);
            }
        }

        public string DeleteNews(dtoNews dto)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.News.Remove(dto.ToEntity());
                    context.SaveChanges();

                    return Constants.Success;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected news article - ", e);
                return Constants.Fail;
            }
        }

        public IList<dtoNews> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the News list - ", e);
                return null;
            }
        }

        public IList<dtoNews> GetAll(Expression<Func<News, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of News by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoNews> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of News - ", e);
                return null;
            }
        }

        public IEnumerable<dtoNews> GetAllEnumerated(Expression<Func<News, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of News - ", e);
                return null;
            }
        }

        public PagedList<dtoNews> GetNewsFeed(int pageIndex, int pageSize)
        {
            try
            {
                var news = GetAllEnumerated().OrderByDescending(n => n.NewsDate).AsEnumerable();

                return new PagedList<dtoNews>(news, pageIndex, pageSize, news.Count());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the news feed News - ", e);
                return null;
            }
        }

        public IQueryable<dtoNews> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of News - ", e);
                return null;
            }
        }

        public string UpdateNews(dtoNews dto)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var news = context.News.Where(n => n.NewsId == dto.NewsId).First();

                    if (news.NewsSlug == dto.NewsSlug && news.Title != dto.Title)
                    {
                        news.NewsSlug = Slugify.SeoFriendly(true, dto.Title);
                    }
                    else if (news.NewsSlug != dto.NewsSlug && news.Title == dto.Title)
                    {
                        news.NewsSlug = Slugify.SeoFriendly(true, dto.NewsSlug);
                    }
                    else if (news.NewsSlug != dto.NewsSlug && news.Title != dto.Title)
                    {
                        news.NewsSlug = Slugify.SeoFriendly(true, dto.NewsSlug);
                    }
                    else
                    {
                    }

                    news.ComicId = dto.ComicId;                    
                    news.Text = StringExtensions.CleanPostBody(dto.Text);
                    news.Title = StringExtensions.CleanText(dto.Title);

                    context.SaveChanges();

                    return Constants.Success;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the selected news article - ", e);

                return Constants.Fail;
            }
        }

        public dtoNews Single(Expression<Func<News, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected News - ", e);
                return null;
            }
        }
    }
}
