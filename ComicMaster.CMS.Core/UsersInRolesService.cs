﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class UsersInRolesService : IService<dtoaspnet_UsersInRoles, aspnet_UsersInRoles>, IUsersInRolesService
    {
        private readonly IObjectSet<aspnet_UsersInRoles> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<aspnet_UsersInRoles> ObjectSet
        {
            get { return _objectSet; }
        }

        public UsersInRolesService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public UsersInRolesService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<aspnet_UsersInRoles>();
        }

        public void Add(dtoaspnet_UsersInRoles entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new user role to userid - " + entity.UserId + " ", e);
            }
        }

        public void Attach(dtoaspnet_UsersInRoles entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the new user role to userid - " + entity.UserId + " ", e);
            }
        }

        public long Count(Expression<Func<aspnet_UsersInRoles, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the users in roles count by the specified criteria count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the users in roles count - ", e);
                return -1;
            }
        }

        public void Delete(dtoaspnet_UsersInRoles entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the select user from the selected role - ", e);
            }
        }

        public IList<dtoaspnet_UsersInRoles> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the users in roles list - ", e);
                return null;
            }
        }

        public IList<dtoaspnet_UsersInRoles> GetAll(Expression<Func<aspnet_UsersInRoles, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of roles for the specified user - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_UsersInRoles> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of users in roles - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_UsersInRoles> GetAllEnumerated(Expression<Func<aspnet_UsersInRoles, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of users in the role specified in the criteria - ", e);
                return null;
            }
        }

        public IQueryable<dtoaspnet_UsersInRoles> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of users in roles - ", e);
                return null;
            }
        }

        public dtoaspnet_UsersInRoles Single(Expression<Func<aspnet_UsersInRoles, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected user role - ", e);
                return null;
            }
        }
    }
}
