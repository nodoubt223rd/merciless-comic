﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class PostTagMapService : IService<dtoPostTagMap, PostTagMap>, IPostTagMapService
    {
        private readonly IObjectSet<PostTagMap> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<PostTagMap> ObjectSet
        {
            get { return _objectSet; }
        }

        public PostTagMapService()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public PostTagMapService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<PostTagMap>();
        }

        public void Add(dtoPostTagMap entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new category - ", e);
            }
        }

        public bool AddRetValue(dtoPostTagMap entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.PostTagMaps.Add(entity.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new category - ", e);
                return false;
            }
        }

        public void Attach(dtoPostTagMap entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the category - ", e);
            }
        }

        public long Count(Expression<Func<PostTagMap, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the category count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the category count - ", e);
                return -1;
            }
        }

        public void Delete(dtoPostTagMap entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected category - ", e);
            }
        }

        public bool DeleteRetValue(dtoPostTagMap entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.PostTagMaps.Remove(entity.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected category - ", e);
                return false;
            }
        }

        public IList<dtoPostTagMap> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of categories - ", e);
                return null;
            }
        }

        public IList<dtoPostTagMap> GetAll(Expression<Func<PostTagMap, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of categories specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoPostTagMap> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of categories - ", e);
                return null;
            }
        }

        public IEnumerable<dtoPostTagMap> GetAllEnumerated(Expression<Func<PostTagMap, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of categories - ", e);
                return null;
            }
        }

        public IQueryable<dtoPostTagMap> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of categories - ", e);
                return null;
            }
        }

        public dtoPostTagMap Single(Expression<Func<PostTagMap, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected category - ", e);
                return null;
            }
        }

        public bool Update(dtoPostTagMap entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    PostTagMap post = context.PostTagMaps.Where(p => p.RowId == entity.RowId).First();

                    post.PostId = entity.PostId;
                    post.TagId = entity.TagId;

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the category - ", e);
                return false;
            }
        }
    }
}
