﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class RoleService :IService<dtoaspnet_Roles, aspnet_Roles>, IRoleService
    {
        private readonly IObjectSet<aspnet_Roles> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public IObjectSet<aspnet_Roles> ObjectSet
        {
            get { return _objectSet;}
        }

        public RoleService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public RoleService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<aspnet_Roles>();
        }

        public void Add(dtoaspnet_Roles entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new aspnet_Roles - " + entity.RoleName + " ", e);
            }
        }

        public void Attach(dtoaspnet_Roles entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new role - " + entity.RoleName + " ", e);
            }
        }

        public long Count(Expression<Func<aspnet_Roles, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Returning the aspnet_Roles count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the aspnet_Roles count - ", e);
                return -1;
            }
        }

        public void Delete(dtoaspnet_Roles entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected role - ", e);
            }
        }

        public IList<dtoaspnet_Roles> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the aspnet_Roles list.", e);
                return null;
            }
        }

        public IList<dtoaspnet_Roles> GetAll(Expression<Func<aspnet_Roles, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of roles by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Roles> GetAllEnumerated() 
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of roles - ", e);
                return null;
            }
        }

        public IEnumerable<dtoaspnet_Roles> GetAllEnumerated(Expression<Func<aspnet_Roles, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of roles using the specified criteria - ", e);
                return null;
            }
        }

        public IQueryable<dtoaspnet_Roles> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of roles - ", e);
                return null;
            }
        }

        public dtoaspnet_Roles Single(Expression<Func<aspnet_Roles, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected role - ", e);
                return null;
            }
        }
    }
}
