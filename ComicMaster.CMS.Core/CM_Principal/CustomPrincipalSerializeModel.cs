﻿using System;

namespace ComicMaster.CMS.Core.CM_Principal
{
    public class CustomPrincipalSerializeModel
    {
        public int AuthorId { get; set; }
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Avatar { get; set; }
    }
}
