﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class PostService : IService<dtoPost, Post>, IPostService
    {
        private readonly IObjectSet<Post> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Post> ObjectSet
        {
            get { return _objectSet; }
        }

        public PostService()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public PostService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Post>();
        }

        public void Add(dtoPost entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new post - ", e);
            }
        }

        public bool AddRetValue(dtoPost entity, string tags)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Post post = entity.ToEntity();

                    post.Modified = DateTime.Now.ToLongDateString();

                    context.Posts.Add(post);
                    context.SaveChanges();

                    var category = context.Categories.Where(c => c.Id == post.CategoryId).FirstOrDefault();

                    category.Used += 1;

                    context.SaveChanges();

                    string[] _tags = tags.Split(',');

                    if (_tags.Length > 0 && !string.IsNullOrEmpty(_tags[0]))
                    {
                        foreach (string tag in _tags)
                        {
                            int tagId;

                            if (int.TryParse(tag, out tagId))
                            {
                                PostTagMap tagmap = new PostTagMap();

                                tagmap.PostId = post.Id;
                                tagmap.TagId = tagId;
                                context.PostTagMaps.Add(tagmap);
                                context.SaveChanges();
                            }

                            var _tag = context.Tags.Where(t => t.TagId == tagId).First();

                            _tag.TagCount += 1;
                            context.SaveChanges();
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new post - ", e);
                return false;
            }
        }

        public List<dtoPostArchive> Archives()
        {
            List<dtoPostArchive> archives = new List<dtoPostArchive>();

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var grouped = from p in context.Posts
                                  group p by ((DateTime)(p.PostedOn)).Year into yg
                                  select
                                  new
                                  {
                                      Year = yg.Key,
                                      MonthGroups =
                                        from o in yg
                                        group o by ((DateTime)(o.PostedOn)).Month into mg
                                        select new { Month = mg.Key, Posts = mg }
                                  };
                    var _grouped = grouped.ToList();

                    foreach (var item in _grouped)
                    {                        

                        foreach (var _item in item.MonthGroups)
                        {
                            dtoPostArchive pa = new dtoPostArchive();

                            pa.Year = item.Year;
                            pa.Month = DateTimeFormatInfo.CurrentInfo.GetMonthName(_item.Month);
                            pa.Posts = new List<dtoPost>(_item.Posts.OrderByDescending(p => p.PostedOn).ToDTOs());

                            foreach (var post in pa.Posts)
                            {
                                post.Tags = new List<dtoTag>();
                                post.Category = new dtoCategory();
                                var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();
                                var cat = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();

                                post.Category.Id = cat.Id;
                                post.Category.Name = cat.Name;
                                post.Category.UrlSlug = cat.UrlSlug;
                                post.Category.Used = cat.Used;
                                
                                foreach(var tagMap in tagMaps)
                                {
                                    var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                                    post.Tags.Add(tag);
                                }                                
                            }

                            archives.Add(pa);                            
                        }
                    }

                    return archives;
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the archives - ", e);
                return null;
            }
        }

        public List<dtoPostArchive> Archives(int year, string month)
        {
            List<dtoPostArchive> archives = new List<dtoPostArchive>();

            int _month = DateTime.ParseExact(month, "MMMM", CultureInfo.InvariantCulture).Month;

            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var grouped = from p in context.Posts
                                  where ((DateTime)(p.PostedOn)).Year == year && ((DateTime)(p.PostedOn)).Month == _month
                                  group p by ((DateTime)(p.PostedOn)).Year into yg
                                  select
                                  new
                                    {
                                        Year = yg.Key,
                                        MonthGroups =
                                         from o in yg
                                         group o by ((DateTime)(o.PostedOn)).Month into mg
                                         select new { Month = mg.Key, Posts = mg }
                                    };

                    var _grouped = grouped.ToList();

                    foreach (var item in _grouped)
                    {
                        dtoPostArchive pa = new dtoPostArchive();

                        pa.Year = year;

                        foreach (var _item in item.MonthGroups)
                        {
                            pa.Month = DateTimeFormatInfo.CurrentInfo.GetMonthName(_item.Month);
                            pa.Posts = new List<dtoPost>(_item.Posts.OrderByDescending(p => p.PostedOn).ToDTOs());
                            archives.Add(pa);
                        }
                        
                        foreach (var post in pa.Posts)
                        {
                            var cat = context.Categories.Where(c => c.Id == post.CategoryId).First().ToDTO();
                            var tags = context.PostTagMaps.Where(p => p.PostId == post.Id).ToList();

                            post.Category = new dtoCategory()
                            {
                                Description = cat.Description,
                                Id = cat.Id,
                                Name = cat.Name,
                                UrlSlug = cat.UrlSlug,
                                Used = cat.Used
                            };

                            post.Tags = new List<dtoTag>();

                            foreach (var _tag in tags)
                            {
                                dtoTag tag = context.Tags.Where(t => t.TagId == _tag.TagId).First().ToDTO();

                                post.Tags.Add(tag);
                            }
                            
                        }
                    }

                    return archives;
                }
                
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the archives - ", e);
                return null;
            }
        }

        public void Attach(dtoPost entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the post - ", e);
            }
        }

        public long Count(Expression<Func<Post, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the post count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the post count - ", e);
                return -1;
            }
        }

        public void Delete(dtoPost entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the post category - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var entity = context.Posts.Where(p => p.Id == id).First();
                    var tagMaps = context.PostTagMaps.Where(ptm => ptm.PostId == entity.Id);

                    if (tagMaps != null)
                    {
                        foreach (var tag in tagMaps)
                        {
                            context.PostTagMaps.Remove(tag);
                            context.SaveChanges();

                            var _tag = context.Tags.Where(t => t.TagId == tag.TagId).First();

                            if (_tag.TagCount > 0)
                            {
                                _tag.TagCount -= 1;
                                context.SaveChanges();
                            }
                        }
                    }

                    var cat = context.Categories.Where(c => c.Id == entity.CategoryId).First();

                    if (cat.Used > 0)
                    {
                        cat.Used -= 1;
                        context.SaveChanges();
                    }

                    context.Posts.Remove(entity);
                    context.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the post category - ", e);
                return false;
            }
        }

        public bool DeleteRetValue(dtoPost entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    context.Posts.Remove(entity.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the post category - ", e);
                return false;
            }
        }        

        public IList<dtoPost> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of posts - ", e);
                return null;
            }
        }

        public List<dtoPost> GetAll(int? count)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    if (count.HasValue)
                    {
                        var posts = context.Posts.ToList().ToDTOs();

                        return posts.OrderByDescending(p => p.PostedOn).Take(count.Value).ToList();
                    }
                    else
                    {
                        return context.Posts.OrderByDescending(p => p.PostedOn).ToDTOs();
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of posts - ", e);
                return null;
            }
        }

        public IList<dtoPost> GetAll(Expression<Func<Post, bool>> whereCondition)
        {
            try
            {                
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of posts specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoPost> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of posts - ", e);
                return null;
            }
        }

        public IEnumerable<dtoPost> GetAllEnumerated(Expression<Func<Post, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of posts - ", e);
                return null;
            }
        }

        public IQueryable<dtoPost> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of posts - ", e);
                return null;
            }
        }

        public dtoNewPost New()
        {
            return new dtoNewPost();
        }

        public dtoPost Post(int id, int blogId)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var post = context.Posts.Where(p => p.Id == id).First().ToDTO();

                    var cat = context.Categories.Where(c => c.Id == post.CategoryId).First();

                    post.Category = new dtoCategory()
                    {
                        Description = cat.Description,
                        Id = cat.Id,
                        Name = cat.Name,
                        UrlSlug = cat.UrlSlug
                    };

                    post.Tags = new List<dtoTag>();

                    var tagMap = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                    foreach (var tag in tagMap)
                    {
                        post.Tags.Add(context.Tags.Where(t => t.TagId == tag.TagId).FirstOrDefault().ToDTO());
                    }

                    return post;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the posts list - ", e);
                    return null;
                }
            }
        }

        public dtoPost Post(int i)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var post = context.Posts.Where(p => p.Id == i).First().ToDTO();

                    var cat = context.Categories.Where(c => c.Id == post.CategoryId).First();

                    post.Category = new dtoCategory()
                    {
                        Description = cat.Description,
                        Id = cat.Id,
                        Name = cat.Name,
                        UrlSlug = cat.UrlSlug
                    };

                    post.Tags = new List<dtoTag>();

                    var tagMap = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                    foreach (var tag in tagMap)
                    {
                        post.Tags.Add(context.Tags.Where(t => t.TagId == tag.TagId).FirstOrDefault().ToDTO());
                    }

                    return post;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the posts list - ", e);
                    return null;
                }
            }
        }

        public dtoPost Post(string slug)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var post = context.Posts.Where(p => p.UrlSlug == slug).First().ToDTO();
                    var blog = context.Blogs.Where(b => b.Id == post.BlogId).First();
                    var cat = context.Categories.Where(c => c.Id == post.CategoryId).First();                   

                    post.Category = new dtoCategory()
                    {
                        Description = cat.Description,
                        Id = cat.Id,
                        Name = cat.Name,
                        UrlSlug = cat.UrlSlug
                    };

                    post.BlogName = blog.BlogTitle;
                    post.BlogSlug = blog.BlogSlug;

                    post.Tags = new List<dtoTag>();

                    var tagMap = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                    foreach (var tag in tagMap)
                    {
                        post.Tags.Add(context.Tags.Where(t => t.TagId == tag.TagId).FirstOrDefault().ToDTO());
                    }

                    return post;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the posts list - ", e);
                    return null;
                }
            }
        }

        public List<dtoPost> Posts(int id, int pageNo, int pageSize)
        {
           try
           {
                using (ComicEntities context = new ComicEntities())
                {                    
                    var posts = context.Posts.Where(p => p.BlogId == id)                  
                        .OrderByDescending(p => p.PostedOn)
                        .Skip(pageNo * pageSize)
                        .Take(pageSize).ToDTOs();

                    foreach (var post in posts)
                    {
                        var cat = context.Categories.Where(c => c.Id == post.CategoryId).FirstOrDefault();

                        post.Category = new dtoCategory()
                        {
                            Description = cat.Description,
                            Id = cat.Id,
                            Name = cat.Name,
                            UrlSlug = cat.UrlSlug
                        };

                        post.Tags = new List<dtoTag>();

                        var tagMap = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                        foreach (var tag in tagMap)
                        {
                            post.Tags.Add(context.Tags.Where(t => t.TagId == tag.TagId).FirstOrDefault().ToDTO());
                        }
                    }

                    return posts.OrderByDescending(p => p.PostedOn).ToList();

                }
           }
            catch (Exception e)
           {
               Log.Error("[Error]: Getting the posts list - ", e);
               return null;
            }
        }

        public List<dtoPost> Posts(int year, string month)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    return null;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the posts list - ", e);
                return null;
            }
        }

        public dtoPost Single(Expression<Func<Post, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected post - ", e);
                return null;
            }
        }

        public bool Update(dtoPost entity, string tags)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Post post = context.Posts.Where(c => c.Id == entity.Id).First();
                    List<PostTagMap> tagList = context.PostTagMaps.Where(tm => tm.PostId == entity.Id).ToList();

                    if (post.ShortDescription == entity.ShortDescription && post.LongDescription != entity.LongDescription)
                    {
                        post.ShortDescription = entity.LongDescription.Substring(0, Math.Min(StringExtensions.CleanCommentBody(entity.LongDescription).Length, 80)) + "...";
                    }
                    else if (post.ShortDescription != entity.ShortDescription && post.LongDescription == entity.LongDescription)
                    {
                        post.ShortDescription = entity.ShortDescription;
                    }
                    else if (post.ShortDescription != entity.ShortDescription && post.LongDescription != entity.LongDescription)
                    {
                        post.ShortDescription = entity.ShortDescription;
                    }
                    else
                    {
                    }

                    string[] _tags = tags.Split(',');

                    // Add new tag mappings
                    foreach (string _tag in _tags)
                    {
                        int tagId;

                        if(int.TryParse(_tag, out tagId))
                        {
                            if (!tagList.Exists(i => i.TagId == tagId))
                            {
                                PostTagMap tagMap = new PostTagMap();

                                tagMap.PostId = entity.Id;
                                tagMap.TagId = tagId;
                                context.PostTagMaps.Add(tagMap);
                                context.SaveChanges();

                                var updateCount = context.Tags.Where(t => t.TagId == tagId).First();
                                updateCount.TagCount += 1;

                                context.SaveChanges();
                            }
                        }
                    }

                    // Remove old tag mappings and reduce count.
                    List<PostTagMap> usedTags = new List<PostTagMap>();

                    for(int i = 0; i < _tags.Length; i++)
                    {
                        PostTagMap newMap = new PostTagMap();

                        int tId;

                        if (int.TryParse(_tags[i], out tId))
                        {
                            newMap.TagId = tId;

                            usedTags.Add(newMap);
                        }
                    }

                    foreach (var _tag in tagList)
                    {
                        if (!usedTags.Exists(i => i.TagId == _tag.TagId))
                        {
                            var deleted = context.PostTagMaps.Where(tm => tm.PostId == entity.Id && tm.TagId == _tag.TagId).First();
                            context.PostTagMaps.Remove(deleted);
                            context.SaveChanges();

                            var removedTag = context.Tags.Where(t => t.TagId == _tag.TagId).First();
                            if (removedTag.TagCount > 0)
                                removedTag.TagCount -= 1;

                            context.SaveChanges();
                        }
                    }

                    if (post.CategoryId != entity.CategoryId)
                    {
                        var removedCat = context.Categories.Where(p => p.Id == post.CategoryId).First();

                        if (removedCat.Used > 0)
                            removedCat.Used -= 1;

                        context.SaveChanges();

                        var catUpdatedCount = context.Categories.Where(c => c.Id == entity.CategoryId).First();

                        catUpdatedCount.Used = catUpdatedCount.Used + 1;

                        context.SaveChanges();

                        post.CategoryId = entity.CategoryId;
                    }

                    post.LongDescription = entity.LongDescription;
                    post.Meta = entity.Meta;
                    post.Modified = DateTime.Now.ToLongDateString();
                    post.Published = entity.Published;                    

                    if (post.UrlSlug == entity.UrlSlug && post.Title != entity.Title)
                    {
                        post.UrlSlug = Slugify.SeoFriendly(true, entity.Title);
                    }
                    else if (post.UrlSlug != entity.UrlSlug && post.Title == entity.Title)
                    {
                        post.UrlSlug = entity.UrlSlug;
                    }
                    else if (post.UrlSlug != entity.UrlSlug && post.Title != entity.Title)
                    {
                        post.UrlSlug = entity.UrlSlug;
                    }
                    else { }

                    post.Title = entity.Title;
                    post.Status = entity.Status;
                    post.Visibility = entity.Visibility;

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the post - ", e);
                return false;
            }
        }

    }
}
