﻿using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Core.Service
{
    public interface IComicService : ICrudService<dtoComic>
    {
        int Add(dtoComic dto);
        bool Delete(dtoComic dto);        
        dtoComic Get(int id);
        IQueryable<dtoComic> GetAll();
        dtoComic GetByChapter(int id);
        IQueryable<dtoComic> GetByChapter(int id);
        dtoComic GetBySlug(string slug);
        bool IsLastMix(int val);
        bool OnHomePage(int val);
        int Update(dtoComic dto);
    }
}
