﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class AuthorService: IService<dtoAuthor, Author>, IAuthorService
    {
        private readonly IObjectSet<Author> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Author> ObjectSet
        {
            get { return _objectSet;}
        }

        public AuthorService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public AuthorService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Author>();
        }

        public void Add(dtoAuthor entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new author - ", e);
            }
        }

        public int AddRetValue(dtoAuthor _author)
        {
            try
            {
                Author author = _author.ToEntity();

                using (ComicEntities context = new ComicEntities())
                {
                    context.Authors.Add(author);
                    context.SaveChanges();
                }

                return author.AuthorID;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new author - ", e);
                return -1;
            }
        }

        public void Attach(dtoAuthor entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new author - ", e);
            }
        }        

        public long Count(Expression<Func<Author, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the author count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the author count - ", e);
                return -1;
            }
        }

        public void Delete(dtoAuthor entity) 
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select author - ", e);
            }
        }

        public string DeleteRetString(dtoAuthor _author)
        {
            try
            {
                this.ObjectSet.DeleteObject(_author.ToEntity());

                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the author - ", e);
                return Constants.Fail;
            }
        }

        public IList<dtoAuthor> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the authors list - ", e);
                return null;
            }
        }

        public IList<dtoAuthor> GetAll(Expression<Func<Author, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of authors by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoAuthor> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of authors - ", e);
                return null;
            }
        }

        public IEnumerable<dtoAuthor> GetAllEnumerated(Expression<Func<Author, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of authors - ", e);
                return null;
            }
        }

        public IQueryable<dtoAuthor> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of authors - ", e);
                return null;
            }
        }

        public dtoAuthor Single(Expression<Func<Author, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected author - ", e);
                return null;
            }
        }
    }
}
