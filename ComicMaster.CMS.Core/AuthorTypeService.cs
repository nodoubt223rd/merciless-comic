﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class AuthorTypeService : IService<dtoAuthorType, AuthorType>, IAuthorTypeService
    {
        private readonly IObjectSet<AuthorType> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<AuthorType> ObjectSet
        {
            get { return _objectSet;}
        }

        public AuthorTypeService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public AuthorTypeService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<AuthorType>();
        }

        public void Add(dtoAuthorType entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new author type - " + entity.Description + " ", e);
            }
        }

        public void Attach(dtoAuthorType entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the new author type - " + entity + " ", e);
            }
        }

        public long Count(Expression<Func<AuthorType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the author type count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the author type count - ", e);
                return -1;
            }
        }

        public void Delete(dtoAuthorType entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the select author type - ", e);
            }
        }

        public IList<dtoAuthorType> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the author type list - ", e);
                return null;
            }
        }

        public IList<dtoAuthorType> GetAll(Expression<Func<AuthorType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of author types by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoAuthorType> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of author types - ", e);
                return null;
            }
        }

        public IEnumerable<dtoAuthorType> GetAllEnumerated(Expression<Func<AuthorType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of author types - ", e);
                return null;
            }
        }

        public IQueryable<dtoAuthorType> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of author types - ", e);
                return null;
            }
        }

        public dtoAuthorType Single(Expression<Func<AuthorType, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected author type - ", e);
                return null;
            }
        }
    }
}
