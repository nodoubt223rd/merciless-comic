﻿using System;
using System.Collections.Generic;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IUsersInRolesService : IService<dtoaspnet_UsersInRoles, aspnet_UsersInRoles>
    {
    }
}
