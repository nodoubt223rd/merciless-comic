﻿using System;
using System.Linq;

using ComicMaster.CMS.Common.Controls.Pager.NewsPager;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface INewsService : IService<dtoNews, News>
    {
        string AddNew(dtoNews dto);
        string UpdateNews(dtoNews dto);
        string DeleteNews(dtoNews dto);
        PagedList<dtoNews> GetNewsFeed(int pageIndex, int pageSize);
    }
}
