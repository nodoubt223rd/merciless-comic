﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IPostService : IService<dtoPost, Post>
    {
        bool AddRetValue(dtoPost entity, string tags);
        List<dtoPostArchive> Archives();
        List<dtoPostArchive> Archives(int year, string month);
        bool Delete(int id);
        bool DeleteRetValue(dtoPost entity);
        List<dtoPost> GetAll(int? count);
        dtoNewPost New();
        dtoPost Post(int id, int blogid);
        dtoPost Post(int id);
        dtoPost Post(string slug);
        List<dtoPost> Posts(int id, int pageNo, int pageSize);
        List<dtoPost> Posts(int year, string month);
        bool Update(dtoPost entity, string tags);
    }
}
