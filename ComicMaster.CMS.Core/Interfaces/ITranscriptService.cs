﻿using System;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ITranscriptService : IService<dtoTranscript, Transcript>
    {
        string AddRetString(dtoTranscript entity);
        bool Update(dtoTranscript entity);
    }
}
