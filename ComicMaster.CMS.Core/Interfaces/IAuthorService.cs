﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IAuthorService : IService<dtoAuthor, Author>
    {
        int AddRetValue(dtoAuthor _author);
        string DeleteRetString(dtoAuthor _author);
    }
}
