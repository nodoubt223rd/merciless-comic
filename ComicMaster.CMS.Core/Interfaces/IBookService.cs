﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IBookService : IService<dtoBook, Book>
    {
        string AddRetString(dtoBook model);
        bool Delete(int bookId);
        string Update(dtoBook entity);
    }
}
