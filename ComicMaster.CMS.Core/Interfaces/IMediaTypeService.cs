﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IMediaTypeService : IService<dtoMediaType, MediaType>
    {
        bool AddRetValue(dtoMediaType model);
        IList<dtoMediaType> MediaTypes(int id);
        bool Delete(int id);
        bool Update(dtoMediaType model);
    }
}
