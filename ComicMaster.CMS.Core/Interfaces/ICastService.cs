﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ICastService : IService<dtoCast, Cast>
    {
        string Add(dtoCast dto, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbUpload);
        bool DeleteRetBool(dtoCast entity);
        IList<dtoCast> GetAll(string sortOrder);
        string Update(dtoCast dto, HttpPostedFileBase imageUpload, HttpPostedFileBase thumbUpload);
    }
}
