﻿using System;
using System.Collections.Generic;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IBlogService : IService<dtoBlog, Blog>
    {
        bool AddRetVal(dtoBlog blog);
        bool Delete(int id);
        bool Update(dtoBlog blog);
        dtoBlog Single(int id);
        List<dtoBlog> Blogs();
        List<dtoBlog> Blogs(int authorId);
        dtoBlog Blog(string name);
    }
}
