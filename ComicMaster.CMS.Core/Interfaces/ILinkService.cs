﻿using System;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ILinkService : IService<dtoLink, Link>
    {
        bool AddRetVal(dtoLink entity, HttpPostedFileBase imagePath);
        bool Delete(int id);
        bool Update(dtoLink model);
        bool Update(dtoLink model, HttpPostedFileBase imagePath);
    }
}
