﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Common.Security.Membership;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IMembershipService : IService<dtoaspnet_Membership, aspnet_Membership>
    {
        bool AddRetBool(dtoaspnet_Users entity);
        IList<dtoaspnet_Membership> Authors();
        bool CheckEmail(string email);
        bool DeleteRetBool(int id);
        dtoaspnet_Applications GetApplicationInfo();
        string GetUserName(string username);
        string UpdateRetString(dtoaspnet_Membership entity, HttpPostedFileBase fileupload);
        bool ValidateUser(LoginModel login);       
    }
}
