﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IPostTagMapService : IService<dtoPostTagMap, PostTagMap>
    {
        bool AddRetValue(dtoPostTagMap entity);
        bool DeleteRetValue(dtoPostTagMap entity);        
        bool Update(dtoPostTagMap entity);
    }
}
