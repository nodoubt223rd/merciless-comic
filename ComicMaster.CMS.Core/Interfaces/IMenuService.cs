﻿using System;
using System.Collections.Generic;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IMenuService
    {
        dtoMenu GetMenu();
    }
}
