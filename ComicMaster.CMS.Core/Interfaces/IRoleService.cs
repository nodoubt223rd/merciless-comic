﻿using System;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IRoleService : IService<dtoaspnet_Roles, aspnet_Roles>
    {
    }
}
