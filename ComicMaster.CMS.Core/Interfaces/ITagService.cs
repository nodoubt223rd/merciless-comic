﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ITagService : IService<dtoTag, Tag>
    {
        bool Add(string newTags);
        bool Delete(int id);
        IList<dtoTag> MostUsed();
        IList<dtoTag> Tags();
        dtoTag Tag(string name);
        IList<dtoTag> TagCloud();
        bool Update(dtoTag tag);
    }
}
