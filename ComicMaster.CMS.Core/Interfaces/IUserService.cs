﻿using System;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IUserService : IService<dtoaspnet_Users, aspnet_Users>
    {
    }
}
