﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IChapterService : IService<dtoChapter, Chapter>
    {
        string Add(dtoChapter model, int bookId);
        string Update(dtoChapter model, int bookId);
        bool Delete(int id);
    }
}
