﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IComicService : IService<dtoComic, Comic>
    {
        string Add(dtoComic entity, HttpPostedFileBase bannerImage, HttpPostedFileBase comicImage, HttpPostedFileBase thumbImage);
        IQueryable<dtoComic> ComicDisplay();
        dtoCurrentPage CurrentComic();
        bool Delete(int id);
        string Update(dtoComic entity, HttpPostedFileBase bannerImagefile, HttpPostedFileBase comicImageFile, HttpPostedFileBase thumbFile);
        string PublishComic(dtoComic entity);
    }
}
