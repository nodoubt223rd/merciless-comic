﻿using System;
using System.Security.Principal;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ICustomPrincipal : IPrincipal
    {
        int AuthorId { get; set; }
        Guid UserId { get; set; }
        string FirstName { get; set; }
        string LastName { get; set; }
        string Avatar { get; set; }
    }
}
