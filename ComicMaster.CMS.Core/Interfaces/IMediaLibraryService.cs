﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IMediaLibraryService : IService<dtoMediaLibrary, MediaLibrary>
    {
        bool Add(dtoMediaLibrary model, HttpPostedFileBase file);
        bool AddRetValue(dtoMediaLibrary model);
        bool Delete(int id);
        dtoMediaLibrary Single(int id);
        bool Update(dtoMediaLibrary model, HttpPostedFileBase file);
    }
}
