﻿using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface ICategoryService : IService<dtoCategory, Category>
    {
        bool AddRetValue(dtoCategory entity);
        IList<dtoCategory> Categories();
        IList<dtoCategory> Categories(int take);
        List<dtoCategory> Categories(string name);
        dtoCategory Categoy(string name);
        IList<dtoCategory> MostUsed();
        bool Delete(int id);
        bool Update(dtoCategory entity);
    }
}
