﻿using System;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Interfaces
{
    public interface IComicCastService : IService<dtoComicCast, ComicCast>
    {        
        bool DeleteRetBool(dtoComicCast entity);
        string AddNewCast(dtoComicCast entity);
    }
}
