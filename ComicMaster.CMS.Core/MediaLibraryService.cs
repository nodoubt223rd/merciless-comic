﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Drawing;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Web;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Utils.Uploads;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;
using ComicMaster.CMS.Services.Images;

namespace ComicMaster.CMS.Core
{
    public class MediaLibraryService : IService<dtoMediaLibrary, MediaLibrary>, IMediaLibraryService
    {
        private readonly IObjectSet<MediaLibrary> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        
        public IObjectSet<MediaLibrary> ObjectSet
        {
            get { return _objectSet; }
        }
        
        public MediaLibraryService()
            : this(new ComicMasterRepositoryContext())
        {            
        }
        
        public MediaLibraryService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<MediaLibrary>();
        }
        

        public void Add(dtoMediaLibrary entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the media item - ", e);
            }
        }

        public bool Add(dtoMediaLibrary model, HttpPostedFileBase file)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    model.Name = StringExtensions.CleanText(model.Name);
                    model.Slug = Slugify.SeoFriendly(true, model.Name);
                    if (file != null)
                    {
                        model.ImagePath = FileUpload.SaveGalleryImage(file);

                        using (Image img = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(Constants.Gallery + file.FileName)))
                        {
                            model.Dimensions = img.Width.ToString() + " x " + img.Height.ToString();
                        }
                    }
                    else
                        return false;

                    model.Extention = ImageType.GetImageType(file.ContentType);

                    if (GenerateThumbNail.Create(model.ImagePath, Constants.ThumbNails + model.Slug + "." + model.Extention))
                        model.ThumbPath = Constants.ThumbNails + model.Slug + "." + model.Extention;
                    else
                        return false;

                    model.Uploaded = DateTime.Now;

                    context.MediaLibraries.Add(model.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception dbEx)
            {
                Log.Error("[Error adding the new comic - ", dbEx);
                return false;
            }            
        }

        public bool AddRetValue(dtoMediaLibrary model)
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    model.Slug = Slugify.SeoFriendly(true, model.Name);

                    context.MediaLibraries.Add(model.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the media item - ", e);

                return false;
            }
        }

        public void Attach(dtoMediaLibrary entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the media item - ", e);
            }
        }

        public long Count(Expression<Func<MediaLibrary, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the library item count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the library item count - ", e);
                return -1;
            }
        }

        public void Delete(dtoMediaLibrary entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected media item - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    MediaLibrary mediaItem = context.MediaLibraries.Where(ml => ml.MediaId == id).First();

                    context.MediaLibraries.Remove(mediaItem);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected media item - ", e);
                return false;
            }
        }

        public IList<dtoMediaLibrary> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of media items - ", e);
                return null;
            }
        }

        public IList<dtoMediaLibrary> GetAll(Expression<Func<MediaLibrary, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of media items specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoMediaLibrary> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of media items - ", e);
                return null;
            }
        }

        public IEnumerable<dtoMediaLibrary> GetAllEnumerated(Expression<Func<MediaLibrary, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of media items - ", e);
                return null;
            }
        }

        public IQueryable<dtoMediaLibrary> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of media items - ", e);
                return null;
            }
        }

        public dtoMediaLibrary Single(Expression<Func<MediaLibrary, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected media item - ", e);
                return null;
            }
        }

        public dtoMediaLibrary Single(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var media = context.MediaLibraries.Where(m => m.MediaId == id).First().ToDTO();
                    var type = context.MediaTypes.Where(mt => mt.TypeId == media.MediaType).First();

                    media.GalleryName = type.Description;

                    return media;
                }
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected media item - ", e);
                return null;
            }
        }

        public bool Update(dtoMediaLibrary model, HttpPostedFileBase file)
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    var updated = context.MediaLibraries.Where(ml => ml.MediaId == model.MediaId).First();

                    if (updated.Slug == model.Slug && updated.Name != model.Name)
                        updated.Slug = Slugify.SeoFriendly(true, model.Name);  
                    else if (updated.Slug != model.Slug && updated.Name == model.Name)
                        updated.Slug = Slugify.SeoFriendly(true, model.Slug);
                    else if (updated.Slug != model.Slug && updated.Name == model.Name)
                        updated.Slug = Slugify.SeoFriendly(true, model.Slug);
                    else
                    {                    
                    }

                    if (file != null)
                    {
                        updated.ImagePath = FileUpload.SaveGalleryImage(file);

                        using (Image img = System.Drawing.Image.FromFile(HttpContext.Current.Server.MapPath(Constants.Gallery + file.FileName)))
                        {
                            model.Dimensions = img.Width.ToString() + " x " + img.Height.ToString();
                        }

                        model.Extention = ImageType.GetImageType(file.ContentType);

                        if (GenerateThumbNail.Create(model.ImagePath, Constants.ThumbNails + model.Slug + "." + model.Extention))
                            model.ThumbPath = Constants.ThumbNails + model.Slug + "." + model.Extention;
                    }

                    updated.MediaType = model.MediaType;
                    updated.Name = StringExtensions.CleanText(model.Name);              

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the media item. - ", e);
                return false;
            }
        }
        
    }
}
