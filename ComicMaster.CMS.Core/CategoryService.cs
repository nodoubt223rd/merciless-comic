﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class CategoryService : IService<dtoCategory, Category>, ICategoryService
    {
        private readonly IObjectSet<Category> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Category> ObjectSet
        {
            get { return _objectSet; }
        }

        public CategoryService()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public CategoryService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Category>();
        }

        public void Add(dtoCategory entity)
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new category - ", e);
            }
        }

        public bool AddRetValue(dtoCategory entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    entity.UrlSlug = Slugify.SeoFriendly(true, entity.Name);
                    entity.Description = StringExtensions.CleanText(entity.Description);
                    entity.Name = StringExtensions.CleanText(entity.Name);
                    entity.Used = 0;

                    context.Categories.Add(entity.ToEntity());
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new category - ", e);
                return false;
            }
        }

        public void Attach(dtoCategory entity)
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Attaching the category - ", e);
            }
        }

        public IList<dtoCategory> Categories()
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var categories = context.Categories.ToDTOs();

                    foreach (var category in categories)
                    {
                        var posts = context.Posts.Where(p => p.CategoryId == category.Id).ToDTOs();

                        posts.OrderByDescending(p => p.PostedOn);

                        foreach(var post in posts)
                        {
                            var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                            post.Tags = new List<dtoTag>();

                            foreach(var tagMap in tagMaps)
                            {
                                var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                                post.Tags.Add(tag);
                            }
                        }

                        category.Posts = new List<dtoPost>(posts);
                    }

                    return categories;
                }
                catch(Exception e)
                {
                    Log.Error("[Error]: Getting the category list - ", e);
                    return null;
                }
            }
        }

        public IList<dtoCategory> Categories(int take)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var categories = context.Categories.Where(c => c.Used >= 1).ToDTOs();
                    categories.OrderByDescending(c => c.Used).Take(take);

                    foreach (var category in categories)
                    {
                        var post = context.Posts.Where(p => p.CategoryId == category.Id).FirstOrDefault().ToDTO();

                        category.Posts = new List<dtoPost>();

                        category.Posts.Add(post);
                    }

                    return categories;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the category list - ", e);
                    return null;
                }
            }
        }

        public List<dtoCategory> Categories(string name)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var categories = context.Categories.Where(c => c.UrlSlug == name).ToDTOs();

                    foreach (var category in categories)
                    {
                        var posts = context.Posts.Where(p => p.CategoryId == category.Id).ToDTOs();

                        category.Posts = new List<dtoPost>(posts.OrderByDescending(p => p.PostedOn));

                        foreach (var post in category.Posts)
                        {
                            var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                            post.Tags = new List<dtoTag>();

                            foreach (var tagMap in tagMaps)
                            {
                                var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                                post.Tags.Add(tag);
                            }
                        }
                    }

                    return categories;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the category list - ", e);
                    return null;
                }
            }
        }

        public dtoCategory Categoy(string name)
        {
            using (ComicEntities context = new ComicEntities())
            {
                try
                {
                    var category = context.Categories.Where(c => c.UrlSlug == name).First().ToDTO();
                    var posts = context.Posts.Where(p => p.CategoryId == category.Id).OrderByDescending(p => p.PostedOn).ToDTOs();

                    foreach (var post in posts)
                    {
                        var tagMaps = context.PostTagMaps.Where(tm => tm.PostId == post.Id).ToDTOs();

                        post.Tags = new List<dtoTag>();

                        foreach (var tagMap in tagMaps)
                        {
                            var tag = context.Tags.Where(t => t.TagId == tagMap.TagId).First().ToDTO();

                            post.Tags.Add(tag);
                        }
                    }

                    category.Posts = new List<dtoPost>();


                    category.Posts.AddRange(posts);

                    return category;
                }
                catch (Exception e)
                {
                    Log.Error("[Error]: Getting the category list - ", e);
                    return null;
                }
            }
        }

        public long Count(Expression<Func<Category, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the category count - ", e);
                return -1;
            }
        }

        public long Count()
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the category count - ", e);
                return -1;
            }
        }

        public void Delete(dtoCategory entity)
        {
            try
            {
                this.ObjectSet.DeleteObject(entity.ToEntity());
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected category - ", e);
            }
        }

        public bool Delete(int id)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Category entity = context.Categories.Where(c => c.Id == id).First();

                    var uncat = context.Categories.Where(c => c.Name == "Uncategorized").First();

                    var posts = context.Posts.Where(p => p.CategoryId == id).ToList();

                    foreach (var post in posts)
                    {
                        post.CategoryId = uncat.Id;
                        uncat.Used += 1;
                        context.SaveChanges();
                    }

                    context.Categories.Remove(entity);
                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Deleting the selected category - ", e);
                return false;
            }
        }

        public IList<dtoCategory> GetAll()
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of categories - ", e);
                return null;
            }
        }

        public IList<dtoCategory> GetAll(Expression<Func<Category, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the list of categories specified in the criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoCategory> GetAllEnumerated()
        {
            try
            {
                return this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of categories - ", e);
                return null;
            }
        }

        public IEnumerable<dtoCategory> GetAllEnumerated(Expression<Func<Category, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of categories - ", e);
                return null;
            }
        }

        public IQueryable<dtoCategory> GetQuery()
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of categories - ", e);
                return null;
            }
        }

        public IList<dtoCategory> MostUsed()
        {
            try
            {
                using(ComicEntities context = new ComicEntities())
                {
                    var mostUsed = context.Categories.Where(c => c.Used > 0).OrderByDescending(c => c.Used).Take(5).ToDTOs();

                    return mostUsed;
                }
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of most used categories - ", e);
                return null;
            }
        }

        public dtoCategory Single(Expression<Func<Category, bool>> whereCondition)
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected category - ", e);
                return null;
            }
        }

        public bool Update(dtoCategory entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    Category category = context.Categories.Where(c => c.Id == entity.Id).First();

                    category.Description = StringExtensions.CleanText(entity.Description);
                    category.Name = StringExtensions.CleanText(entity.Name);
                    category.UrlSlug = Slugify.SeoFriendly(true, StringExtensions.CleanText(entity.Name));                    

                    context.SaveChanges();
                }

                return true;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Updating the category - ", e);
                return false;
            }
        }

    }
}
