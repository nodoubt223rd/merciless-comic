using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="News"/> and <see cref="dtoNews"/>.
    /// </summary>
    public static partial class NewsAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoNews"/> converted from <see cref="News"/>.</param>
        static partial void OnDTO(this News entity, dtoNews dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="News"/> converted from <see cref="dtoNews"/>.</param>
        static partial void OnEntity(this dtoNews dto, News entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoNews"/> to an instance of <see cref="News"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoNews"/> to convert.</param>
        public static News ToEntity(this dtoNews dto)
        {
            if (dto == null) return null;

            var entity = new News();

            entity.NewsId = dto.NewsId;
            entity.ComicId = dto.ComicId;
            entity.Title = dto.Title;
            entity.Text = dto.Text;
            entity.NewsDate = dto.NewsDate;
            entity.NewsSlug = dto.NewsSlug;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="News"/> to an instance of <see cref="dtoNews"/>.
        /// </summary>
        /// <param name="entity"><see cref="News"/> to convert.</param>
        public static dtoNews ToDTO(this News entity)
        {
            if (entity == null) return null;

            var dto = new dtoNews();

            dto.NewsId = entity.NewsId;
            dto.ComicId = entity.ComicId.Value;
            dto.Title = entity.Title;
            dto.Text = entity.Text;
            dto.NewsDate = entity.NewsDate;
            dto.NewsSlug = entity.NewsSlug;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoNews"/> to an instance of <see cref="News"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<News> ToEntities(this IEnumerable<dtoNews> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="News"/> to an instance of <see cref="dtoNews"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoNews> ToDTOs(this IEnumerable<News> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="News"/> to an instance of <see cref="dtoNews"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoNews> ToQueryDTOs(this IQueryable<News> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
