using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_Membership_GetPassword_Result"/> and <see cref="dtoaspnet_Membership_GetPassword_Result"/>.
    /// </summary>
    public static partial class aspnet_Membership_GetPassword_ResultAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Membership_GetPassword_Result"/> converted from <see cref="aspnet_Membership_GetPassword_Result"/>.</param>
        static partial void OnDTO(this aspnet_Membership_GetPassword_Result entity, dtoaspnet_Membership_GetPassword_Result dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Membership_GetPassword_Result"/> converted from <see cref="dtoaspnet_Membership_GetPassword_Result"/>.</param>
        static partial void OnEntity(this dtoaspnet_Membership_GetPassword_Result dto, aspnet_Membership_GetPassword_Result entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Membership_GetPassword_Result"/> to an instance of <see cref="aspnet_Membership_GetPassword_Result"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Membership_GetPassword_Result"/> to convert.</param>
        public static aspnet_Membership_GetPassword_Result ToEntity(this dtoaspnet_Membership_GetPassword_Result dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Membership_GetPassword_Result();

            entity.Column1 = dto.Column1;
            entity.Column2 = dto.Column2;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Membership_GetPassword_Result"/> to an instance of <see cref="dtoaspnet_Membership_GetPassword_Result"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Membership_GetPassword_Result"/> to convert.</param>
        public static dtoaspnet_Membership_GetPassword_Result ToDTO(this aspnet_Membership_GetPassword_Result entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Membership_GetPassword_Result();

            dto.Column1 = entity.Column1;
            dto.Column2 = entity.Column2;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Membership_GetPassword_Result"/> to an instance of <see cref="aspnet_Membership_GetPassword_Result"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Membership_GetPassword_Result> ToEntities(this IEnumerable<dtoaspnet_Membership_GetPassword_Result> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Membership_GetPassword_Result"/> to an instance of <see cref="dtoaspnet_Membership_GetPassword_Result"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Membership_GetPassword_Result> ToDTOs(this IEnumerable<aspnet_Membership_GetPassword_Result> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Membership_GetPassword_Result"/> to an instance of <see cref="dtoaspnet_Membership_GetPassword_Result"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Membership_GetPassword_Result> ToQueryDTOs(this IQueryable<aspnet_Membership_GetPassword_Result> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
