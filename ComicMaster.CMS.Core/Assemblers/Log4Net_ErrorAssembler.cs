using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="Log4Net_Error"/> and <see cref="dtoLog4Net_Error"/>.
    /// </summary>
    public static partial class Log4Net_ErrorAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoLog4Net_Error"/> converted from <see cref="Log4Net_Error"/>.</param>
        static partial void OnDTO(this Log4Net_Error entity, dtoLog4Net_Error dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="Log4Net_Error"/> converted from <see cref="dtoLog4Net_Error"/>.</param>
        static partial void OnEntity(this dtoLog4Net_Error dto, Log4Net_Error entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoLog4Net_Error"/> to an instance of <see cref="Log4Net_Error"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoLog4Net_Error"/> to convert.</param>
        public static Log4Net_Error ToEntity(this dtoLog4Net_Error dto)
        {
            if (dto == null) return null;

            var entity = new Log4Net_Error();

            entity.Id = dto.Id;
            entity.Date = dto.Date;
            entity.Thread = dto.Thread;
            entity.Level = dto.Level;
            entity.Logger = dto.Logger;
            entity.Message = dto.Message;
            entity.Exception = dto.Exception;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="Log4Net_Error"/> to an instance of <see cref="dtoLog4Net_Error"/>.
        /// </summary>
        /// <param name="entity"><see cref="Log4Net_Error"/> to convert.</param>
        public static dtoLog4Net_Error ToDTO(this Log4Net_Error entity)
        {
            if (entity == null) return null;

            var dto = new dtoLog4Net_Error();

            dto.Id = entity.Id;
            dto.Date = entity.Date;
            dto.Thread = entity.Thread;
            dto.Level = entity.Level;
            dto.Logger = entity.Logger;
            dto.Message = entity.Message;
            dto.Exception = entity.Exception;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoLog4Net_Error"/> to an instance of <see cref="Log4Net_Error"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<Log4Net_Error> ToEntities(this IEnumerable<dtoLog4Net_Error> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Log4Net_Error"/> to an instance of <see cref="dtoLog4Net_Error"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoLog4Net_Error> ToDTOs(this IEnumerable<Log4Net_Error> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Log4Net_Error"/> to an instance of <see cref="dtoLog4Net_Error"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoLog4Net_Error> ToQueryDTOs(this IQueryable<Log4Net_Error> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
