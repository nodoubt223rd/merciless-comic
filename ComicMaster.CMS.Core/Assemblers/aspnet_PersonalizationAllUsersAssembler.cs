using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_PersonalizationAllUsers"/> and <see cref="dtoaspnet_PersonalizationAllUsers"/>.
    /// </summary>
    public static partial class aspnet_PersonalizationAllUsersAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_PersonalizationAllUsers"/> converted from <see cref="aspnet_PersonalizationAllUsers"/>.</param>
        static partial void OnDTO(this aspnet_PersonalizationAllUsers entity, dtoaspnet_PersonalizationAllUsers dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_PersonalizationAllUsers"/> converted from <see cref="dtoaspnet_PersonalizationAllUsers"/>.</param>
        static partial void OnEntity(this dtoaspnet_PersonalizationAllUsers dto, aspnet_PersonalizationAllUsers entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_PersonalizationAllUsers"/> to an instance of <see cref="aspnet_PersonalizationAllUsers"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_PersonalizationAllUsers"/> to convert.</param>
        public static aspnet_PersonalizationAllUsers ToEntity(this dtoaspnet_PersonalizationAllUsers dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_PersonalizationAllUsers();

            entity.PathId = dto.PathId;
            entity.PageSettings = dto.PageSettings;
            entity.LastUpdatedDate = dto.LastUpdatedDate;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_PersonalizationAllUsers"/> to an instance of <see cref="dtoaspnet_PersonalizationAllUsers"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_PersonalizationAllUsers"/> to convert.</param>
        public static dtoaspnet_PersonalizationAllUsers ToDTO(this aspnet_PersonalizationAllUsers entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_PersonalizationAllUsers();

            dto.PathId = entity.PathId;
            dto.PageSettings = entity.PageSettings;
            dto.LastUpdatedDate = entity.LastUpdatedDate;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_PersonalizationAllUsers"/> to an instance of <see cref="aspnet_PersonalizationAllUsers"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_PersonalizationAllUsers> ToEntities(this IEnumerable<dtoaspnet_PersonalizationAllUsers> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_PersonalizationAllUsers"/> to an instance of <see cref="dtoaspnet_PersonalizationAllUsers"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_PersonalizationAllUsers> ToDTOs(this IEnumerable<aspnet_PersonalizationAllUsers> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_PersonalizationAllUsers"/> to an instance of <see cref="dtoaspnet_PersonalizationAllUsers"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_PersonalizationAllUsers> ToQueryDTOs(this IQueryable<aspnet_PersonalizationAllUsers> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
