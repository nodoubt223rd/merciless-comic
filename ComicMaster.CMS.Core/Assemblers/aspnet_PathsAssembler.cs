using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_Paths"/> and <see cref="dtoaspnet_Paths"/>.
    /// </summary>
    public static partial class aspnet_PathsAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Paths"/> converted from <see cref="aspnet_Paths"/>.</param>
        static partial void OnDTO(this aspnet_Paths entity, dtoaspnet_Paths dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Paths"/> converted from <see cref="dtoaspnet_Paths"/>.</param>
        static partial void OnEntity(this dtoaspnet_Paths dto, aspnet_Paths entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_Paths"/> to an instance of <see cref="aspnet_Paths"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_Paths"/> to convert.</param>
        public static aspnet_Paths ToEntity(this dtoaspnet_Paths dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_Paths();

            entity.ApplicationId = dto.ApplicationId;
            entity.PathId = dto.PathId;
            entity.Path = dto.Path;
            entity.LoweredPath = dto.LoweredPath;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_Paths"/> to an instance of <see cref="dtoaspnet_Paths"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_Paths"/> to convert.</param>
        public static dtoaspnet_Paths ToDTO(this aspnet_Paths entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_Paths();

            dto.ApplicationId = entity.ApplicationId;
            dto.PathId = entity.PathId;
            dto.Path = entity.Path;
            dto.LoweredPath = entity.LoweredPath;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_Paths"/> to an instance of <see cref="aspnet_Paths"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_Paths> ToEntities(this IEnumerable<dtoaspnet_Paths> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_Paths"/> to an instance of <see cref="dtoaspnet_Paths"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_Paths> ToDTOs(this IEnumerable<aspnet_Paths> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="Cast"/> to an instance of <see cref="dtoCast"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_Paths> ToQueryDTOs(this IQueryable<aspnet_Paths> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
