using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_WebEvent_Events"/> and <see cref="dtoaspnet_WebEvent_Events"/>.
    /// </summary>
    public static partial class aspnet_WebEvent_EventsAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_WebEvent_Events"/> converted from <see cref="aspnet_WebEvent_Events"/>.</param>
        static partial void OnDTO(this aspnet_WebEvent_Events entity, dtoaspnet_WebEvent_Events dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_WebEvent_Events"/> converted from <see cref="dtoaspnet_WebEvent_Events"/>.</param>
        static partial void OnEntity(this dtoaspnet_WebEvent_Events dto, aspnet_WebEvent_Events entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_WebEvent_Events"/> to an instance of <see cref="aspnet_WebEvent_Events"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_WebEvent_Events"/> to convert.</param>
        public static aspnet_WebEvent_Events ToEntity(this dtoaspnet_WebEvent_Events dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_WebEvent_Events();

            entity.EventId = dto.EventId;
            entity.EventTimeUtc = dto.EventTimeUtc;
            entity.EventTime = dto.EventTime;
            entity.EventType = dto.EventType;
            entity.EventSequence = dto.EventSequence;
            entity.EventOccurrence = dto.EventOccurrence;
            entity.EventCode = dto.EventCode;
            entity.EventDetailCode = dto.EventDetailCode;
            entity.Message = dto.Message;
            entity.ApplicationPath = dto.ApplicationPath;
            entity.ApplicationVirtualPath = dto.ApplicationVirtualPath;
            entity.MachineName = dto.MachineName;
            entity.RequestUrl = dto.RequestUrl;
            entity.ExceptionType = dto.ExceptionType;
            entity.Details = dto.Details;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_WebEvent_Events"/> to an instance of <see cref="dtoaspnet_WebEvent_Events"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_WebEvent_Events"/> to convert.</param>
        public static dtoaspnet_WebEvent_Events ToDTO(this aspnet_WebEvent_Events entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_WebEvent_Events();

            dto.EventId = entity.EventId;
            dto.EventTimeUtc = entity.EventTimeUtc;
            dto.EventTime = entity.EventTime;
            dto.EventType = entity.EventType;
            dto.EventSequence = entity.EventSequence;
            dto.EventOccurrence = entity.EventOccurrence;
            dto.EventCode = entity.EventCode;
            dto.EventDetailCode = entity.EventDetailCode;
            dto.Message = entity.Message;
            dto.ApplicationPath = entity.ApplicationPath;
            dto.ApplicationVirtualPath = entity.ApplicationVirtualPath;
            dto.MachineName = entity.MachineName;
            dto.RequestUrl = entity.RequestUrl;
            dto.ExceptionType = entity.ExceptionType;
            dto.Details = entity.Details;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_WebEvent_Events"/> to an instance of <see cref="aspnet_WebEvent_Events"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_WebEvent_Events> ToEntities(this IEnumerable<dtoaspnet_WebEvent_Events> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_WebEvent_Events"/> to an instance of <see cref="dtoaspnet_WebEvent_Events"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_WebEvent_Events> ToDTOs(this IEnumerable<aspnet_WebEvent_Events> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_WebEvent_Events"/> to an instance of <see cref="dtoaspnet_WebEvent_Events"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_WebEvent_Events> ToQueryDTOs(this IQueryable<aspnet_WebEvent_Events> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
