using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> and <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
    /// </summary>
    public static partial class aspnet_UsersInRoles_RemoveUsersFromRoles_ResultAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> converted from <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.</param>
        static partial void OnDTO(this aspnet_UsersInRoles_RemoveUsersFromRoles_Result entity, dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> converted from <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.</param>
        static partial void OnEntity(this dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result dto, aspnet_UsersInRoles_RemoveUsersFromRoles_Result entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to an instance of <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to convert.</param>
        public static aspnet_UsersInRoles_RemoveUsersFromRoles_Result ToEntity(this dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_UsersInRoles_RemoveUsersFromRoles_Result();

            entity.Column1 = dto.Column1;
            entity.Name = dto.Name;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to an instance of <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to convert.</param>
        public static dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result ToDTO(this aspnet_UsersInRoles_RemoveUsersFromRoles_Result entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result();

            dto.Column1 = entity.Column1;
            dto.Name = entity.Name;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to an instance of <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_UsersInRoles_RemoveUsersFromRoles_Result> ToEntities(this IEnumerable<dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to an instance of <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result> ToDTOs(this IEnumerable<aspnet_UsersInRoles_RemoveUsersFromRoles_Result> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_UsersInRoles_RemoveUsersFromRoles_Result"/> to an instance of <see cref="dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_UsersInRoles_RemoveUsersFromRoles_Result> ToQueryDTOs(this IQueryable<aspnet_UsersInRoles_RemoveUsersFromRoles_Result> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
