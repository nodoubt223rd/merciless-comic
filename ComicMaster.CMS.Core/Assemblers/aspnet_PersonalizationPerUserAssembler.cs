using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_PersonalizationPerUser"/> and <see cref="dtoaspnet_PersonalizationPerUser"/>.
    /// </summary>
    public static partial class aspnet_PersonalizationPerUserAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_PersonalizationPerUser"/> converted from <see cref="aspnet_PersonalizationPerUser"/>.</param>
        static partial void OnDTO(this aspnet_PersonalizationPerUser entity, dtoaspnet_PersonalizationPerUser dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_PersonalizationPerUser"/> converted from <see cref="dtoaspnet_PersonalizationPerUser"/>.</param>
        static partial void OnEntity(this dtoaspnet_PersonalizationPerUser dto, aspnet_PersonalizationPerUser entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_PersonalizationPerUser"/> to an instance of <see cref="aspnet_PersonalizationPerUser"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_PersonalizationPerUser"/> to convert.</param>
        public static aspnet_PersonalizationPerUser ToEntity(this dtoaspnet_PersonalizationPerUser dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_PersonalizationPerUser();

            entity.Id = dto.Id;
            entity.PathId = dto.PathId;
            entity.UserId = dto.UserId;
            entity.PageSettings = dto.PageSettings;
            entity.LastUpdatedDate = dto.LastUpdatedDate;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_PersonalizationPerUser"/> to an instance of <see cref="dtoaspnet_PersonalizationPerUser"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_PersonalizationPerUser"/> to convert.</param>
        public static dtoaspnet_PersonalizationPerUser ToDTO(this aspnet_PersonalizationPerUser entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_PersonalizationPerUser();

            dto.Id = entity.Id;
            dto.PathId = entity.PathId;
            dto.UserId = entity.UserId;
            dto.PageSettings = entity.PageSettings;
            dto.LastUpdatedDate = entity.LastUpdatedDate;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_PersonalizationPerUser"/> to an instance of <see cref="aspnet_PersonalizationPerUser"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_PersonalizationPerUser> ToEntities(this IEnumerable<dtoaspnet_PersonalizationPerUser> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_PersonalizationPerUser"/> to an instance of <see cref="dtoaspnet_PersonalizationPerUser"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_PersonalizationPerUser> ToDTOs(this IEnumerable<aspnet_PersonalizationPerUser> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_PersonalizationPerUser"/> to an instance of <see cref="dtoaspnet_PersonalizationPerUser"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_PersonalizationPerUser> ToQueryDTOs(this IQueryable<aspnet_PersonalizationPerUser> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
