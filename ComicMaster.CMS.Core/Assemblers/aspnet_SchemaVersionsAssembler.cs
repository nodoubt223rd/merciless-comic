using System;
using System.Collections.Generic;
using System.Linq;

using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Core.Assemblers
{

    /// <summary>
    /// Assembler for <see cref="aspnet_SchemaVersions"/> and <see cref="dtoaspnet_SchemaVersions"/>.
    /// </summary>
    public static partial class aspnet_SchemaVersionsAssembler
    {
        /// <summary>
        /// Invoked when <see cref="ToDTO"/> operation is about to return.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_SchemaVersions"/> converted from <see cref="aspnet_SchemaVersions"/>.</param>
        static partial void OnDTO(this aspnet_SchemaVersions entity, dtoaspnet_SchemaVersions dto);

        /// <summary>
        /// Invoked when <see cref="ToEntity"/> operation is about to return.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_SchemaVersions"/> converted from <see cref="dtoaspnet_SchemaVersions"/>.</param>
        static partial void OnEntity(this dtoaspnet_SchemaVersions dto, aspnet_SchemaVersions entity);

        /// <summary>
        /// Converts this instance of <see cref="dtoaspnet_SchemaVersions"/> to an instance of <see cref="aspnet_SchemaVersions"/>.
        /// </summary>
        /// <param name="dto"><see cref="dtoaspnet_SchemaVersions"/> to convert.</param>
        public static aspnet_SchemaVersions ToEntity(this dtoaspnet_SchemaVersions dto)
        {
            if (dto == null) return null;

            var entity = new aspnet_SchemaVersions();

            entity.Feature = dto.Feature;
            entity.CompatibleSchemaVersion = dto.CompatibleSchemaVersion;
            entity.IsCurrentVersion = dto.IsCurrentVersion;

            dto.OnEntity(entity);

            return entity;
        }

        /// <summary>
        /// Converts this instance of <see cref="aspnet_SchemaVersions"/> to an instance of <see cref="dtoaspnet_SchemaVersions"/>.
        /// </summary>
        /// <param name="entity"><see cref="aspnet_SchemaVersions"/> to convert.</param>
        public static dtoaspnet_SchemaVersions ToDTO(this aspnet_SchemaVersions entity)
        {
            if (entity == null) return null;

            var dto = new dtoaspnet_SchemaVersions();

            dto.Feature = entity.Feature;
            dto.CompatibleSchemaVersion = entity.CompatibleSchemaVersion;
            dto.IsCurrentVersion = entity.IsCurrentVersion;

            entity.OnDTO(dto);

            return dto;
        }

        /// <summary>
        /// Converts each instance of <see cref="dtoaspnet_SchemaVersions"/> to an instance of <see cref="aspnet_SchemaVersions"/>.
        /// </summary>
        /// <param name="dtos"></param>
        /// <returns></returns>
        public static List<aspnet_SchemaVersions> ToEntities(this IEnumerable<dtoaspnet_SchemaVersions> dtos)
        {
            if (dtos == null) return null;

            return dtos.Select(e => e.ToEntity()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_SchemaVersions"/> to an instance of <see cref="dtoaspnet_SchemaVersions"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static List<dtoaspnet_SchemaVersions> ToDTOs(this IEnumerable<aspnet_SchemaVersions> entities)
        {
            if (entities == null) return null;

            return entities.Select(e => e.ToDTO()).ToList();
        }

        /// <summary>
        /// Converts each instance of <see cref="aspnet_SchemaVersions"/> to an instance of <see cref="dtoaspnet_SchemaVersions"/>.
        /// </summary>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IQueryable<dtoaspnet_SchemaVersions> ToQueryDTOs(this IQueryable<aspnet_SchemaVersions> entities)
        {
            if (entities == null)
                return null;

            return entities.Select(e => e.ToDTO()).AsQueryable();
        }
    }
}
