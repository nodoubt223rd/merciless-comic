﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

using log4net;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core.Assemblers;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Core
{
    public class TranscriptService : IService<dtoTranscript, Transcript>, ITranscriptService
    {
        private readonly IObjectSet<Transcript> _objectSet;

        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public IObjectSet<Transcript> ObjectSet
        {
            get { return _objectSet;}
        }

        public TranscriptService()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public TranscriptService(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<Transcript>();
        }

        public void Add(dtoTranscript entity) 
        {
            try
            {
                this.ObjectSet.AddObject(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Adding the new Transcript for - " + entity.Comic.Title + " ", e);
            }
        }

        public string AddRetString(dtoTranscript entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {

                    if (entity.Slug == null)
                        entity.Slug = Slugify.SeoFriendly(true, entity.Comic.Title.Substring(0, 39) + " Transcript");

                    entity.ScriptText = StringExtensions.CleanPostBody(entity.ScriptText);

                    context.Transcripts.Add(entity.ToEntity());
                    context.SaveChanges();
                }
                return Constants.Success;
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Adding the new transcript for " + entity.Comic.Title + " - ", e);
                return Constants.Fail;
            }
        }
        
        public void Attach(dtoTranscript entity) 
        {
            try
            {
                this.ObjectSet.Attach(entity.ToEntity());
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Attaching the new Transcript for - " + entity.Comic.Title + " ", e);
            }
        }

        public long Count(Expression<Func<Transcript, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Transcript count - ", e);
                return -1;
            }
        }

        public long Count() 
        {
            try
            {
                return this.ObjectSet.LongCount();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Transcript count - ", e);
                return -1;
            }
        }

        public void Delete(dtoTranscript entity) 
        {
            try
            {
                Transcript Transcript = entity.ToEntity();
                this.ObjectSet.DeleteObject(Transcript);
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Deleting the select Transcript - ", e);
            }
        }

        public IList<dtoTranscript> GetAll() 
        {
            try
            {
                return this.ObjectSet.ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the Transcripts list - ", e);
                return null;
            }
        }

        public IList<dtoTranscript> GetAll(Expression<Func<Transcript, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the list of Transcripts by the specified criteria - ", e);
                return null;
            }
        }

        public IEnumerable<dtoTranscript> GetAllEnumerated() 
        {
            try
            {
                return  this.ObjectSet.AsEnumerable().ToDTOs();
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Transcripts - ", e);
                return null;
            }
        }

        public IEnumerable<dtoTranscript> GetAllEnumerated(Expression<Func<Transcript, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).AsEnumerable().ToDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the enumerated list of Transcripts - ", e);
                return null;
            }
        }

        public IQueryable<dtoTranscript> GetQuery() 
        {
            try
            {
                return this.ObjectSet.AsQueryable().ToQueryDTOs();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the query-able list of Transcripts - ", e);
                return null;
            }
        }

        public bool Update(dtoTranscript entity)
        {
            try
            {
                using (ComicEntities context = new ComicEntities())
                {
                    var trans = context.Transcripts.Where(t => t.TranscriptId == entity.TranscriptId).First();

                    if (entity.Slug != trans.Slug)
                        trans.Slug = Slugify.SeoFriendly(true, entity.Slug);

                    trans.ParentComic = entity.ParentComic;
                    trans.AuthorId = entity.AuthorId;
                    trans.ScriptText = StringExtensions.CleanPostBody(entity.ScriptText);

                    context.SaveChanges();
                }

                return true;
            }
            catch(Exception e)
            {
                Log.Error("[Error]: Updating the transcript - ", e);
                return false;
            }
        }

        public dtoTranscript Single(Expression<Func<Transcript, bool>> whereCondition) 
        {
            try
            {
                return this.ObjectSet.Where(whereCondition).FirstOrDefault().ToDTO();
            }
            catch (Exception e)
            {
                Log.Error("[Error]: Getting the selected Transcript - ", e);
                return null;
            }
        }
    }
}
