﻿using System;

namespace ComicMaster.CMS.Common.Logging.ExceptionService
{
    public interface IExceptionMessageFormatter
    {
        string GetEntireExceptionStack(Exception ex);
    }
}
