﻿using System;
using System.Runtime.Serialization;

namespace ComicMaster.CMS.Common.Infrastructure.Service
{
    [DataContract]
    public class ServiceDataResponse<T> : ServiceResponse
    {
        #region Instance Fields
        [DataMember]
        public T data;
        #endregion Instance Fields

        public ServiceDataResponse()
        {
        }

        public ServiceDataResponse(T d)
        {
            this.success = true;
            data = d;
        }
    }
}
