﻿using System.Text.RegularExpressions;

namespace ComicMaster.CMS.Common.Infrastructure
{
    public interface IRegularExpressions
    {
        Regex GetExpression(string expressionName);
        bool IsMatch(string expressionName, string input);
        string Clean(string expressionName, string input);
    }
}
