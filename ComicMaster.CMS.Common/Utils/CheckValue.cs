﻿using System;

namespace ComicMaster.CMS.Common.Utils
{
    public static class CheckValue
    {
        public static bool Contains(Array array, object val)
        {
            return Array.IndexOf(array, val) != -1;
        }
    }
}
