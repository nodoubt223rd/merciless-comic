﻿using System;
using System.Configuration;

namespace ComicMaster.CMS.Common.Utils
{
    /// <summary>
    /// Configuration element which has a name.
    /// </summary>
    public class NamedConfigurationElement : ConfigurationElement
    {
        #region Constants
        private const string NAME = "name";
        #endregion Constants

        #region Properties
        /// <summary>
        /// The site name. (<c>name</c>.)
        /// </summary>
        [ConfigurationProperty(NAME, IsKey = true, IsRequired = true)]
        public string Name
        {
            get { return (string)base[NAME]; }
            set { base[NAME] = value; }
        }
        #endregion Properties
    }
}
