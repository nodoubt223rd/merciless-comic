﻿
namespace ComicMaster.CMS.Common.Utils.Sorting
{
    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }
}
