﻿using System;

namespace ComicMaster.CMS.Common.Utils.Sorting
{
    public class OrderByInfo
    {
        public string PropertyName { get; set; }
        public SortDirection Direction { get; set; }
        public bool Initial { get; set; }
    }
}
