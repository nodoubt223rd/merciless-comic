﻿using System;
using System.Configuration;
using System.IO;
using System.Web;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Logging;

namespace ComicMaster.CMS.Common.Utils.Uploads
{
    public static class FileUpload
    {
        private static string SaveFileName { get; set; }

        private static string CharacterPath
        {
            get { return ConfigurationManager.AppSettings["Characters"].ToString(); }
        }

        private static string CharacterPathThumb
        {
            get { return ConfigurationManager.AppSettings["CharacterThumbs"].ToString(); }
        }

        private static string ComicBannerPath
        {
            get { return ConfigurationManager.AppSettings["ComicBanner"].ToString(); }
        }

        private static string ComicFilePath
        {
            get { return ConfigurationManager.AppSettings["ComicPath"].ToString(); }
        }

        private static string ComicThumbnailPath
        {
            get { return ConfigurationManager.AppSettings["ComicThumbnailPath"].ToString(); }
        }

        private static string LinkPath
        {
            get { return ConfigurationManager.AppSettings["LinkImages"].ToString(); }
        }

        private static string UserAvatarPath
        {
            get { return ConfigurationManager.AppSettings["UserAvatar"].ToString(); }
        }

        private readonly static Logger log = new Logger();

        public static string SaveCharacter(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(CharacterPath + "/" + SaveFileName));
                return CharacterPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveCharacterThumb(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(CharacterPathThumb + "/" + SaveFileName));
                return CharacterPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveComic(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicFilePath + "/" + SaveFileName));
                return ComicFilePath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveComicBanner(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicBannerPath + "/" + SaveFileName));
                return ComicBannerPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveComicThumbNail(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(ComicThumbnailPath + "/" + SaveFileName));
                return ComicThumbnailPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveGalleryImage(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(Constants.Gallery + SaveFileName));
                return Constants.Gallery + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveLink(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(LinkPath + "/" + SaveFileName));
                return LinkPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveUserAvatar(HttpPostedFileBase file)
        {
            try
            {
                SaveFileName = Path.GetFileName(file.FileName);

                file.SaveAs(HttpContext.Current.Server.MapPath(UserAvatarPath + "/" + SaveFileName));
                return UserAvatarPath + "/" + SaveFileName;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }

        public static string SaveUserGravatar(HttpPostedFileBase file, string email)
        {
            if (string.IsNullOrEmpty(email))
                throw new ArgumentNullException("file");

            try
            {
                return Constants.GavatarUrl + email;
            }
            catch (Exception e)
            {
                log.Error("[Error]: File upload failed - ", e);
                return Constants.Fail;
            }
        }
    }
}
