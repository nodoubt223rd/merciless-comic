﻿using System;

namespace ComicMaster.CMS.Common.Validation
{
    public class DateTimeAdapter : IDateTime
    {
        public DateTime UtcNow
        {
            get
            {
                return DateTime.UtcNow;
            }
        }
    }
}
