﻿
namespace ComicMaster.CMS.Common.Validation
{
    public interface IValidator<T>
    {
        ValidationState Validate(T entity);
    }
}
