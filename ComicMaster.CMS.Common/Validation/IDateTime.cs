﻿using System;

namespace ComicMaster.CMS.Common.Validation
{
    public interface IDateTime
    {
        DateTime UtcNow { get; }
    }
}
