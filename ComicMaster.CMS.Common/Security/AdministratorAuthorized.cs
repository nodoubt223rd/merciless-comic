﻿using System.Web.Http;

namespace ComicMaster.CMS.Common.Security
{
    public class AdministratorAuthorized : AuthorizeAttribute
    {
        public AdministratorAuthorized()
        {
            Roles = "Administrator";
        }
    }
}
