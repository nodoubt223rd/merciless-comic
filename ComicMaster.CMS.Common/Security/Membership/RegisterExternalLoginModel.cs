﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ComicMaster.CMS.Common.Security.Membership
{
    public class RegisterExternalLoginModel
    {
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        public string ExternalLoginData { get; set; }
    }
}
