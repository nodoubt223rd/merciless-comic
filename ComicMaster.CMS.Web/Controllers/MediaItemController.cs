﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class MediaItemController : ApiController
    {
        private readonly IMediaLibraryService _mediaLibraryService;

        public MediaItemController()
            : this(new MediaLibraryService())
        {            
        }

        public MediaItemController(IMediaLibraryService mediaLibraryService)
        {
            _mediaLibraryService = mediaLibraryService;    
        }

        // GET api/mediaitem
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/mediaitem/5
        public string Get(int id)
        {
            return "value";
        }

        // DELETE api/mediaitem/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            if (_mediaLibraryService.Delete(id))
                return new HttpResponseMessage(HttpStatusCode.OK);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
