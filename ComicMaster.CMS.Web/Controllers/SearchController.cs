﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class SearchController : ApiController
    {
        private readonly ISearchService _searchService;

        public SearchController()
            : this(new SearchService())
        {
        }

        public SearchController(ISearchService searchService)
        {
            _searchService = searchService;
            
        }

        // GET api/search/5
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<dtoSearchResults> Get(string id)
        {
            dtoSearchResults results = _searchService.SearchContet(id);

            if (results != null)
                return new ServiceDataResponse<dtoSearchResults>(results);
            else
            {
                ServiceDataResponse<dtoSearchResults> fail = new ServiceDataResponse<dtoSearchResults>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }
    }
}
