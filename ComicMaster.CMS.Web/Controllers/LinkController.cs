﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class LinkController : ApiController
    {
        private readonly ILinkService _linkService;

        public LinkController()
            :this (new LinkService())
        {            
        }

        public LinkController(ILinkService linkService)
        {
            _linkService = linkService;
        }

        // GET api/link
        [HttpGet]
        public ServiceDataResponse<IList<dtoLink>> Get()
        {
            IList<dtoLink> _links = _linkService.GetAll();

            if (_links != null)
            {
                return new ServiceDataResponse<IList<dtoLink>>(_links);
            }
            else
            {
                ServiceDataResponse<IList<dtoLink>> fail = new ServiceDataResponse<IList<dtoLink>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // DELETE api/link/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            if (_linkService.Delete(id))
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);            
        }

    }
}
