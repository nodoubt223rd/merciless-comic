﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class MediaTypeController : ApiController
    {
        private readonly IMediaTypeService _mediaTypeService;

        public MediaTypeController()
            : this(new MediaTypeService())
        {            
        }

        public MediaTypeController(IMediaTypeService mediaTypeService)
        {
            _mediaTypeService = mediaTypeService;
        }

        // GET api/mediatype
        [HttpGet]
        public ServiceDataResponse<IList<dtoMediaType>> Get()
        {
            IList<dtoMediaType> _mediaTypes = _mediaTypeService.GetAll();

            if (_mediaTypes != null)
            {
                return new ServiceDataResponse<IList<dtoMediaType>>(_mediaTypes);
            }
            else
            {
                ServiceDataResponse<IList<dtoMediaType>> fail = new ServiceDataResponse<IList<dtoMediaType>>();

                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // GET api/mediatype/5
        [HttpGet]
        public ServiceDataResponse<dtoMediaType> Get(int id)
        {
            var _mediaType = _mediaTypeService.Single(mt => mt.TypeId == id);

            if (_mediaType != null)
                return new ServiceDataResponse<dtoMediaType>(_mediaType);
            else
            {
                ServiceDataResponse<dtoMediaType> fail = new ServiceDataResponse<dtoMediaType>();

                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }       

        // DELETE api/mediatype/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            if (_mediaTypeService.Delete(id))
                return new HttpResponseMessage(HttpStatusCode.OK);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
