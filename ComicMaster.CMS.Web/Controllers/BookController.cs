﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class BookController : ApiController
    {
        private readonly IBookService _bookService;

        public BookController()
            : this(new BookService())
        {
        }

        public BookController(IBookService bookService)
        {
            _bookService = bookService;
        }

        // GET api/book
        [HttpGet]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ServiceDataResponse<IList<dtoBook>> Get()
        {
            return new ServiceDataResponse<IList<dtoBook>>(_bookService.GetAll());
        }

        // GET api/book/5
        public string Get(int id)
        {
            return "value";
        }

        // DELETE api/book/5
        [System.Web.Http.AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [System.Web.Http.HttpDelete]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            bool success = _bookService.Delete(id);

            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
