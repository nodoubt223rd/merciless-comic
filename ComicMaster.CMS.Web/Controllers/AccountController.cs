﻿using System;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;
using System.Web.Security;

using ComicMaster.CMS.Common.Security.Membership;
using ComicMaster.CMS.Common.Security.Sessions;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;

using WebMatrix.WebData;

namespace ComicMaster.CMS.Web.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private readonly IAuthorService _authorService;
        private readonly IMembershipService _membershipService;
        private readonly IUserService _userService;
        private readonly IBlogService _blogService;

        public AccountController() 
            : this (new AuthorService(), new BlogService(), new MembershipService(), new UserService())
        {
        }

        public AccountController(IAuthorService authorService, IBlogService blogService, IMembershipService membershipService, IUserService userService)
        {
            _authorService = authorService;
            _blogService = blogService;
            _membershipService = membershipService;
            _userService = userService;
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.RetunUrl = returnUrl;

            return View(new LoginModel());
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            var remember = model.RememberMe;
            returnUrl = ConfigurationManager.AppSettings["adminRoot"].ToString();            

            if(ModelState.IsValid && _membershipService.ValidateUser(model))
            {
                var user = _userService.Single(u => u.UserName == model.UserName);
                var member = _membershipService.Single(m => m.UserId == user.UserId);
                var author = _authorService.Single(a => a.AuthorID == member.AuthorId);
                var blog = _blogService.Single(b => b.AuthorId == author.AuthorID);

                int blogId = 0;

                if (blog != null)
                    blogId = blog.Id;

                CP_Session UserSession = new CP_Session()
                {
                    AuthorId = author.AuthorID,
                    Avatar = author.Avatar,
                    BlogId = blogId,
                    FirstName = member.FirstName,
                    LastName = member.LastName,
                    UserId = member.UserId,
                    Vistied = 0
                };

                Session["AdminUser"] = UserSession;

                FormsAuthentication.SetAuthCookie((model.UserName), remember);

                return RedirectToLocal(returnUrl);
            }

            // If we got this far, something failed, redisplay from
            ModelState.AddModelError("", "The user name or password provided is incorrect.");

            return View(model);
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            WebSecurity.Logout();

            return RedirectToAction("Login", "Account");
        }        

        public void BuildSession(string id)
        {
            var user = _userService.Single(u => u.UserName == id);
            var member = _membershipService.Single(m => m.UserId == user.UserId);
            var author = _authorService.Single(a => a.AuthorID == member.AuthorId);

            CP_Session UserSession = new CP_Session()
            {
                AuthorId = author.AuthorID,
                Avatar = author.Avatar,
                FirstName = member.FirstName,
                LastName = member.LastName,
                UserId = member.UserId
            };


            Session["AdminUser"] = UserSession;

            //return RedirectToLocal(returnUrl);
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
        }        

        private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        {
            // See http://go.microsoft.com/fwlink/?LinkID=177550 for
            // a full list of status codes.
            switch (createStatus)
            {
                case MembershipCreateStatus.DuplicateUserName:
                    return "User name already exists. Please enter a different user name.";

                case MembershipCreateStatus.DuplicateEmail:
                    return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

                case MembershipCreateStatus.InvalidPassword:
                    return "The password provided is invalid. Please enter a valid password value.";

                case MembershipCreateStatus.InvalidEmail:
                    return "The e-mail address provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidAnswer:
                    return "The password retrieval answer provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidQuestion:
                    return "The password retrieval question provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.InvalidUserName:
                    return "The user name provided is invalid. Please check the value and try again.";

                case MembershipCreateStatus.ProviderError:
                    return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                case MembershipCreateStatus.UserRejected:
                    return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

                default:
                    return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
            }
        }
        #endregion
    }
}
