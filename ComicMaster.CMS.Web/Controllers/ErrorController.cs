﻿using System;
using System.Linq;
using System.Web.Mvc;

namespace ComicMaster.CMS.Web.Controllers
{
    public class ErrorController : Controller
    {
        public ActionResult Index(int? status, Exception error)
        {
            if (status.HasValue)
            {
                Response.StatusCode = status.Value;
                Response.TrySkipIisCustomErrors = true;
            }
            else
            {
                Response.StatusCode = 404;
                Response.TrySkipIisCustomErrors = true;
                status = 404;
            }


            return View(status);
        }

        #region Dashboard Errors
        public ActionResult ErrorIndex(int? status, Exception error)
        {
            if (status.HasValue)
            {
                Response.StatusCode = status.Value;
                Response.TrySkipIisCustomErrors = true;
            }

            return View(status);
        }
        #endregion Dashboard Errors
    }
}
