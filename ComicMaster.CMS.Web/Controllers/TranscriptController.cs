﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ComicMaster.CMS.Web.Controllers
{
    public class TranscriptController : ApiController
    {
        // GET api/transcript
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/transcript/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/transcript
        public void Post([FromBody]string value)
        {
        }

        // PUT api/transcript/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/transcript/5
        public void Delete(int id)
        {
        }
    }
}
