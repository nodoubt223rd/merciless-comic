﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

using MvcPaging;

namespace ComicMaster.CMS.Web.Controllers
{
    public class ComicController : ApiController
    {
        private readonly IAuthorService _authorService;
        private readonly IBookService _bookService;
        private readonly ICastService _castService;
        private readonly IChapterService _chapterService;
        private readonly IComicService _comicService;
        private readonly IMembershipService _membershipService;
        private readonly IUserService _userService;
        
        public ComicController()
            : this(new AuthorService(), new BookService(), new CastService(), new ChapterService(), new ComicService(), new MembershipService(), new UserService())
        {            
        }

        public ComicController(IAuthorService authorService, IBookService bookService, ICastService castService, IChapterService chapterService, IComicService comicService, IMembershipService membershipService, IUserService userService)
        {
            _authorService = authorService;
            _bookService = bookService;
            _castService = castService;
            _chapterService = chapterService;
            _comicService = comicService;
            _membershipService = membershipService;
            _userService = userService;            
        }
        //
        // GET: /Comic/
        [System.Web.Http.HttpGet]
        [AllowAnonymous]
        public ServiceDataResponse<IList<dtoComic>> Get()
        {
            IList<dtoComic> _comics = _comicService.GetAll();
            if (_comics != null)
                return new ServiceDataResponse<IList<dtoComic>>(_comics);
            else
            {
                ServiceDataResponse<IList<dtoComic>> fail = new ServiceDataResponse<IList<dtoComic>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        [System.Web.Http.HttpGet]
        [AllowAnonymous]
        public ServiceDataResponse <dtoComic> Get(string id)
        {
            dtoComic _comic = _comicService.Single(c => c.ComicSlug == id);

            if (_comic != null)
                return new ServiceDataResponse<dtoComic>(_comic);
            else
            {
                ServiceDataResponse<dtoComic> fail = new ServiceDataResponse<dtoComic>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }
        
        [HttpGet]
        public ServiceDataResponse<dtoCurrentPage> GetCurrent()
        {
            dtoCurrentPage current = _comicService.CurrentComic();

            if (current != null)
            {

                IPagedList<dtoComic> comicList = _comicService.ComicDisplay().ToPagedList(1, 1);
                current.FirstPage = comicList.PageCount;
                
                return new ServiceDataResponse<dtoCurrentPage>(current);
            }
            else
            {
                ServiceDataResponse<dtoCurrentPage> fail = new ServiceDataResponse<dtoCurrentPage>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }           
        }


        // POST api/author
        [Authorize(Roles = "Administrator")]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/author/5
        [Authorize(Roles = "Administrator, Content Editor")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/author/5
        [HttpDelete]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            bool success = _comicService.Delete(id);

            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
