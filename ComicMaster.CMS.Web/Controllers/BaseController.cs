﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Security;
using System.Web.Http;
using System.Web.Mvc;

using ComicMaster.CMS.Common.Security.Sessions;
using ComicMaster.CMS.Core.CM_Principal;

namespace ComicMaster.CMS.Web.Controllers
{
    public class BaseController : Controller
    {
        protected virtual new CustomPrincipal User
        {
            get { return HttpContext.User as CustomPrincipal; }
        }

        /// <summary>
        /// Gets the current site session.
        /// </summary>
        public CP_Session CurrentSiteSession
        {
            get
            {
                CP_Session siteSession = (CP_Session)this.Session["AdminSession"];
                return siteSession;
            }
        }
    }
}
