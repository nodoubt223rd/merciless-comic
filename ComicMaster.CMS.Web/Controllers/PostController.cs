﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class PostController : ApiController
    {
        private readonly IPostService _postService;

        public PostController()
            : this(new PostService())
        {
        }

        public PostController(IPostService postService)
        {
            _postService = postService;
        }

        // GET api/post
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<IList<dtoPost>> GetAll(int? id)
        {
            if (id.HasValue)
            {
                var _posts = _postService.GetAll(id.Value);

                if (_posts != null)
                {
                    return new ServiceDataResponse<IList<dtoPost>>(_posts);
                }
                else
                {
                    ServiceDataResponse<IList<dtoPost>> fail = new ServiceDataResponse<IList<dtoPost>>();
                    fail.data = null;
                    fail.message = ResponseMessage.ApiFail;
                    fail.success = false;

                    return fail;
                }
            }
            else
            {
                var _posts = _postService.GetAll();

                if (_posts != null)
                {
                    return new ServiceDataResponse<IList<dtoPost>>(_posts);
                }
                else
                {
                    ServiceDataResponse<IList<dtoPost>> fail = new ServiceDataResponse<IList<dtoPost>>();
                    fail.data = null;
                    fail.message = ResponseMessage.ApiFail;
                    fail.success = false;

                    return fail;
                }
            }
        }

        // GET api/post
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<IList<dtoPost>> Get()
        {
            var _posts = _postService.GetAll();

            if (_posts != null)
            {
                return new ServiceDataResponse<IList<dtoPost>>(_postService.GetAll());
            }
            else
            {
                ServiceDataResponse<IList<dtoPost>> fail = new ServiceDataResponse<IList<dtoPost>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // GET api/post/5
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<dtoPost> Get(int id)
        {
            var post = _postService.Single(x => x.Id == id);

            if (post != null)
            {
                return new ServiceDataResponse<dtoPost>(post);
            }
            else
            {
                ServiceDataResponse<dtoPost> fail = new ServiceDataResponse<dtoPost>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage Delete(int id)
        {
            bool success = _postService.Delete(id);
            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
