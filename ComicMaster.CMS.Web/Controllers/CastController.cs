﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;


namespace ComicMaster.CMS.Web.Controllers
{
    public class CastController : ApiController
    {
        private readonly IBookChapterService _bookChapterService;
        private readonly IBookService _bookService;
        private readonly ICastService _castService;
        private readonly IChapterService _chapterService;
        private readonly IComicCastService _comicCastService;
        private readonly IComicService _comicService;

        public CastController()
            : this(new BookChapterService(), new BookService(), new CastService(), new ChapterService(), new ComicCastService(), new ComicService())
        {
        }

        public CastController(IBookChapterService bookChapterService, IBookService bookService, ICastService castService, IChapterService chapterService, IComicCastService comicCastService, IComicService comicService)
        {
            _bookChapterService = bookChapterService;
            _bookService = bookService;
            _castService = castService;
            _chapterService = chapterService;
            _comicCastService = comicCastService;
            _comicService = comicService;
        }

        // GET api/cast/5
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<dtoCast> Get(int id)
        {
            dtoCast character = _castService.Single(c => c.CastID == id);

            if (character != null)
            {
                var characterApperance = _comicCastService.GetAll(a => a.CastID == id);

                character.ComicCasts = new List<dtoComicCast>(characterApperance);

                var comics = new List<dtoComic>();

                foreach (var apperance in characterApperance)
                {
                    dtoComic comic = new dtoComic();
                    comic = _comicService.Single(c => c.ComicID == apperance.ComicID);

                    comics.Add(comic);
                }

                character.Apperances = new List<dtoComic>(comics);

                return new ServiceDataResponse<dtoCast>(character);
            }
            else
            {
                ServiceDataResponse<dtoCast> fail = new ServiceDataResponse<dtoCast>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // POST api/cast
        [System.Web.Http.HttpPost]
        [Authorize(Roles = "Administrator, Content Editor")]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/cast/5
        [System.Web.Http.HttpPut]
        [Authorize(Roles = "Administrator, Content Editor")]
        public void Put(int id, [FromBody]string value, dtoNewCharacter model)
        {
        }

        // DELETE api/cast/5
        [System.Web.Http.AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [System.Web.Http.HttpDelete]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            var character = _castService.Single(c => c.CastID == id);
            bool success = _castService.DeleteRetBool(character);

            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
