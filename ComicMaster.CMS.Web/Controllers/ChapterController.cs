﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class ChapterController : ApiController
    {
        private readonly IBookChapterService _bookChapterService;
        private readonly IBookService _bookService;
        private readonly IChapterService _chapterService;

        public ChapterController()
            : this(new BookChapterService(), new BookService(), new ChapterService())
        {
        }

        public ChapterController(IBookChapterService bookChapterService, IBookService bookService, IChapterService chapterService)
        {
            _bookChapterService = bookChapterService;
            _bookService = bookService;
            _chapterService = chapterService; 
        }

        // GET api/article
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ServiceDataResponse<IList<dtoChapter>> Get()
        {
            return new ServiceDataResponse<IList<dtoChapter>>(_chapterService.GetAll());
        }

        // GET api/chapter/5
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ServiceDataResponse<IList<dtoChapter>> Get(int id)
        {
            var _bookChapers = _bookChapterService.GetAll(x => x.BookID == id);

            List<dtoChapter> chapters = new List<dtoChapter>();

            foreach (var _chapter in _bookChapers)
            {
                var chapter = _chapterService.Single(c => c.ChapterID == _chapter.ChapterID);

                chapters.Add(chapter);
            }

            return new ServiceDataResponse<IList<dtoChapter>>(chapters);
        }

        // POST api/chapter
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/chapter/5
        [HttpPut]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/chapter/5
        [System.Web.Http.AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [System.Web.Http.HttpDelete]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            bool success = _chapterService.Delete(id);

            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
