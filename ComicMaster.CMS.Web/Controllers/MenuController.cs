﻿using System;
using System.Linq;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class MenuController : ApiController
    {
        private readonly IMenuService _menuService;

        public MenuController()
            : this(new MenuService())
        {
        }

        public MenuController(IMenuService menuservice)
        {
            _menuService = menuservice;
        }

        // Get api/menu
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<dtoMenu> Get()
        {
            dtoMenu _menu = _menuService.GetMenu();
            if (_menu != null)
                return new ServiceDataResponse<dtoMenu>(_menu);
            else
            {

                ServiceDataResponse<dtoMenu> fail = new ServiceDataResponse<dtoMenu>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }
    }
}
