﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Security.Sessions;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Web.Infrastructure;

using WebMatrix.WebData;

namespace ComicMaster.CMS.Web.Controllers
{
    public class DashBoardController : BaseController
    {
        #region Service calls
        private readonly IAuthorService _authorService;
        private readonly IAuthorTypeService _authorTypeService;
        private readonly IBlogService _blogService;
        private readonly IBookChapterService _bookChapterService;
        private readonly IBookService _bookService;
        private readonly ICategoryService _categoryService; 
        private readonly ICastService _castService;
        private readonly IChapterService _chapterService;
        private readonly IComicCastService _comicCastService;
        private readonly IComicService _comicService;
        private readonly ILinkService _linkService;
        private readonly IMediaLibraryService _mediaLibraryService;
        private readonly IMediaTypeService _mediaTypeService;
        private readonly IMembershipService _memberService;
        private readonly IMenuService _menuService;
        private readonly INewsService _newsService;
        private readonly IPostService _postService;
        private readonly IPostTagMapService _postTagMapService;
        private readonly IRoleService _roleService;
        private readonly ITagService _tagService;
        private readonly ITranscriptService _transcriptService;
        private readonly IUserService _userService;
        private readonly IUsersInRolesService _usersInRolesService;

        #endregion Service calls

        public DashBoardController()
            : this(new AuthorService(), new AuthorTypeService(), new BlogService(), new BookChapterService(), new BookService(), new CategoryService(), new CastService(), new ChapterService(), new ComicCastService(), new ComicService(), new LinkService(), new MediaLibraryService(), new MediaTypeService(), new MembershipService(), new MenuService(), new NewsService(), new PostService(), new PostTagMapService(), new RoleService(), new TagService(), new TranscriptService(), new UserService(), new UsersInRolesService())
        {
        }

        public DashBoardController(IAuthorService authorService, IAuthorTypeService authorTypeService, IBlogService blogService, IBookChapterService bookChapterService, IBookService bookService, ICategoryService categoryService, ICastService castService, IChapterService chapterService, IComicCastService comicCastService, IComicService comicService, ILinkService linkService, IMediaLibraryService mediaLibraryService, IMediaTypeService mediaTypeService, IMembershipService memberService, IMenuService menuservice, INewsService newsService, IPostService postService, IPostTagMapService postTagMapService, IRoleService roleService, ITagService tagService, ITranscriptService transcriptService, IUserService userService, IUsersInRolesService usersInRolesService)
        {
            _authorService = authorService;
            _authorTypeService = authorTypeService;
            _blogService = blogService;
            _bookChapterService = bookChapterService;
            _bookService = bookService;
            _categoryService = categoryService;
            _castService = castService;
            _chapterService = chapterService;
            _comicCastService = comicCastService;
            _comicService = comicService;
            _linkService = linkService;
            _mediaLibraryService = mediaLibraryService;
            _mediaTypeService = mediaTypeService;
            _memberService = memberService;
            _menuService = menuservice;
            _newsService = newsService;
            _postService = postService;
            _postTagMapService = postTagMapService;
            _roleService = roleService;
            _tagService = tagService;
            _transcriptService = transcriptService;
            _userService = userService;
            _usersInRolesService = usersInRolesService;
        }

        #region Articles
        public ActionResult Article(int id)
        {
            var news = _newsService.Single(n => n.NewsId == id);

            news.Comics = new List<dtoComic>(_comicService.GetAll());

            return View(news);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor ")]
        [ValidateAntiForgeryToken]
        public ActionResult Article(dtoNews news, FormCollection form)
        {
            return View(news);
        }

        public ActionResult Articles()
        {
            return View();
        }

        public ActionResult AddArticles()
        {
            dtoNews news = new dtoNews();

            news.Comics = new List<dtoComic>(_comicService.GetAll());
            news.NewsDate = DateTime.Now;

            return View(news);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor ")]
        [ValidateAntiForgeryToken]
        public ActionResult AddArticles(dtoNews news, FormCollection form)
        {
            return View(news);
        }
        #endregion Articles

        #region Authors
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Account()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult AddAuthor()
        {
            return View();
        }

        [Authorize(Roles = "Administrator")]
        public ActionResult Authors(int? id)
        {
            if (id.HasValue)
            {
                bool success = _memberService.DeleteRetBool(id.Value);
                if (success == true)
                    ViewData["Delete"] = Constants.Success;
                else
                    ViewData["Delete"] = Constants.Fail;
            }
            else
                ViewData["Delete"] = null;

            IList<dtoaspnet_Membership> _authors = _memberService.GetAll(a => a.IsApproved == true);

            if (_authors.Count > 0)
            {
                foreach (var author in _authors)
                {
                    author.aspnet_Users = new dtoaspnet_Users();

                    var user = _userService.Single(u => u.UserId == author.UserId);

                    if (user == null)
                    {
                    }
                    else
                        author.aspnet_Users.UserName = user.UserName;
                }

                return View(_authors);
            }
            else
            {
                return View(_authors);
            }            
        }

        [HttpGet]
        [Authorize(Roles = "Administrator")]
        public ActionResult CreateAccount()
        {
            dtoNewAccount model = new dtoNewAccount();
            var roles = _roleService.GetAll(r => r.RoleName != "Anonymous");
            var authorTypes = _authorTypeService.GetAll();

            model.AuthorTypes = new List<dtoAuthorType>();
            model.Roles = new Dictionary<string, string>();

            foreach (var authorType in authorTypes)
            {
                model.AuthorTypes.Add(authorType);
            }

            foreach(var role in roles)
            {
                model.Roles.Add(role.RoleId.ToString(), role.RoleName);
            }

            ViewData["Success"] = string.Empty;
            return View(model);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult CreateAccount(dtoNewAccount model, FormCollection collection)
        {
            bool OK = false;
            var _date = DateTime.Now;
            dtoaspnet_Users _model = new dtoaspnet_Users();

            _model.aspnet_UsersInRoles = new List<dtoaspnet_UsersInRoles>();
            _model.aspnet_Membership = new dtoaspnet_Membership();
            _model.aspnet_Membership.Author = new dtoAuthor();

            if (collection != null || collection.Count > 0)
            {
                model.Roles = new Dictionary<string, string>();

                if (collection[Constants.Administrator] != null)
                {
                    Guid newRole = new Guid(collection[Constants.Administrator]);

                    var adminRole = _roleService.Single(r => r.RoleId == newRole);

                    model.Roles.Add(adminRole.RoleId.ToString(), adminRole.RoleName);
                }

                if (collection[Constants.ContentEditor] != null)
                {
                    Guid newRole = new Guid(collection[Constants.ContentEditor]);

                    var contentEditor = _roleService.Single(r => r.RoleId == newRole);

                    model.Roles.Add(contentEditor.RoleId.ToString(), contentEditor.RoleName);
                }

                if (collection[Constants.AuthorSelectedType] != null)
                {
                    int aType = 0; 
                    string _atype = collection[Constants.AuthorSelectedType];

                    if (int.TryParse(_atype, out aType))
                    {
                        _model.aspnet_Membership.Author.AuthorType = aType;
                    }
                }
            }

            if (ModelState.IsValid)
            {
                _model.UserId = Guid.NewGuid();
                _model.UserName = model.UserName;
                _model.aspnet_Membership.FirstName = model.FirstName;
                _model.aspnet_Membership.LastName = model.LastName;
                _model.aspnet_Membership.Password = model.Password;
                _model.aspnet_Membership.UserId = _model.UserId;
                _model.LoweredUserName = model.UserName.ToLowerInvariant();
                _model.aspnet_Membership.Email = model.Email;
                _model.aspnet_Membership.LoweredEmail = model.Email.ToLowerInvariant();
                _model.LastActivityDate = _date;
                _model.aspnet_Membership.IsApproved = true;
                _model.aspnet_Membership.IsLockedOut = false;

                if (model.Roles.Count > 0)
                {
                    foreach (var role in model.Roles)
                    {
                        dtoaspnet_UsersInRoles _role = new dtoaspnet_UsersInRoles();

                        _role.UserId = _model.UserId;
                        _role.RoleId = Guid.Parse(role.Key);

                        _model.aspnet_UsersInRoles.Add(_role);
                    }
                }
                OK =  _memberService.AddRetBool(_model);
            }

            if (OK)
            {
                dtoNewAccount retModel = new dtoNewAccount();
                retModel.AuthorTypes = new List<dtoAuthorType>();
                retModel.Roles = new Dictionary<string, string>();                

                ViewData["Success"] = ResponseMessage.AddUserSuccess;

                var authorTypes = _authorTypeService.GetAll();
                var roles = _roleService.GetAll(r => r.RoleName != "Anonymous");

                foreach (var authorType in authorTypes)
                {
                    retModel.AuthorTypes.Add(authorType);
                }

                foreach (var role in roles)
                {
                    retModel.Roles.Add(role.RoleId.ToString(), role.RoleName);
                }

                return View(retModel);
            }
            else
            {
                dtoNewAccount retModel = new dtoNewAccount();
                retModel.AuthorTypes = new List<dtoAuthorType>();
                retModel.Roles = new Dictionary<string, string>();  

                ViewData["Success"] = ResponseMessage.AddUserFail;

                var authorTypes = _authorTypeService.GetAll();
                var roles = _roleService.GetAll(r => r.RoleName != "Anonymous");

                foreach (var authorType in authorTypes)
                {
                    retModel.AuthorTypes.Add(authorType);
                }

                foreach (var role in roles)
                {
                    retModel.Roles.Add(role.RoleId.ToString(), role.RoleName);
                }

                return View(retModel);
            }
        }     

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult EditAccount(int id)
        {
            return View(GetMember(id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        [ValidateAntiForgeryToken]
        public ActionResult EditAccount(dtoaspnet_Membership model, FormCollection formCollection, HttpPostedFileBase fileupload, int id)
        {
            var gravatar = formCollection[Constants.UseGravatar];

            if (gravatar == "false")
                model.Author.UseGravatar = false;
            else
                model.Author.UseGravatar = true;

            if (fileupload != null)
                if (!string.IsNullOrEmpty(fileupload.FileName))
                    model.Author.Avatar = fileupload.FileName;

            model.Author.AuthorID = id;
            model.AuthorId = id;

            var success = _memberService.UpdateRetString(model, fileupload);

            ViewData[Constants.UpdateStatus] = success;

            return View(GetMember(id));
        }
        #endregion Authors

        #region Blogs

        public ActionResult AddBlog()
        {
            ViewData[Constants.AddStatus] = string.Empty;

            return View(new dtoBlog());
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddBlog(dtoBlog blog, FormCollection formCollection)
        {
            int aId;

            if (int.TryParse(formCollection["Authors"].ToString(), out aId))
                blog.AuthorId = aId;
            else
            {
                ViewData[Constants.AddStatus] = Constants.Fail;
                return View(blog);
            }

            if (_blogService.AddRetVal(blog))
                ViewData[Constants.AddStatus] = Constants.Success;
            else
                ViewData[Constants.AddStatus] = Constants.Fail;

            return View(blog);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Blog(int id)
        {
            ViewData["UpdateStatus"] = string.Empty;

            return View(_blogService.Single(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Blog(dtoBlog blog)
        {
            if (_blogService.Update(blog))
                ViewData["UpdateStatus"] = Constants.Success;
            else
                ViewData["UpdateStatus"] = Constants.Fail;

            return View(blog);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Blogs()
        {
            return View(_blogService.Blogs());
        }
        #endregion Blogs

        #region Blog Posts

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult  AddPost()
        {
            ViewData["AddStatus"] = string.Empty;

            return View(_postService.New());
        }

        [HttpPost, ValidateInput(false)]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddPost(dtoNewPost post, FormCollection form)
        {
            DateTime postedOn;
            int blogId;

            CP_Session _session = (CP_Session)System.Web.HttpContext.Current.Session["AdminUser"];

            if (null == _session)
                blogId = 0;
            else
                blogId = _session.BlogId;

            if (blogId == 0 && _session != null)
            {
                var user = _memberService.Single(u => u.UserId == _session.UserId);

                if (user != null)
                {
                    var blog = _blogService.Single(b => b.AuthorId == user.AuthorId);
                    blogId = blog.Id;
                }
            }

            if (!DateTime.TryParse(post.Publish, out postedOn))
                postedOn = DateTime.Now;

            if (post.Category == 0)
            {
                var catergory = _categoryService.Single(c => c.Name == "Uncategorized");
                post.Category = catergory.Id;
            }

            dtoPost model = new dtoPost()
            {
                BlogId = blogId,
                CategoryId = post.Category,
                LongDescription = post.LongDescription,
                ShortDescription = post.LongDescription.Substring(0, Math.Min(post.LongDescription.Length, 80)) + "...",
                Title = post.Title,
                Visibility = post.Visibility,
                PostedOn = postedOn,
                UrlSlug = Slugify.SeoFriendly(true, post.Title)
            };

            if (post.Meta != null)
                model.Meta = post.Meta;

            if (post.Status == "Live")
            {
                model.Published = true;
                model.Status = "Published";
            }
            else
            {
                model.Published = false;
                model.Status = post.Status;
            }



            if (_postService.AddRetValue(model, form["Tags"].ToString()))
                ViewData["AddStatus"] = Constants.AddStatus;
            else
                ViewData["AddStatus"] = Constants.Fail;

            return View(post);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Post(int id, int? okVal)
        {
            string updated = string.Empty;

            if (okVal.HasValue)
            {
                if (okVal.Value == 1)
                    updated = Constants.UpdateStatus;
                else
                    updated = Constants.Fail;
            }

            ViewData["UpdateStatus"] = updated;

            dtoPost post = _postService.Post(id);

            StringBuilder postTags = new StringBuilder();

            foreach (var tag in post.Tags)
            {
                postTags.Append(tag.TagId.ToString()).Append(",");
            }

            if (postTags.Length != 0)
                ViewData["PostTags"] = postTags.ToString().Substring(0, postTags.ToString().Length - 1);
            else ViewData["PostTags"] = string.Empty;

            ViewData["UsedCategory"] = post.CategoryId;

            return View(post);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Post(dtoPost post, FormCollection form)
        {
            post.LongDescription = post.LongDescription;
            int _okVal = 0;

            if (_postService.Update(post, form["Tags"].ToString()))
            {                
                _okVal = 1;
            }

            var tags = _postTagMapService.GetAll(tm => tm.PostId == post.Id);
            var ret = _postService.Single(p => p.Id == post.Id);

            StringBuilder postTags = new StringBuilder();

            if (tags.Count != 0)
            {
                foreach (var tag in tags)
                {
                    postTags.Append(tag.TagId.ToString()).Append(",");
                }
            }

            
            if (postTags.Length != 0)
                ViewData["PostTags"] = postTags.ToString().Substring(0, postTags.ToString().Length - 1);
            else
                ViewData["PostTags"] = string.Empty;

            ViewData["UsedCategory"] = ret.CategoryId;


            return RedirectToAction("Post", new { id = post.Id, okVal = _okVal});
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Posts(int id, int? page)
        {
            int start = 1;
            int pageSize;

            if (page.HasValue)
                start = page.Value;

            if (int.TryParse(ConfigurationManager.AppSettings["PageSize"], out pageSize))
                return View(_postService.Posts(id, start - 1, pageSize));
            else
                return View();
        }

        #endregion Blog Posts

        #region Books
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Book(int id)
        {
            ViewData[Constants.UpdateStatus] = Constants.Ready;

            var book = _bookService.Single(b => b.BookID == id);
            return View(book);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Book(dtoBook model, FormCollection collection)
        {
            int authorId;

            if (Int32.TryParse(collection[Constants.Authors].ToString(), out authorId))
            {
                model.AuthorID = authorId;

                if (model.BookSlug == null)
                    model.BookSlug = string.Empty;

                ViewData[Constants.UpdateStatus] = _bookService.Update(model);
            }

            var retModel = _bookService.Single(b => b.BookID == model.BookID);
            
            return View(retModel);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Books()
        {            
            return View(_bookService.GetAll());
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddBook()
        {
            ViewData[Constants.AddStatus] = Constants.Ready;
            
            return View(new dtoBook());
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddBook(dtoBook model, FormCollection collection)
        {
            int authorId;

            if (Int32.TryParse(collection[Constants.Authors].ToString(), out authorId))
            {
                model.AuthorID = authorId;

                ViewData[Constants.AddStatus] = _bookService.AddRetString(model);
            }

            return View(new dtoBook());
        }
        #endregion Books

        #region Cast
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Characters()
        {
            return View(_castService.GetAll());
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Cast(int id)
        {
            ViewData["UpdateStatus"] = "Ready";
            return View(_castService.Single(c => c.CastID == id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Cast(dtoCast model, FormCollection collection, HttpPostedFileBase imageupload, HttpPostedFileBase thumbImage)
        {
            try
            {
                if (collection[Constants.Gender] != null)
                    model.Sex = collection[Constants.Gender].ToString();

                var updated = _castService.Update(model, imageupload, thumbImage);

                ViewData["UpdateStatus"] = updated;
            }
            catch
            {
                ViewData["UpdateStatus"] = Constants.Fail;
            }

            return View(_castService.Single(c => c.CastID == model.CastID));
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddCast()
        {
            ViewData["UpdateStatus"] = "Ready";

            return View(GetNewCharacter("new"));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        [ValidateAntiForgeryToken]
        public ActionResult AddCast(dtoCast model, FormCollection collection, HttpPostedFileBase imageupload, HttpPostedFileBase thumbImage)
        {
            try
            {
                if (collection[Constants.Gender] != null)
                    model.Sex = collection[Constants.Gender].ToString();

                var addNew = _castService.Add(model, imageupload, thumbImage);
                if (addNew == Constants.Success)
                {
                    return View(GetNewCharacter(Constants.Success));
                }
                else if (addNew == Constants.Fail)
                {
                    return View(GetNewCharacter(Constants.Fail));
                }
                else
                {
                    return View(GetNewCharacter(Constants.CharacterName));
                }
            }
            catch
            {
                return View(GetNewCharacter(Constants.Fail));
            }
        }
        #endregion Cast

        #region Category
        public ActionResult Category(int id)
        {
            return View(_categoryService.Single(c => c.Id == id));
        }

        public ActionResult Categories()
        {
            return View();
        }
        #endregion Category

        #region Chapters
        public ActionResult Chapter(int id)
        {
            var bookChapter = _bookChapterService.Single(bc => bc.ChapterID == id);

            ViewData[Constants.UpdateStatus] = Constants.Ready;
            ViewData[Constants.BookChapter] = bookChapter.BookID;

            return View(_chapterService.Single(c => c.ChapterID == id));
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult Chapter(dtoChapter model, FormCollection collection)
        {
            int bookId;

            if (Int32.TryParse(collection[Constants.Books].ToString(), out bookId))
            {
                ViewData[Constants.UpdateStatus] = _chapterService.Update(model, bookId);                
            }
            model = _chapterService.Single(c => c.ChapterID == model.ChapterID);

            ViewData[Constants.BookChapter] = bookId;

            return View(model);
        }

        public ActionResult Chapters()
        {
            return View();
        }

        public ActionResult AddChapter()
        {
            ViewData[Constants.AddStatus] = Constants.Ready;

            return View(new dtoChapter());
        }

        [HttpPost, ValidateInput(false)]
        public ActionResult AddChapter(dtoChapter model, FormCollection collection)
        {
            int bookId;

            if (Int32.TryParse(collection[Constants.Books].ToString(), out bookId))
            {
                ViewData[Constants.AddStatus] = _chapterService.Add(model, bookId);
            }

            return View(new dtoChapter());
        }
        #endregion Chapters

        #region Comics
        public ActionResult Comic(int id)
        {
            ViewData["UpdateStatus"] = "Ready";

            var author = new dtoAuthor();
            var book = new dtoBook();
            var chapter = new dtoChapter();
            var castMembers = new List<dtoCast>();
            var comicCast = new List<dtoComicCast>();
            var comic = new dtoComic();
            var illustrator = new dtoaspnet_Membership();
            var writer = new dtoaspnet_Membership();

            comic.Author = new dtoAuthor();
            comic.Book = new dtoBook();
            comic.Chapter = new dtoChapter();
            comic = _comicService.Single(c => c.ComicID == id);

            author = _authorService.Single(a => a.AuthorID == comic.AuthorID);
            book = _bookService.Single(b => b.BookID == comic.BookID);
            chapter = _chapterService.Single(c => c.ChapterID == comic.ChapterID);
            comicCast = _comicCastService.GetAll(c => c.ComicID == comic.ComicID).ToList();
            illustrator = _memberService.Single(m => m.AuthorId == comic.IllustratorID);

            if (comic.IllustratorID != comic.WriterID)
            {
                writer = _memberService.Single(m => m.AuthorId == comic.WriterID);
                comic.WriterName = writer.FirstName + " " + writer.LastName;
            }
            else
                comic.WriterName = illustrator.FirstName + " " + illustrator.LastName;
            


            foreach (var _cast in comicCast)
            {
                var cast = _castService.Single(c => c.CastID == _cast.CastID);

                castMembers.Add(cast);
            }

            comic.Author = author;
            comic.Book = book;
            comic.Chapter = chapter;            
            comic.CastMembers = new List<dtoCast>(castMembers);
            comic.Characters = new List<dtoCast>(_castService.GetAll());
            comic.Chapters = new List<dtoChapter>(_chapterService.GetAll());
            comic.IllustratorName = illustrator.FirstName + " " + illustrator.LastName;

            return View(comic);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor ")]
        [ValidateAntiForgeryToken]
        public ActionResult Comic(dtoComic model, FormCollection collection, HttpPostedFileBase bannerImagefile, HttpPostedFileBase comicImagefile, HttpPostedFileBase thumbImagefile)
        {
            bool _bool;
            int _cId;

            model.LongDescription = collection[Constants.LongDescription].ToString();
            model.ShortDescription = collection[Constants.ShortDescription].ToString();

            var archived = collection[Constants.IsArchived].ToString();
            var chapterid = collection[Constants.ChapterId].ToString();
            var lastmix = collection[Constants.IsLastMix].ToString();
            var onhome = collection[Constants.OnHomePage].ToString();

            if (Boolean.TryParse(archived, out _bool))
                model.IsArchived = _bool;

            if (Boolean.TryParse(lastmix, out _bool))
                model.IsLastComic = _bool;

            if (Boolean.TryParse(onhome, out _bool))
                model.OnHomePage = _bool;

            if (Int32.TryParse(chapterid, out _cId))
                model.ChapterID = _cId;

            ViewData["UpdateStatus"] = _comicService.Update(model, bannerImagefile, comicImagefile, thumbImagefile);

            return RedirectToAction("Comic", new { id = model.ComicID });
        }

        public ActionResult Comics()
        {
            var _comics = _comicService.GetAll();

            foreach (var comic in _comics)
            {
                var author = GetMember(comic.AuthorID);

                switch (author.Author.AuthorType)
                {
                    case 1:
                        var writer = GetMember(comic.WriterID);

                        comic.IllustratorName = author.aspnet_Users.aspnet_Membership.FirstName + " " + author.aspnet_Users.aspnet_Membership.LastName;
                        comic.WriterName = writer.aspnet_Users.aspnet_Membership.FirstName + " " + writer.aspnet_Users.aspnet_Membership.LastName;
                        break;
                    case 2:
                        var illustrator = GetMember(comic.IllustratorID);

                        comic.IllustratorName = illustrator.aspnet_Users.aspnet_Membership.FirstName + " " + illustrator.aspnet_Users.aspnet_Membership.LastName;
                        comic.WriterName = author.aspnet_Users.aspnet_Membership.FirstName + " " + author.aspnet_Users.aspnet_Membership.LastName;
                        break;
                    default:
                        comic.IllustratorName = author.aspnet_Users.aspnet_Membership.FirstName + " " + author.aspnet_Users.aspnet_Membership.LastName;
                        comic.WriterName = author.aspnet_Users.aspnet_Membership.FirstName + " " + author.aspnet_Users.aspnet_Membership.LastName;
                        break;
                }
            }

            return View(_comics);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor ")]
        public ActionResult AddComic(dtoNewComicModel model, FormCollection collection, HttpPostedFileBase bannerImage, HttpPostedFileBase comicImage, HttpPostedFileBase thumbImage)
        {
            int Id;

            dtoComic _comic = new dtoComic()
            {
                ComicDate = model.ComicDate,
                LongDescription = model.LongDescription,
                ShortDescription = model.ShortDescription,
                Title = model.Title
            };

            var onHomePage = collection["OnHomePage"];
            bool _onHomePage = false;

            char[] delimiters = { ',' };
            string[] checkedValue = onHomePage.Split(delimiters);

            if (checkedValue.Length > 1 || onHomePage.Contains("true") || onHomePage == "true")
            {
                if (bool.TryParse(checkedValue[0].ToString(), out _onHomePage))
                {
                    _comic.OnHomePage = _onHomePage;
                }
            }
            else
            {
                _comic.OnHomePage = _onHomePage;
            }

            if (Int32.TryParse(collection[Constants.Artist], out Id))
            {
                _comic.IllustratorID = Id;
                _comic.AuthorID = Id;
            }

            if (Int32.TryParse(collection[Constants.Writer], out Id))
                _comic.WriterID = Id;

            _comic.CastMembers = new List<dtoCast>();
            _comic.Tags = new List<dtoTag>();

            foreach(char _cast in collection[Constants.ComicCast].ToList())
            {
                dtoCast cast = new dtoCast();

                if (Int32.TryParse(_cast.ToString(), out Id))
                    cast.CastID = Id;

                _comic.CastMembers.Add(cast);
            }

            foreach(char _tag in collection[Constants.Tags].ToList())
            {
                dtoTag tag = new dtoTag();

                if (Int32.TryParse(_tag.ToString(), out Id))
                    tag.TagId = Id;

                _comic.Tags.Add(tag);
            }

            if (Int32.TryParse(collection[Constants.Book], out Id))
                _comic.BookID = Id;

            if (Int32.TryParse(collection[Constants.Chapter], out Id))
                _comic.ChapterID = Id;

            ViewData[Constants.AddStatus] = _comicService.Add(_comic, bannerImage, comicImage, thumbImage);

            return View(GetNewComic());
        }

        public ActionResult AddComic()
        {
            ViewData[Constants.AddStatus] = Constants.Ready;

            return View(GetNewComic());
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor ")]
        public ActionResult AddComicCast(FormCollection formCollection)
        {
            int _id = 0;

            if (!Int32.TryParse(formCollection[Constants.ComicId].ToString(), out _id))
            { }

            IList<string> castIds = formCollection[Constants.CastIds].ToString().Split(',').ToList<string>();

            foreach (var castId in castIds)
            {
                int cId;

                if (!Int32.TryParse(castId, out cId))
                { }
                else
                {
                    dtoComicCast comicCast = new dtoComicCast()
                    {
                        CastID = cId,
                        ComicID = _id
                    };

                    _comicCastService.AddNewCast(comicCast);
                }
            }

            return RedirectToAction("Comic", new { id = _id });
        }

        #endregion Comics

        #region ComicSystem
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult ComicSystem()
        {
            return View();
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Settings()
        {
            ViewData["UpdateStatus"] = string.Empty;

            dtoConfiguration siteSettings = SiteSettings.GetSiteSettings();

            return View(siteSettings);
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator")]
        [ValidateAntiForgeryToken]
        public ActionResult Settings(dtoConfiguration model)
        {
            bool success = SiteSettings.SaveConfig(model);

            if (success == true)
                ViewData["UpdateStatus"] = Constants.Success;
            else
                ViewData["UpdateStatus"] = Constants.Fail;

            return View(model);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult SystemErrors()
        {
            return View();
        }
        #endregion ComicSystem

        #region DashBoard Errors
        public ActionResult ErrorIndex(int? status, Exception error)
        {
            if (status.HasValue)
                Response.StatusCode = status.Value;

            return View(status);
        }
        #endregion Dashboard Errors

        #region Links
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Link(int id, int? response)
        {
            if (response.HasValue)
                Response.StatusCode = response.Value;

            return View(_linkService.Single(l => l.LinkId == id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Link(dtoLink model, HttpPostedFileBase imagePath)
        {
            int response = 500;
            bool success = false;

            if (imagePath != null)
                success = _linkService.Update(model, imagePath);
            else
                success = _linkService.Update(model);

            if (success == true)
                response = 201;

            return RedirectToAction("Link", new { @id = model.LinkId, @response = response });
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Links()
        {
            return View(_linkService.GetAll());
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddLink(dtoLink model, int? response)
        {
            if (response.HasValue)
                Response.StatusCode = response.Value;
            else
                Response.StatusCode = 200;

            return View(new dtoLink());    
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddLink(dtoLink model, HttpPostedFileBase ImagePath)
        {
            int value = 200;

            if (ImagePath != null && model.ExternalUrl != null )
            {
                if (_linkService.AddRetVal(model, ImagePath))
                {
                    value = 201;
                }
                else
                {
                    value = 500;
                }
            }
            else if (ImagePath == null && model.ExternalUrl != null)
            {
                value = 400;
            }

            return RedirectToAction("AddLink", new { @response = value });
        }
        #endregion Links

        #region Media

        #region Media Items
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddMedia(int? status)
        {
            string vStatus = string.Empty;

            if (status.HasValue)
            {
                if (status.Value == 1)
                    vStatus = Constants.Success;
                else if (status.Value == 2)
                    vStatus = Constants.InvalidFormData;
                else
                    vStatus = Constants.Fail;
            }

            ViewData[Constants.AddStatus] = vStatus;

            return View(new dtoMediaLibrary());
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddMedia(dtoMediaLibrary item, HttpPostedFileBase fileInput)
        {
            int status = 0;

            CP_Session _session = (CP_Session)System.Web.HttpContext.Current.Session["AdminUser"];

            item.Author = _session.FirstName + " " + _session.LastName;

            if (fileInput == null || string.IsNullOrEmpty(item.Name) || item.MediaType == 0)
                status = 2;

            if (_mediaLibraryService.Add(item, fileInput))
                status = 1;

            return RedirectToAction("AddMedia", new { @status = status });
        }

        public ActionResult MediaLibrary()
        {
            return View(_mediaLibraryService.GetAll());
        }

        public ActionResult MediaItem(int id, int? status)
        {
            string vStatus = string.Empty;

            if (status.HasValue)
            {
                if (status.Value == 1)
                    vStatus = Constants.UpdateStatus;
                else if (status.Value == 2)
                    vStatus = Constants.InvalidFormData;
                else
                    vStatus = Constants.Fail;
            }

            ViewData[Constants.UpdateStatus] = vStatus;

            return View(_mediaLibraryService.Single(id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult MediaItem(dtoMediaLibrary media, HttpPostedFileBase fileInput)
        {
            int status = 0;

            if (string.IsNullOrEmpty(media.Name) || media.MediaType == 0)
                status = 2;
            else
            {
                if (_mediaLibraryService.Update(media, fileInput))
                    status = 1;
            }

            return RedirectToAction("MediaItem", new { @id = media.MediaId, @status = status });
        }
        #endregion Media Items

        #region Media Types
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddMediaType(int? status)
        {
            string vStatus = string.Empty;

            if (status.HasValue)
            {
                if (status.Value == 1)
                    vStatus = Constants.AddStatus;
                else
                    vStatus = Constants.Fail;
            }

            ViewData["AddStatus"] = vStatus;

            return View(new dtoMediaType());    
        }

        [HttpPost]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddMediaType(dtoMediaType mediaType)
        {
            int status = 0;

            if (_mediaTypeService.AddRetValue(mediaType))
                status = 1;

            return RedirectToAction("AddMediaType", new { @status = status });
        }

        public ActionResult MediaTypes()
        {
            return View(_mediaTypeService.GetAll());
        }    

        public ActionResult MediaType(int id, int? status)
        {
            string vStatus = string.Empty;

            if (status.HasValue)
            {
                if (status.Value == 1)
                    vStatus = Constants.Success;
                else
                    vStatus = Constants.Fail;
            }

            ViewData[Constants.AddStatus] = vStatus;

            return View(_mediaTypeService.Single(mt => mt.TypeId == id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult MediaType(dtoMediaType mediaType)
        {
            int status = 0;

            if (_mediaTypeService.Update(mediaType))
                status = 1;

            return RedirectToAction("MediaType", new { @id = mediaType.TypeId, @status = status });
        }
        #endregion Media Types

        #endregion Media

        #region Tags
        public ActionResult Tag(int id)
        {
            return View(_tagService.Single(t => t.TagId == id));
        }

        public ActionResult Tags()
        {
            return View();
        }
        #endregion Tags

        #region Transcripts
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Transcript(int id, int? response)
        {
            if (response.HasValue)
                Response.StatusCode = response.Value;
            else
                Response.StatusCode = 200;

            return View(_transcriptService.Single(t => t.TranscriptId == id));
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Transcript(dtoTranscript model)
        {
            int value = 500;

            if (_transcriptService.Update(model))
                value = 201;

            return RedirectToAction("Transcript", new { @id = model.TranscriptId, @response = value });
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult Transcripts()
        {
            var transcripts = _transcriptService.GetAll();

            foreach (var transcript in transcripts)
            {
                var author = _memberService.Single(a => a.AuthorId == transcript.AuthorId);
                var comic = _comicService.Single(c => c.ComicID == transcript.ParentComic);

                transcript.Author = new dtoaspnet_Membership()
                {
                    AuthorId = author.AuthorId,
                    FirstName = author.FirstName,
                    LastName = author.LastName,
                };

                transcript.Comic = new dtoComic() 
                {
                    Title = comic.Title
                };                
            }

            return View(transcripts);
        }

        [Authorize(Roles = "Administrator, Content Editor")]
        public ActionResult AddTranscript(int? response)
        {
            if (response.HasValue)
                Response.StatusCode = response.Value;
            else
                Response.StatusCode = 200;

            return View(new dtoTranscript());
        }

        [HttpPost, ValidateInput(false)]
        [Authorize(Roles = "Administrator, Content Editor")]
        [ValidateAntiForgeryToken]
        public ActionResult AddTranscript(dtoTranscript model)
        {
            int value = 200;

            if (model.ParentComic > 0)
            {
                var obj = _comicService.Single(c => c.ComicID == model.ParentComic);
                model.Comic = new dtoComic()
                {
                    ComicID = obj.ComicID,
                    Title = obj.Title
                };
            }

            if (_transcriptService.AddRetString(model) == Constants.Success)
                value = 201;
            else
                value = 500;

            return RedirectToAction("AddTranscript", new { @response = value });
        }
        #endregion Transcripts

        #region Home Page
        //
        // GET: /DashBoard/
        [Authorize(Roles="Administrator, Content Editor")]
        public ActionResult Index()
        {
            return View();
        }
        #endregion Home Page

        #region Login/Logout
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session.Clear();
            Session.Abandon();

            FormsAuthentication.SignOut();

            return RedirectToAction("Login", "Account");
        }
        #endregion Login/Logout

        #region Menus and Drop Downs
        public ActionResult ActionsMenu()
        {
            return View();
        }

        public ActionResult MainMenu()
        {            
            return View(_menuService.GetMenu());
        }

        public ActionResult RightMenu()
        {
            return View();
        }

        public ActionResult TaskMenu()
        {
            return View();
        }

        #endregion Menus and Drop Downs

        #region Static Methods

        public dtoaspnet_Membership GetMember(int id)
        {
            dtoaspnet_Membership _member = _memberService.Single(a => a.AuthorId == id);

            var _author = _authorService.Single(a => a.AuthorID == id);
            var _books = _bookService.GetAll(b => b.AuthorID == id);
            var _comics = _comicService.GetAll(c => c.AuthorID == id);            
            var _userRoles = _usersInRolesService.GetAll(ur => ur.UserId == _member.UserId);
            var _authorType = _authorTypeService.Single(at => at.TypeId == _author.AuthorType);
            var _authorInfo = _memberService.Single(ai => ai.AuthorId == _author.AuthorID);

            if (_member != null || _author != null)
            {
                _member.Author = new dtoAuthor();
                _member.Author.AuthorTypes = new dtoAuthorType();
                _member.Author.Books = new List<dtoBook>();
                _member.Author.Comics = new List<dtoComic>();
                _member.Aspnet_Roles = new List<dtoaspnet_Roles>();
                _member.aspnet_Users = new dtoaspnet_Users();
                _member.aspnet_Users.aspnet_Membership = new dtoaspnet_Membership()
                    {
                        ApplicationId = _authorInfo.ApplicationId,
                        Bio = _authorInfo.Bio,
                        Email = _authorInfo.Email,
                        FirstName = _authorInfo.FirstName,
                        IsApproved = _authorInfo.IsApproved,
                        IsLockedOut = _authorInfo.IsLockedOut,
                        LastName = _authorInfo.LastName,
                        LoweredEmail = _authorInfo.LoweredEmail,
                        MobilePIN = _authorInfo.MobilePIN,
                        UserId = _authorInfo.UserId
                    };
                

                _member.Author.Avatar = _author.Avatar;
                _member.Author.AuthorType = _author.AuthorType;
                _member.Author.AuthorTypes = _authorType;
                _member.Author.UseGravatar = _author.UseGravatar;

                var _user = _userService.Single(u => u.UserId == _member.UserId);


                if (_user != null)
                {
                    _member.aspnet_Users.IsAnonymous = _user.IsAnonymous;
                    _member.aspnet_Users.LastActivityDate = _user.LastActivityDate;
                    _member.aspnet_Users.LoweredUserName = _user.LoweredUserName;
                    _member.aspnet_Users.UserName = _user.UserName;
                }

                if (_userRoles.Count > 0)
                {
                    foreach (var _role in _userRoles)
                    {
                        var role = _roleService.Single(r => r.RoleId == _role.RoleId);

                        _member.Aspnet_Roles.Add(role);
                    }
                }

                if (_books.Count > 0)
                {
                    _member.Author.Books.AddRange(_books);
                }

                _member.Author.ComicCount = _comics.Count;

                if (_comics.Count > 0)
                {
                    foreach (var comic in _comics)
                    {
                        _member.Author.Comics.Add(comic);
                    }
                }                
            }

            return _member;
        }

        public dtoNewComicModel GetNewComic()
        {
            dtoNewComicModel model = new dtoNewComicModel();

            var books = _bookService.GetAll();
            var castMembers = _castService.GetAll();
            var chapters = _chapterService.GetAll();
            var tags = _tagService.GetAll();

           

            model.CastMembers = new List<dtoCast>(castMembers);
            model.Books = new List<dtoBook>(books);
            model.Authors = new List<dtoaspnet_Membership>(_memberService.GetAll(a => a.IsApproved == true));                

            foreach (var author in model.Authors)
            {
                var _author = _authorService.Single(a => a.AuthorID == author.AuthorId);
                var authorType = _authorTypeService.Single(at => at.TypeId == _author.AuthorType);

                _author.AuthorType = authorType.TypeID;
                _author.TypeName = authorType.Description;

                author.Author = new dtoAuthor();

                author.Author.AuthorType = _author.AuthorType;
                author.Author.TypeName = _author.TypeName;
            }

            model.Chapters = new List<dtoChapter>(chapters);
            model.Tags = new List<dtoTag>(tags);

            return model;
        }

        public dtoNewCharacter GetNewCharacter(string sucess)
        {
            dtoNewCharacter character = new dtoNewCharacter();
            ViewData["UpdateStatus"] = "Ready";

            character.BloodTypes = new List<dtoBloodType>()
            {
                new dtoBloodType()
                {
                    Type = "A+"
                },
                new dtoBloodType()
                {
                    Type = "A-"
                },
                new dtoBloodType()
                {
                    Type = "AB+"
                },
                new dtoBloodType()
                {
                    Type = "AB-"
                },
                new dtoBloodType()
                {
                    Type = "B+"
                },
                new dtoBloodType()
                {
                    Type = "B-"
                },
                new dtoBloodType()
                {
                    Type = "O+"
                },
                new dtoBloodType()
                {
                    Type = "O-"
                }
            };

            character.Success = sucess;

            return character;
        }

        #endregion Static Methods
    }
}
