﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class ArticleController : ApiController
    {
        private readonly INewsService _newsService;

        public ArticleController()
            : this(new NewsService())
        {
        }

        public ArticleController(INewsService newsService)
        {
            _newsService = newsService;
        }

        // GET api/article
        [System.Web.Http.HttpGet]
        [Authorize(Roles = "Administrator, Content Editor")]
        public ServiceDataResponse<IList<dtoNews>> Get()
        {
            IList<dtoNews> _news = _newsService.GetAll().OrderByDescending(n => n.NewsDate).ToList();

            if (_news != null)
            {
                return new ServiceDataResponse<IList<dtoNews>>(_news);
            }
            else
            {
                ServiceDataResponse<IList<dtoNews>> fail = new ServiceDataResponse<IList<dtoNews>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // GET api/article/5
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Get(int id)
        {
            dtoNews news = _newsService.Single(n => n.NewsId == id);

            if (news == null)
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "News article not found for the given id");
            else
                return Request.CreateResponse(HttpStatusCode.OK, news);
        }

        // POST api/article
        [HttpPost]
        [AcceptVerbs("GET", "POST")]        
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Post(dtoNews news)
        {            
            news.Title = StringExtensions.CleanText(news.Title);

            string message = _newsService.AddNew(news);

            if (message == Constants.Success)
            {
                return Request.CreateResponse<string>(HttpStatusCode.Created, message);
            }
            else
            {
                return Request.CreateResponse<string>(HttpStatusCode.ExpectationFailed, message);
            }
        }

        // PUT api/article/5
        [HttpPut]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Put(int id, dtoNews news)
        {
            news.NewsId = id;
            news.Title = StringExtensions.CleanText(news.Title);

            if (_newsService.UpdateNews(news) != Constants.Fail)
                return Request.CreateResponse(HttpStatusCode.OK);
            else
                return Request.CreateErrorResponse(HttpStatusCode.ExpectationFailed, "Unable to update the news article with the given ID");
        }

        // DELETE api/article/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            var news = _newsService.Single(x => x.NewsId == id);

            if (news != null)
            {
                var response = _newsService.DeleteNews(news);

                if (response == Constants.Success)
                    return new HttpResponseMessage(HttpStatusCode.NoContent);
                else
                    return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
            else
                return new HttpResponseMessage(HttpStatusCode.NotFound);
        }
    }
}
