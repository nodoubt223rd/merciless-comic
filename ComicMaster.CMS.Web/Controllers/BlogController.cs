﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;
using ComicMaster.CMS.Web.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class BlogController : ApiController
    {
        private readonly IPostService _postService;

        public BlogController()
            : this(new PostService())
        {            
        }

        public BlogController(IPostService postService)
        {
            _postService = postService;
        }

        // GET api/blog
        public ServiceDataResponse<PostViewModel> Get(int id, int? page)
        {
            int start = 1;
            int pageSize;

            if (page.HasValue)
                start = page.Value;

            if (int.TryParse(ConfigurationManager.AppSettings["PageSize"], out pageSize))
            {
                var posts = _postService.Posts(id, start - 1, pageSize);
                var totalPosts = _postService.Count();

                var postViewModel = new PostViewModel
                {
                    Posts = posts,
                    TotalPosts = totalPosts
                };

                if (postViewModel.Posts != null)
                    return new ServiceDataResponse<PostViewModel>(postViewModel);
                else
                {
                    ServiceDataResponse<PostViewModel> fail = new ServiceDataResponse<PostViewModel>();
                    fail.data = null;
                    fail.message = ResponseMessage.ApiFail;
                    fail.success = false;

                    return fail;
                }
            }

            return null;
        }

        // GET api/blog/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/blog
        public void Post([FromBody]string value)
        {
        }

        // PUT api/blog/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/blog/5
        public void Delete(int id)
        {
        }
    }
}
