﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class CategoryController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController()
            : this(new CategoryService())
        {            
        }

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        // GET api/category
        [HttpGet]
        public ServiceDataResponse<IList<dtoCategory>> Get()
        {
            IList<dtoCategory> _categories = _categoryService.Categories();

            if (_categories != null)
            {
                return new ServiceDataResponse<IList<dtoCategory>>(_categories);
            }
            else
            {
                ServiceDataResponse<IList<dtoCategory>> fail = new ServiceDataResponse<IList<dtoCategory>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        [HttpGet]
        public ServiceDataResponse<IList<dtoCategory>> GetHome(int id)
        {            
            IList<dtoCategory> _categories = _categoryService.Categories(id);

            if (_categories != null)
            {
                return new ServiceDataResponse<IList<dtoCategory>>(_categories);
            }
            else
            {
                ServiceDataResponse<IList<dtoCategory>> fail = new ServiceDataResponse<IList<dtoCategory>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        [HttpGet]
        public ServiceDataResponse<dtoCategory> Get(string id)
        {
            dtoCategory _categories = _categoryService.Categoy(id);

            if (_categories != null)
            {
                return new ServiceDataResponse<dtoCategory>(_categories);
            }
            else
            {
                ServiceDataResponse<dtoCategory> fail = new ServiceDataResponse<dtoCategory>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // POST api/category
        [HttpPost]
        [AcceptVerbs("GET", "POST")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Post(dtoCategory category)
        {
            if (_categoryService.AddRetValue(category))
            {
                var response = Request.CreateResponse<dtoCategory>(HttpStatusCode.Created, category);
                return response;
            }
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }

        // PUT api/category/5
        [HttpPut]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Put(dtoCategory category)
        {
            if (_categoryService.Update(category))
            {
                return new HttpResponseMessage(HttpStatusCode.OK);
            }
            else
            {
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }

        // DELETE api/category/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            if (_categoryService.Delete(id))
            {
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            }
            else
            {                
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
            }
        }
    }
}
