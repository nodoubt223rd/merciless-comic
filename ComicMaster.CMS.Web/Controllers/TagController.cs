﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class TagController : ApiController
    {
        private readonly ITagService _tagService;

        public TagController()
            : this(new TagService())
        {            
        }

        public TagController(ITagService tagService)
        {
            _tagService = tagService;
        }

        // GET api/tag
        [HttpGet]
        public ServiceDataResponse<IList<dtoTag>> Get()
        {
            IList<dtoTag> _tags = _tagService.TagCloud();

            if (_tags != null)
            {
                return new ServiceDataResponse<IList<dtoTag>>(_tags);               
            }
            else
            {
                ServiceDataResponse<IList<dtoTag>> fail = new ServiceDataResponse<IList<dtoTag>>();

                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        [HttpGet]
        public ServiceDataResponse<IList<dtoTag>> Menu()
        {
            IList<dtoTag> _tags = _tagService.GetAll();

            if (_tags != null)
            {
                return new ServiceDataResponse<IList<dtoTag>>(_tags);
            }
            else
            {
                ServiceDataResponse<IList<dtoTag>> fail = new ServiceDataResponse<IList<dtoTag>>();

                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // GET api/tag/5
        public string Get(string id)
        {
            return "value";
        }

        // POST api/tag
        [System.Web.Http.HttpPost]
        [AcceptVerbs("GET", "POST")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Post([FromBody]string tags)
        {
            if (_tagService.Add(tags))
                return new HttpResponseMessage(HttpStatusCode.OK);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }

        // PUT api/tag/5
        [HttpPut]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Put(dtoTag tag)
        {
            if (_tagService.Update(tag))
                return new HttpResponseMessage(HttpStatusCode.OK);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }

        // DELETE api/tag/5
        [HttpDelete]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Authorize(Roles = "Administrator, Content Editor")]
        public HttpResponseMessage Delete(int id)
        {
            if (_tagService.Delete(id))
                return new HttpResponseMessage(HttpStatusCode.OK);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
