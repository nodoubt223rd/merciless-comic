﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.Syndication;
using System.Web.Mvc;

using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Web.Infrastructure.Rss;

using MvcPaging;

namespace ComicMaster.CMS.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBlogService _blogService;
        private readonly ICastService _castService;
        private readonly ICategoryService _categoryService;
        private readonly IComicService _comicService;
        private readonly ILinkService _linkService;
        private readonly IMembershipService _membershipService;
        private readonly INewsService _newsService;
        private readonly IPostService _postService;
        private readonly ISearchService _searchService;
        private readonly ITagService _tagService;

        public HomeController()
            : this(new BlogService(), new CastService(), new CategoryService(), new ComicService(), new LinkService(), new MembershipService(), new NewsService(), new PostService(), new SearchService(), new TagService())
        {
        }        

        public HomeController(IBlogService blogService, ICastService castService, ICategoryService categoryService, IComicService comicService, ILinkService linkService, IMembershipService membershipService, INewsService newsService, IPostService postService, ISearchService searchService, ITagService tagService)
        {
            _blogService = blogService;
            _castService = castService;
            _categoryService = categoryService;
            _comicService = comicService;
            _linkService = linkService;
            _membershipService = membershipService;
            _newsService = newsService;
            _postService = postService;
            _searchService = searchService;
            _tagService = tagService;
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Blog(string id)
        {
            return View(_blogService.Blog(id));
        }

        public ActionResult Blogs()
        {
            return View(_blogService.Blogs());
        }

        public ActionResult Authors()
        {
            return View(_membershipService.Authors());
        }

        public ActionResult Cast()
        {
            return View(_castService.GetAll());
        }

        public ActionResult Category(string id = "Default")
        {
            if ("Default" == id)
                return View(_categoryService.Categories());
            else
                return View(_categoryService.Categories(id));
        }

        public ActionResult Comic(string id)
        {
            return View(_comicService.Single(c => c.ComicSlug == id));
        }

        public ActionResult Comics()
        {
            return View();
        }

        public ActionResult _ComicDisplay(int? id)
        {
            int currentPageIndex = id.HasValue ? id.Value : 1;

            var comics = _comicService.ComicDisplay().ToPagedList(currentPageIndex, 1);           

            //System.Web.HttpContext.Current.RewritePath("/comic/archives/" + _comics[currentPageIndex].ComicSlug);

            return PartialView(comics);
        }

        public ActionResult ComicFeed()
        {
            var _comics = _comicService.GetAll().OrderByDescending(c => c.ComicID)
                .Select(c => new SyndicationItem(c.Title, c.LongDescription, new Uri("http://www.merciless-comic.com/comic/" + c.ComicSlug.ToString())));

            var feed = new SyndicationFeed("Merciless | All Shall Fall Before His Might", "Merciless web comic created by Brett G. Murphy, and written by Hannah Lavallee", new Uri("http://www.merciless-comic.com/comic/archives/"), _comics)
                {                  
                    Copyright = new TextSyndicationContent("Copyright &copy;" + DateTime.Now.Year.ToString()),
                    Language = "en-US"
                };

            return new RssFeed(new Atom10FeedFormatter(feed));
        }

        public ActionResult _News()
        {
            var news = _newsService.GetNewsFeed(0, 3);

            return PartialView(news);
        }

        public ActionResult AjaxNews(int? page)
        {
            int currentPageIndex = page.HasValue ? page.Value  -1 : 0;
            var news = this._newsService.GetNewsFeed(currentPageIndex, 3);          

            return PartialView("_News", news);
        }

        public ActionResult Post(string id)
        {
            return View(_postService.Post(id));
        }

        public ActionResult Posts(int? year, string month = "Default")
        {
            if ( year.HasValue && "Default" != month)
                return View(_postService.Archives(year.Value, month));
            else
                return View(_postService.Archives());
        }

        public ActionResult _PreviousComic()
        {
            return PartialView(_comicService.Single(x => x.IsLastComic == true));
        }

        public ActionResult Search(string id)
        {
            return View(_searchService.SearchContet(StringExtensions.CleanText(id)));
        }

        public ActionResult _SideBar()
        {
            return PartialView(_linkService.GetAll());
        }

        public ActionResult Tag(string id)
        {
            return View(_tagService.Tag(id));
        }

        public ActionResult Wallpapers()
        {
            return View();
        }        
    }
}
