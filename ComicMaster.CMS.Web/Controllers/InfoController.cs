﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class InfoController : ApiController
    {
        private readonly IAuthorService _authorService;
        private readonly IMembershipService _membershipService;
        private readonly IUserService _userService;

        public InfoController()
            : this(new AuthorService(), new MembershipService(), new UserService())
        {
        }

        public InfoController(IAuthorService authorService, IMembershipService membershipService, IUserService userService)
        {
            _authorService = authorService;
            _membershipService = membershipService;
            _userService = userService;
        }

        // GET api/author/user-name
        [Authorize(Roles = "Administrator, Content Editor")]
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<dtoaspnet_Membership> GetAvatar(string id)
        {
            dtoaspnet_Users _user = _userService.Single(u => u.UserName == id);
            dtoaspnet_Membership _member = _membershipService.Single(m => m.UserId == _user.UserId);
            dtoAuthor _author = _authorService.Single(a => a.AuthorID == _member.AuthorId);

            _member.Author = new dtoAuthor()
            {
                Avatar = _author.Avatar,
                AuthorID = _author.AuthorID
            };

            return new ServiceDataResponse<dtoaspnet_Membership>(_member);
        }
    }
}
