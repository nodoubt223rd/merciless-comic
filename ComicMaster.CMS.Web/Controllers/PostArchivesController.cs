﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Extentions;
using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class PostArchivesController : ApiController
    {
        private readonly IPostService _postService;

        public PostArchivesController()
            : this(new PostService())
        {            
        }

        public PostArchivesController(IPostService postService)
        {
            _postService = postService;
        }
        // GET api/post-archives
        [HttpGet]
        public ServiceDataResponse<IList<dtoPostArchive>> Get()
        {
            var archives = _postService.Archives();

            if (archives != null)
                return new ServiceDataResponse<IList<dtoPostArchive>>(archives);
            else
            {
                ServiceDataResponse<IList<dtoPostArchive>> error = new ServiceDataResponse<IList<dtoPostArchive>>();
                error.data = null;
                error.message = ResponseMessage.ApiFail;
                error.success = false;

                return error;
            }
        }

        // GET api/postarchives/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/postarchives
        public void Post([FromBody]string value)
        {
        }

        // PUT api/postarchives/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/postarchives/5
        public void Delete(int id)
        {
        }
    }
}
