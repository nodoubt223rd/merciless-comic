﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using ComicMaster.CMS.Common.Infrastructure.Service;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Controllers
{
    public class AuthorController : ApiController
    {
        private readonly IAuthorService _authorService;
        private readonly IMembershipService _membershipService;
        private readonly IUserService _userService;
        private readonly IComicService _comicService;

        public AuthorController()
            : this(new AuthorService(), new MembershipService(), new UserService(), new ComicService())
        {
        }

        public AuthorController(IAuthorService authorService, IMembershipService membershipService, IUserService userService, IComicService comicService)
        {
            _authorService = authorService;
            _membershipService = membershipService;
            _userService = userService;
            _comicService = comicService;
        }

        // GET api/author
        [System.Web.Http.HttpGet]
        public ServiceDataResponse<IList<dtoaspnet_Membership>> Get()
        {
            IList<dtoaspnet_Membership> _authors = _membershipService.Authors();

            if (_authors != null)
            {
                return new ServiceDataResponse<IList<dtoaspnet_Membership>>(_authors);
            }
            else
            {
                ServiceDataResponse<IList<dtoaspnet_Membership>> fail = new ServiceDataResponse<IList<dtoaspnet_Membership>>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }        

        // GET api/author/5
        public ServiceDataResponse<dtoaspnet_Membership> Get(int id)
        {
            dtoaspnet_Membership _member = _membershipService.Single(a => a.AuthorId == id);
            var _comics = _comicService.GetAll(c => c.AuthorID == id);
            var _author = _authorService.Single(a => a.AuthorID == id);

            if (_member != null || _author != null)
            {
                _member.Author = new dtoAuthor();
                _member.Author.Comics = new List<dtoComic>();
                _member.aspnet_Users = new dtoaspnet_Users();

                _member.Author.Avatar = _author.Avatar;

                var _user = _userService.Single(u => u.UserId == _member.UserId);
                

                if (_user != null)
                {
                    _member.aspnet_Users.IsAnonymous = _user.IsAnonymous;
                    _member.aspnet_Users.LastActivityDate = _user.LastActivityDate;
                    _member.aspnet_Users.LoweredUserName = _user.LoweredUserName;
                    _member.aspnet_Users.UserName = _user.UserName;
                }

                _member.Author.ComicCount = _comics.Count;

                if (_comics.Count > 0)
                {
                    foreach (var comic in _comics)
                    {
                        _member.Author.Comics.Add(comic);
                    }
                }

                return new ServiceDataResponse<dtoaspnet_Membership>(_member);
            }
            else
            {
                ServiceDataResponse<dtoaspnet_Membership> fail = new ServiceDataResponse<dtoaspnet_Membership>();
                fail.data = null;
                fail.message = ResponseMessage.ApiFail;
                fail.success = false;

                return fail;
            }
        }

        // DELETE api/author/5
        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        public HttpResponseMessage Delete(int id)
        {
            bool success = _membershipService.DeleteRetBool(id);
            if (success == true)
                return new HttpResponseMessage(HttpStatusCode.NoContent);
            else
                return new HttpResponseMessage(HttpStatusCode.ExpectationFailed);
        }
    }
}
