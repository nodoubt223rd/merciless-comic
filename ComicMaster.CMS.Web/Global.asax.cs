﻿using System;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

using ComicMaster.CMS.Common.Infrastructure.Mvc;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;
using ComicMaster.CMS.Services.Jobs;

using log4net;
using log4net.Config;
using Microsoft.Practices.Unity;

[assembly: WebActivator.PreApplicationStartMethod(typeof(ComicMaster.CMS.Web.LogStartup), "Start")]

namespace ComicMaster.CMS.Web
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class WebApiApplication : System.Web.HttpApplication
    {
        public const int MINUTES_TO_WAIT = 5;

        private static IUnityContainer _container;
        private string _workerPageUrl = null;
        private readonly Logger Log = new Logger();
        Setting currentSetting = null;

        protected string WorkerPageUrl
        {
            get
            {
                if(_workerPageUrl == null)
                    _workerPageUrl = (Application["WebRoot"] + VirtualPathUtility.ToAbsolute("~/DoTimedWork.ashx")).Replace("//",  "/").Replace(":/", "://") + "?schedule=true";

                return _workerPageUrl;
            }
        }

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            GlobalConfiguration.Configuration.Filters.Add(new ElmahErrorAttribute());
            log4net.Config.XmlConfigurator.Configure();

            Logger logger = new Logger();

            logger.Info(string.Format("[INFO]: Comic Presence started on --- {0}", DateTime.Now));

            _container = new UnityContainer()
                .RegisterType<IAuthorService, AuthorService>(new ContainerControlledLifetimeManager())
                .RegisterType<IAuthorTypeService, AuthorTypeService>()
                .RegisterType<IBlogService, BlogService>()
                .RegisterType<IBookChapterService, BookChapterService>()
                .RegisterType<IBookService, BookService>()
                .RegisterType<ICategoryService, CategoryService>()
                .RegisterType<ICastService, CastService>()
                .RegisterType<IChapterService, ChapterService>()
                .RegisterType<IComicCastService, ComicCastService>()
                .RegisterType<IComicService, ComicService>()
                .RegisterType<IComicTagService, ComicTagService>()
                .RegisterType<ILinkService, LinkService>()
                .RegisterType<IMediaLibraryService, MediaLibraryService>()
                .RegisterType<IMediaTypeService, MediaTypeService>()
                .RegisterType<IMembershipService, MembershipService>()
                .RegisterType<IMenuService, MenuService>()
                .RegisterType<INewsService, NewsService>()
                .RegisterType<IPostService, PostService>()
                .RegisterType<IPostTagMapService, PostTagMapService>()
                .RegisterType<IRoleService, RoleService>()
                .RegisterType<ISiteSettingService, SiteSettingService>()
                .RegisterType<ITagService, TagService>()
                .RegisterType<ITranscriptService, TranscriptService>()
                .RegisterType<IUserService, UserService>()
                .RegisterType<IUsersInRolesService, UsersInRolesService>()
                .RegisterType<ISearchService, SearchService>()
                .RegisterType<IRepositoryContext, ComicMasterRepositoryContext>();
            
            //Set for Controller Factory
            IControllerFactory controllerFactory = new UnityControllerFactory(_container);

            ControllerBuilder.Current.SetControllerFactory(controllerFactory);            
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            Uri reqUri = HttpContext.Current.Request.Url;
            Uri relativePath = new Uri(WorkerPageUrl, UriKind.RelativeOrAbsolute);
            Uri fullUri = new Uri(reqUri, relativePath);

            Application["WebRoot"] = new UriBuilder(reqUri.Scheme, reqUri.Host, reqUri.Port).ToString();
            Application["TimerRunning"] = false;

            //StartTimer();   // don't want timer to start unless we call the url (don't run in dev, etc).  Url will be called by monastic for live site.

            if (HttpContext.Current.Request.Path == GetLocalUrl(fullUri))
            {
                CurrentSetting.LastRun = DateTime.Now;
                try
                {
                    CurrentSetting.RunCount++;
                }
                catch (Exception)
                {
                    CurrentSetting.RunCount = 0;  // just in case of an overflow
                }
                SaveSettings();
            }
        }

        private void StartTimer()
        {
            if (!(bool)Application["TimerRunning"]) // don't want multiple timers
            {
                System.Timers.Timer timer = new System.Timers.Timer(MINUTES_TO_WAIT * 60 * 1000);
                timer.AutoReset = false;
                timer.Enabled = true;
                timer.Elapsed += new System.Timers.ElapsedEventHandler(timer_Elapsed);
                timer.Start();
                Application["TimerRunning"] = true;
            }

        }

        void timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            Application["TimerRunning"] = false;
            System.Net.WebClient client = new System.Net.WebClient();
            // have to issue a request so that there is a context
            // also lets us separate all of the scheduling logic from the work logic
            client.DownloadData(WorkerPageUrl + "&LastSuccessfulRun=" + Server.UrlEncode((CurrentSetting.LastSuccessfulRun ?? DateTime.Now.AddYears(-1)).ToString()));
        }



        protected void Application_Error(object sender, EventArgs e)
        {
            Uri reqUri = HttpContext.Current.Request.Url;
            Uri relativePath = new Uri(WorkerPageUrl, UriKind.RelativeOrAbsolute);
            Uri fullUri = new Uri(reqUri, relativePath);

            Exception exception = Server.GetLastError().GetBaseException();

            Server.ClearError();

            var url = HttpContext.Current.Request.Url.OriginalString;

            var routeData = new RouteData();

            if (HttpContext.Current.Request.Path == GetLocalUrl(fullUri))
            {
                Log.Error(HttpContext.Current.Error.GetBaseException());
            }

            if (url.Contains("dashboard"))
            {
                routeData.Values.Add("controller", "DashBoard");
                routeData.Values.Add("action", "ErrorIndex");
            }
            else
            {
                routeData.Values.Add("controller", "Error");
                routeData.Values.Add("action", "Index");
            }

            if (exception.GetType() == typeof(HttpException))
            {
                var httpException = (HttpException)exception;
                var code = httpException.GetHttpCode();
                routeData.Values.Add("status", code);
            }
            else
            {
                routeData.Values.Add("status", 500);
            }

            routeData.Values.Add("error", exception);

            IController errorController = new ComicMaster.CMS.Web.Controllers.ErrorController();
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));            
        }        

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            Uri reqUri = HttpContext.Current.Request.Url;
            Uri relativePath = new Uri(WorkerPageUrl, UriKind.RelativeOrAbsolute);
            Uri fullUri = new Uri(reqUri, relativePath);

            if(Context.Response.StatusCode == 401)
            {
                throw new HttpException(401, "You are not authorized to view this content!");
            }

            if (HttpContext.Current.Request.Path == GetLocalUrl(fullUri))
            {
                if (HttpContext.Current.Error == null) //
                {
                    CurrentSetting.LastSuccessfulRun = DateTime.Now;
                    SaveSettings();
                }

                if (HttpContext.Current.Request["schedule"] == "true")// register the next iteration whenever worker finished
                    StartTimer();
            }
        }
        
        protected Setting CurrentSetting
        {
            get
            {
                if (currentSetting == null)
                {
                    using (System.Security.Principal.WindowsImpersonationContext imp = CommonTask.Impersonate())
                    {
                        System.IO.FileInfo f = new System.IO.FileInfo(HttpContext.Current.Server.MapPath("~/data/settings.xml"));
                        if (f.Exists)
                        {
                            System.Xml.Linq.XDocument doc = System.Xml.Linq.XDocument.Load(f.FullName);
                            currentSetting = (from s in doc.Elements("Setting")
                                              select new Setting()
                                              {
                                                  LastRun = DateTime.Parse(s.Element("LastRun").Value),
                                                  LastSuccessfulRun = DateTime.Parse(s.Element("LastSuccessfulRun").Value),
                                                  RunCount = long.Parse(s.Element("RunCount").Value)
                                              }).First();

                        }
                    }
                }

                if (currentSetting == null)
                {
                    currentSetting = new Setting()
                    {
                        LastRun = null,
                        LastSuccessfulRun = DateTime.Now.AddYears(-1),//ignore older than one year old in test
                        RunCount = 0
                    };
                }

                return currentSetting;
            }
            set
            {
                currentSetting = value;
                if (CommonTask.Live)
                {
                    using (System.Security.Principal.WindowsImpersonationContext imp = CommonTask.Impersonate())
                    {
                        System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(HttpContext.Current.Server.MapPath("~/data"));
                        if (!di.Exists)
                            di.Create();
                        System.Xml.XmlWriter writer = System.Xml.XmlWriter.Create(HttpContext.Current.Server.MapPath("~/data/settings.xml"));
                        try
                        {
                            System.Xml.Linq.XDocument doc = new System.Xml.Linq.XDocument(
                                    new System.Xml.Linq.XElement("Setting",
                                        new System.Xml.Linq.XElement("LastRun", currentSetting.LastRun ?? DateTime.Now),
                                        new System.Xml.Linq.XElement("LastSuccessfulRun", currentSetting.LastSuccessfulRun),
                                        new System.Xml.Linq.XElement("RunCount", currentSetting.RunCount)
                                        )
                                );
                            doc.WriteTo(writer);
                        }
                        catch (Exception exc)
                        {
                            Log.Error(exc);
                        }
                        finally
                        {
                            writer.Flush();
                            writer.Close();
                        }
                    }
                }
            }
        }

        protected void SaveSettings()
        {
            CurrentSetting = CurrentSetting; // reset to ensure "setter" code saves to file
        }

        private string GetLocalUrl(Uri uri)
        {
            string ret = uri.AbsoluteUri;
            if (uri.Query != null && uri.Query.Length > 0)
                ret = ret.Replace(uri.Query, "");

            return ret;
        }        
    }

    public static class LogStartup
    {
        public static void Start()
        {
            XmlConfigurator.Configure();
        }
    }

}