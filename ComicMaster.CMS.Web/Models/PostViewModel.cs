﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Models
{
    public class PostViewModel
    {
        public List<dtoPost> Posts { get; set; }
        public long TotalPosts { get; set; }
    }
}