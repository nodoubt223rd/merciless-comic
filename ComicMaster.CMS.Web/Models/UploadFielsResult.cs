﻿using System;

namespace ComicMaster.CMS.Web.Models
{
    public class UploadFielsResult
    {
        public string Name { get; set; }
        public string Length { get; set; }
        public string Type { get; set; }
        public string FilePath { get; set; }
    }
}