﻿using System;
using System.Linq;
using System.Web.Mvc;

using ComicMaster.CMS.Security;

namespace ComicMaster.CMS.Web.Infrastructure
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }
}