﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComicMaster.CMS.Web.Infrastructure.Rss
{
    public interface IRss
    {
        string Title { get; }
        string ComicImage { get; }
        string ShortDescription { get; }
        string LongDescription { get; }
    }
}
