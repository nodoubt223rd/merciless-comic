﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Core.Models;

namespace ComicMaster.CMS.Web.Infrastructure
{
    public static class SiteSettings
    {
        public static dtoConfiguration GetSiteSettings()
        {
            dtoConfiguration settings = new dtoConfiguration();

            int aas;

            if (int.TryParse(ConfigurationManager.AppSettings[ConfigConstants.AdminAvatarSize], out aas))
            {
                settings.AdminAvatarSize = aas;
            }
           
            settings.Characters = ConfigurationManager.AppSettings[ConfigConstants.Characters];
            settings.CharacterThumbs = ConfigurationManager.AppSettings[ConfigConstants.CharacterThumbs];
            settings.ComicBanner = ConfigurationManager.AppSettings[ConfigConstants.ComicBanner];
            settings.ComicPath = ConfigurationManager.AppSettings[ConfigConstants.ComicPath];
            settings.ComicThumbNailPath = ConfigurationManager.AppSettings[ConfigConstants.ComicThumbnailPath];
            settings.DefaultPostTime = ConfigurationManager.AppSettings[ConfigConstants.DefaultPostTime];
            settings.LinkImages = ConfigurationManager.AppSettings[ConfigConstants.LinkImages];
            settings.SiteName = ConfigurationManager.AppSettings[ConfigConstants.SiteName];
            settings.SiteSlogan = ConfigurationManager.AppSettings[ConfigConstants.SiteSlogan];
            settings.SiteTheme = ConfigurationManager.AppSettings[ConfigConstants.SiteTheme];
            settings.SiteVersion = ConfigurationManager.AppSettings[ConfigConstants.SiteVersion];
            settings.UserAvatar = ConfigurationManager.AppSettings[ConfigConstants.UserAvatar];

            return settings;
        }

        public static bool SaveConfig(dtoConfiguration settings)
        {
            try
            {
                Configuration config = WebConfigurationManager.OpenWebConfiguration("~");

                config.AppSettings.Settings[ConfigConstants.AdminAvatarSize].Value = settings.AdminAvatarSize.ToString();
                config.AppSettings.Settings[ConfigConstants.Characters].Value = settings.Characters;
                config.AppSettings.Settings[ConfigConstants.CharacterThumbs].Value = settings.CharacterThumbs;
                config.AppSettings.Settings[ConfigConstants.ComicBanner].Value = settings.ComicBanner;
                config.AppSettings.Settings[ConfigConstants.ComicPath].Value = settings.ComicPath;
                config.AppSettings.Settings[ConfigConstants.ComicThumbnailPath].Value = settings.ComicThumbNailPath;
                config.AppSettings.Settings[ConfigConstants.DefaultPostTime].Value = settings.DefaultPostTime;
                config.AppSettings.Settings[ConfigConstants.LinkImages].Value = settings.LinkImages;
                config.AppSettings.Settings[ConfigConstants.SiteName].Value = settings.SiteName;
                config.AppSettings.Settings[ConfigConstants.SiteSlogan].Value = settings.SiteSlogan;
                config.AppSettings.Settings[ConfigConstants.SiteTheme].Value = settings.SiteTheme;
                config.AppSettings.Settings[ConfigConstants.SiteVersion].Value = settings.SiteVersion;
                config.AppSettings.Settings[ConfigConstants.UserAvatar].Value = settings.UserAvatar;

                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}