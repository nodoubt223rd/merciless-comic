using System;

[assembly: WebActivator.PreApplicationStartMethod(
    typeof(ComicMaster.CMS.Web.App_Start.MySuperPackage), "PreStart")]

namespace ComicMaster.CMS.Web.App_Start {
    public static class MySuperPackage {
        public static void PreStart() {
            MVCControlsToolkit.Core.Extensions.Register();
        }
    }
}