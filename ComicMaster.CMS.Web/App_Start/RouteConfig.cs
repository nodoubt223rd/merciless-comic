﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;

using LowercaseRoutesMVC4;

namespace ComicMaster.CMS.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            
            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "About",
                url: "about/",
                defaults: new { controller = "Home", action = "About" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Archive",
                url: "posts/archive/{year}/{month}",
                defaults: new { controller = "Home", action = "Posts", year = "year", month = "month" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "ArchiveList",
                url: "posts/archive/",
                defaults: new { controller = "Home", action = "Posts" }
            );

            //routes,MapRoute
            routes.MapRouteLowercase(
                name: "Blog",
                url: "blog/{id}",
                defaults: new { controller = "Home", action = "Blog", id = UrlParameter.Optional } 
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Blogs",
                url: "blogs/posts/",
                defaults: new { controller = "Home", action = "Blogs" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Category",
                url: "posts/category/{id}",
                defaults: new { controller = "Home", action = "category", id = UrlParameter.Optional }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Cast",
                url: "cast/",
                defaults: new { controller = "Home", action = "Cast" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "CastMember",
                url: "cast/member/{id}",
                defaults: new { controller = "Home", action = "CastMember", id = UrlParameter.Optional }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Comic",
                url: "comic/{id}",
                defaults: new { controller = "Home", action = "Comic", id = UrlParameter.Optional }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Comics",
                url: "comic/archives/{id}",
                defaults: new { controller = "Home", action = "Comics", id = UrlParameter.Optional }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Extras",
                url: "extras/",
                defaults: new { controller = "Home", action = "Wallpapers" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Post",
                url: "post/{id}",
                defaults: new { controller = "Home", action = "Post", id = "" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Rss",
                url: "Rss",
                defaults: new { controller = "Home", action = "ComicFeed" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Search",
                url: "search/{id}",
                defaults: new { controller = "Home", action = "Search", id = "" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Tag",
                url: "posts/tag/{id}",
                defaults: new { controller = "Home", action = "Tag", id = "id" }
            );

            //routes.MapRoute
            routes.MapRouteLowercase(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}