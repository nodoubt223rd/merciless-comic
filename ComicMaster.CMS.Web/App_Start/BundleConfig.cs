﻿using System.Web;
using System.Web.Optimization;

namespace ComicMaster.CMS.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            bundles.IgnoreList.Clear();

            var cdnPath = "http://fonts.googleapis.com/css?family=Lato:300,400,700";

            bundles.Add(new ScriptBundle("~/bundles/gridmvc").Include(
                        "~/Scripts/gridmvc.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.9.1.min.js",
                        "~/Scripts/jquery-migrate-1.1.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-1.8.21.custom.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*",
                        "~/Scripts/jquery.alert.js"));

            bundles.Add(new ScriptBundle("~/bundles/easing").Include(
                       "~/Scripts/jquery.unobtrusive-ajax.min.js",
                        "~/Scripts/jquery-migrate-1.1.1.min.js",
                        "~/Scripts/jquery.easing.min.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/themes/MercilessTheme/css/bootstrap.css",
                "~/Content/themes/MercilessTheme/css/site.css",
                "~/Content/Gridmvc.css"));

            bundles.Add(new StyleBundle("~/fonts", cdnPath));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new ScriptBundle("~/bundles/misc").Include(
                        "~/Scripts/bootstrap.min.js",
                        "~/Scripts/jquery.event.drag.js",
                        "~/Scripts/jquery.event.drop.js",
                        "~/Scripts/jquery.roundabout.min.js",
                        "~/Scripts/jquery.touchSwipe.min.js",
                        "~/Scripts/jquery.caroufredsel.js",
                        "~/Scripts/jquery.tweet.js",
                        "~/Scripts/blueimp-helper.js",
                        "~/Scripts/blueimp-gallery.js",
                        "~/Scripts/blueimp-gallery-fullscreen.js",
                        "~/Scripts/blueimp-gallery-indicator.js",
                        "~/Scripts/jquery.blueimp-gallery.js"));

            bundles.Add(new Bundle("~/bundles/dashstyles").Include(
                        "~/Content/dash/bootstrap.min.css",
                        "~/Content/dash/bootstrap-responsive.min.css",
                        "~/Content/dash/cus-icons.css",
                        "~/Content/dash/font-awesome.min.css",
                        "~/Content/dash/jarvis-widgets.css",
                        "~/Content/dash/DT_bootstrap.css",
                        "~/Content/dash/responsive-tables.css",
                        "~/Content/dash/uniform.default.css",
                        "~/Content/dash/select2.css",
                        "~/Content/dash/theme.css",
                        "~/Content/dash/theme-responsive.css",
                        "~/Content/dash/datepicker.css",
                        "~/Content/dash/fullcalendar.css",
                        "~/Content/dash/TableTools.css",
                        "~/Content/dash/bootstrap-wysihtml5.css",
                        "~/Content/dash/wysiwyg-color.css",
                        "~/Content/dash/toastr.custom.css",
                        "~/Content/dash/toastr-responsive.css",
                        "~/Content/dash/jquery.jgrowl.css"));

            bundles.Add(new ScriptBundle("~/bundles/dash").Include(
                        "~/Scripts/dash/include/jquery.ui.touch-punch.min.js",
                        "~/Scripts/dash/include/selectnav.min.js",
                        "~/Scripts/dash/include/jquery.accordion.min.js",
                        "~/Scripts/dash/include/toastr.min.js",
                        "~/Scripts/dash/include/jquery.jgrowl.min.js",
                        "~/Scripts/dash/include/slimScroll.min.js",
                        "~/Scripts/dash/include/DT_bootstrap.min.js",
                        "~/Scripts/dash/include/jquery.uniform.min.js",
                        "~/Scripts/dash/include/amchart/amcharts.js",
                        "~/Scripts/dash/include/amchart/amchart-draw1.js",
                        "~/Scripts/dash/include/jquery.ibutton.min.js",
                        "~/Scripts/dash/include/raphael.2.1.0.min.js",
                        "~/Scripts/dash/include/justgage.min.js",
                        "~/Scripts/dash/include/morris.min.js",
                        "~/Scripts/dash/include/morris-chart-settings.js",
                        "~/Scripts/dash/include/jquery.easy-pie-chart.min.js",
                        "~/Scripts/dash/include/jarvis.widget.min.js",
                        "~/Scripts/dash/include/mobiledevices.min.js",
                        "~/Scripts/dash/include/jquery.fullcalendar.min.js",
                        "~/Scripts/dash/include/jquery.flot.cust.min.js",
                        "~/Scripts/dash/include/jquery.flot.resize.min.js",
                        "~/Scripts/dash/include/jquery.flot.tooltip.min.js",
                        "~/Scripts/dash/include/jquery.flot.orderBar.min.js",
                        "~/Scripts/dash/include/jquery.flot.fillbetween.min.js",
                        "~/Scripts/dash/include/jquery.flot.pie.min.js",
                        "~/Scripts/dash/include/jquery.sparkline.min.js",
                        "~/Scripts/dash/include/jquery.inbox.slashc.sliding-menu.js",
                        "~/Scripts/dash/include/jquery.modal.min.js",
                        "~/Scripts/dash/include/jquery.validate.min.js",
                        "~/Scripts/dash/include/bootstrap-progressbar.min.js",
                        "~/Scripts/dash/include/wysihtml5-0.3.0.min.js",
                        "~/Scripts/dash/include/bootstrap-wysihtml5.min.js",
                        "~/Scripts/dash/include/jquery.maskedinput.min.js",
                        "~/Scripts/dash/include/bootstrap-datepicker.min.js",
                        "~/Scripts/dash/include/bootstrap.wizard.min.js",
                        "~/Scripts/dash/include/bootstrap-colorpicker.min.js",
                        "~/Scripts/dash/include/bootstrap-timepicker.min.js",
                        "~/Scripts/dash/include/bootbox.min.js",
                        "~/Scripts/dash/include/bootstrap.min.js",
                        "~/Scripts/dash/config.js",
                        "~/Scripts/dash/include/d3.v3.min.js",
                        "~/Scripts/dash/adv_charts/d3-chart-1.js",
                        "~/Scripts/dash/adv_charts/d3-chart-2.js",
                        "~/Scripts/knockout.all-2.4.0.min.js",
                        "~/Scripts/dash/include/ZeroClipboard.min.js",
                        "~/Scripts/dash/include/select2.min.js",
                        "~/Scripts/dash/include/jquery.placeholder.min.js",
                        "~/Scripts/dash/include/jquery.excanvas.min.js",
                        "~/Scripts/dash/include/TableTools.min.js",
                        "~/Scripts/dash/modaldialog.js"));
        }
    }
}