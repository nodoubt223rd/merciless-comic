﻿using System;
using System.Web;
using System.Web.Mvc;

using ComicMaster.CMS.Core.CM_Principal;

namespace ComicMaster.CMS.Web
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new CustomPrincipal User
        {
            get { return base.User as CustomPrincipal; }
        }
    }
}