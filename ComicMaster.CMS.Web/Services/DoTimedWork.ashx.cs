﻿using System;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Core;
using ComicMaster.CMS.Core.Interfaces;

namespace ComicMaster.CMS.Web.Services
{
    public class DoTimedWork : IHttpHandler
    {
        private readonly IComicService _comicService;
        readonly Logger Log = new Logger();

        public DoTimedWork()
            :this(new ComicService())
        {
        }

        public DoTimedWork(IComicService comicService)
        {
            _comicService = comicService;
        }

        public void ProcessRequest(HttpContext context)
        {            
            if (context.Request["dowork"] != "false") // don't do work if it's just monastic hitting the page (to make sure the  timer is running)
            {
                Log.Info("Comic publishing event has started on " + DateTime.Now.ToLongDateString());

                context.Server.ScriptTimeout = 1800; // 30 minutes

                DateTime day = DateTime.Now;
                DateTime _time = DateTime.Now.ToLocalTime();
                TimeSpan time = new TimeSpan();

                if (TimeSpan.TryParse(_time.ToShortTimeString(), out time))
                {
                    var comic = _comicService.Single(c => c.ComicDate == day && c.PublishTime == time);

                    comic.OnHomePage = true;

                    var status = _comicService.PublishComic(comic);

                    if ( status == Constants.Success)
                    {
                        Log.Info("Comic publishing event has completed successfully on " + DateTime.Now.ToLongDateString());
                    }
                    else
                    {
                        Log.Error("Comic publishing event has failed on " + DateTime.Now.ToLongDateString());
                    }
                }

            }
            context.Response.Write("done");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        protected DateTime LastSuccessfulRun
        {
            get
            {
                try
                {
                    return DateTime.Parse(HttpContext.Current.Request["LastSuccessfulRun"]);
                }
                catch
                {
                    return DateTime.Now.AddDays(-1);
                }
            }
        }
    }
}