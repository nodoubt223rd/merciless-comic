﻿//home-index.js
function homeIndexController($scope, $http) {
    $scope.data = [];
    $scope.isBusy = true;

    $http.get("/api/comic/getcurrent", { cache: true})
    .then(function (result) {
        angular.copy(result.data.data, $scope.data);
    },
    function (){
        //Error
        $scope.error = "No comics are currently loaded.";
        $scope.loading = false;
    }).then(function() {
        $scope.isBusy = false;
    });
}