﻿function CategoryController($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/category/gethome/10').success(function (data) {
        $scope.categories = data.data;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An error has occurred while loading the categories!";
        $scope.loading = false;
    });

}