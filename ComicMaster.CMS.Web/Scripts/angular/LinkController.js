﻿function LinkController($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/link/get').success(function (data) {
        $scope.authors = data.data;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An error has occurred while loading the external links!";
        $scope.loading = false;
    });
}
