﻿function PostController($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/post/getall/3').success(function (data) {
        $scope.posts = data.data;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An error has occurred while loading posts!";
        $scope.loading = false;
    });

}