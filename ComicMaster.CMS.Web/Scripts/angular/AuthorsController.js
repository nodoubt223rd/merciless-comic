﻿function AuthorsController($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/author/get').success(function(data) {
        $scope.authors = data.data;
        $scope.loading = false;
    })
    .error(function() {
        $scope.error = "An error has occurred while loading the authors!";
        $scope.loading = false;
    });

}