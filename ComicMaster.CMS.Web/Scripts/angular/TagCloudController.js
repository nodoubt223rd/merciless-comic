﻿//TagCloudController.js
function TagCloudController($scope, $http) {
    $scope.data = [];
    $scope.isBusy = true;

    $http.get("/api/tag/get")
        .then(function(result) {
            //Successful
            angular.copy(result.data.data, $scope.data);
        },
        function() {
            //Error
            $scope.error = "An error has occurred while loading the tags!";
            $scope.loading = false;
        })
        .then(function () {
            $scope.isBusy = false;
        });
}