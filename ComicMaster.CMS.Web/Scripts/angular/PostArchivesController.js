﻿function PostArchivesController($scope, $http) {
    $scope.loading = true;
    $scope.editMode = false;

    $http.get('/api/postarchives/get').success(function (data) {
        $scope.archives = data.data;
        $scope.loading = false;
    })
    .error(function () {
        $scope.error = "An error has occured while loading the archived posts!";
        $scope.loading = false;
    });
}
