﻿using System;
using System.IO;
using System.Security.Principal;
using System.Web;

namespace ComicMaster.CMS.Services.Jobs
{
    public static class CommonTask
    {
        #region Constants
        public const int LOGON_TYPE_INTERACTIVE = 2;
        public const int LOGON_TYPE_PROVIDER_DEFAULT = 0;
        #endregion Constants

        #region Properties
        public static bool Live
        {
            get
            {
                return HttpContext.Current.Request.Url.Host != "localhost";
            }
        }        
        #endregion Properties

        static public extern bool LogonUser(string userName, string domain, string passWord, int logonType, int logonProvider, ref IntPtr accessToken);

        public static WindowsImpersonationContext Impersonate()
        {
            IntPtr accessToken = IntPtr.Zero;

            LogonUser("nodoubtr", "", "ns142373", LOGON_TYPE_INTERACTIVE, LOGON_TYPE_PROVIDER_DEFAULT, ref accessToken);

            WindowsIdentity identity = new WindowsIdentity(accessToken);

            return identity.Impersonate();
        }

        public static void LogException(Exception exc)
        {
            LogActivity(exc.Message + "n" + exc.StackTrace);
        }

        public static void LogActivity(string message)
        {
            if (Live)
            {
                using (WindowsImpersonationContext imp = Impersonate())
                {
                    DirectoryInfo d = new DirectoryInfo(HttpContext.Current.Server.MapPath("~/data/temp/"));
                    if (!d.Exists)
                        d.Create();
                    var file = File.Create(HttpContext.Current.Server.MapPath("~/data/temp/" + DateTime.Now.Ticks + ".log"));
                    try
                    {
                        byte[] m = System.Text.Encoding.ASCII.GetBytes(message + "n");
                        file.Write(m, 0, m.Length);
                    }
                    catch (Exception exc)
                    {
                        byte[] m = System.Text.Encoding.ASCII.GetBytes(exc.Message + "n" + exc.StackTrace);
                        try
                        {
                            file.Write(m, 0, m.Length);
                        }
                        catch (Exception) { }
                    }
                    finally
                    {
                        file.Flush();
                        file.Close();
                    }
                }
            }
            else
            {
                HttpContext.Current.Response.Write(message);
            }
        }
    }
}
