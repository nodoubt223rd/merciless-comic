﻿using System;

namespace ComicMaster.CMS.Services.Jobs
{
    public class Setting
    {
        public DateTime? LastRun { get; set; }
        public DateTime? LastSuccessfulRun { get; set; }
        public long RunCount { get; set; }
    }
}
