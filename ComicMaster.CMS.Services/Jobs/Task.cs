﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;

using ComicMaster.CMS.Common.Logging;
using ComicMaster.CMS.Common.Utils;

namespace ComicMaster.CMS.Services.Jobs
{    
    /// <summary>
    /// Base class for ASP.NET background tasks.
    /// </summary>
    /// readonly Logger log = new Logger();
    public abstract class Task : IDisposable
    {
        #region Instance Fields
        private readonly object _lock = new object();
        private bool _isRunning;
        private DateTime _started;
        #endregion Instance Fields

        #region Properties
        /// <summary>
        /// Gets <see cref="HttpApplication"/> context associated with this task.
        /// </summary>
        public HttpApplication Context { get; internal set; }

        /// <summary>
        /// Gets / sets if the task is running.
        /// </summary>
        virtual public bool IsRunning
        {
            get
            {
                lock (this.Lock)
                    return _isRunning;
            }
            protected set
            {
                lock (this.Lock)
                {
                    if (!_isRunning && value)
                        _started = DateTime.Now;

                    _isRunning = value;
                }
            }
        }

        /// <summary>
        /// Gets if the task can be stopped.
        /// </summary>
        virtual public bool IsStoppable { get { return true; } }

        /// <summary>
        /// Gets a synchronization lock for this task.
        /// </summary>
        virtual protected object Lock { get { return _lock; } }

        /// <summary>
        /// Gets the <see cref="DateTime"/> the task was started.
        /// </summary>
        virtual public DateTime Started { get { return _started; } }
        #endregion Properties

        #region IDisposable Implementation
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);

            GC.SuppressFinalize(this);
        }
        #endregion IDisposable Implementation

        #region Methods
		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting managed and unmanaged resources.
		/// </summary>
		/// <param name="disposing"><see langword="true"/> to release managed and unmanaged resources, <see langword="false"/> to release only unmanaged resources.</param>
		virtual protected void Dispose(bool disposing)
		{
			this.IsRunning = false;
		}

		/// <summary>
		/// When overridden in derived classes, performs a task once.
		/// </summary>
		/// <seealso cref="Run"/>
		abstract protected void RunOnce();

		/// <summary>
		/// Runs (starts) the task.
		/// </summary>
		/// <seealso cref="RunOnce"/>
		virtual public void Run()
		{
			this.IsRunning = true;

			RunOnce();

			this.IsRunning = false;
		}
		#endregion Methods
	}

	/// <summary>
	/// Represents an ASP.NET background task that occurs periodically.
	/// </summary>
	public class PeriodicBackgroundTask : Task
	{
		#region Instance Fields
		private Timer _timer;
		private WindowsIdentity _primaryIdentity;
		#endregion Instance Fields

		#region Properties
		/// <summary>
		/// Gets / sets the task period in milliseconds.
		/// </summary>
		public long PeriodMs { get; set; }

		/// <summary>
		/// Gets / sets the task period <see cref="TimeSpan"/>.
		/// </summary>
		public TimeSpan Period
		{
			get { return TimeSpan.FromMilliseconds(this.PeriodMs); }
			set { this.PeriodMs = (long)value.TotalMilliseconds; }
		}

		/// <summary>
		/// Gets / sets the milliseconds to delay before the task starts.
		/// </summary>
		public long StartDelayMs { get; set; }

		/// <summary>
		/// Gets /sets the start delay <see cref="TimeSpan"/>.
		/// </summary>
		public TimeSpan StartDelay
		{
			get { return TimeSpan.FromMilliseconds(this.StartDelayMs); }
			set { this.StartDelayMs = (long)value.TotalMilliseconds; }
		}

		/// <summary>
		/// Gets the underlying <see cref="System.Threading.Timer"/>.
		/// </summary>
		protected Timer Timer { get { return _timer; } }
		#endregion Properties

		#region BackgroundTaskBase Overrides
		/// <summary>
		/// When overridden in derived classes, performs the periodic task once.
		/// </summary>
		/// <seealso cref="OnTimer"/>
		protected override void RunOnce()
		{
		}

		/// <summary>
		/// Starts the periodic task.
		/// </summary>
		public override void Run()
		{
			_primaryIdentity = WindowsIdentity.GetCurrent();

			this.IsRunning = true;

			_timer = new Timer(new TimerCallback(OnTimer), null, this.StartDelayMs, this.PeriodMs);
		}
		#endregion BackgroundTaskBase Overrides

		#region Methods
		/// <summary>
		/// Handles the signal to execute the periodic task.
		/// </summary>
		/// <param name="sender">The <see cref="System.Threading.Timer"/> event sender.</param>
		/// <seealso cref="RunOnce"/>
		protected virtual void OnTimer(object sender)
		{
			if (Monitor.TryEnter(this))
			{
				WindowsImpersonationContext ic = null;

				if (_primaryIdentity != null)
					ic = _primaryIdentity.Impersonate();

				try
				{
					this.RunOnce();
				}
				finally
				{
					Monitor.Exit(this);

					if (ic != null)
						ic.Undo();
				}
			}
		}
		#endregion Methods
	}

	public class TaskModule : IHttpModule
	{
		#region Constants
		private const string LOG_NAME = "cp_task_module";

		private const string KEY = "$$cp.web.TaskModule";
		#endregion Constants

		#region Static Fields
		private static HttpApplication _context;
		private static bool _initialized;
		private readonly static object _lock = new object();
		private readonly static object _taskLock = new object();
		private readonly static Dictionary<string, Task> _tasks = new Dictionary<string, Task>();
        readonly Logger log = new Logger();
		#endregion Static Fields

		#region Static Properties
		/// <summary>
		/// Gets the current <c>TaskModule</c>.
		/// </summary>
		public static TaskModule Current
		{
			get
			{
				if (HttpContext.Current != null)
					return HttpContext.Current.Application[KEY] as TaskModule;

				return null;
			}
		}
		#endregion Static Properties

		#region Properties
		/// <summary>
		/// Gets the count of tasks.
		/// </summary>
		public int TaskCount
		{
			get { return _tasks.Count; }
		}

		/// <summary>
		/// Gets a list of current task names.
		/// </summary>
		public List<string> TaskList
		{
			get
			{
				lock (_taskLock)
					return _tasks.Keys.ToList();
			}
		}
		#endregion Properties

		#region IHttpModule Implementation
		/// <summary>
		/// Disposes of the resources (other than memory) used by the module
		/// </summary>
		public void Dispose()
		{
            if (0 == _tasks.Count)
                return;

            new List<string>(_tasks.Keys).ForEach(t => StopTask(t));
		}

		/// <summary>
		/// Initializes a module and prepares it to handle requests.
		/// </summary>
		/// <param name="context">An <see cref="HttpApplication"/> that provides access to the methods, properties, and events common to all application objects within an ASP.NET application.</param>
		public void Init(HttpApplication context)
		{
			lock (_lock)
			{
				if (_initialized)
					return;

				_context = context;
				context.Application[KEY] = this;

                _initialized = true;

                log.Info("CP Task Module Started");

                if (TaskModuleConfigurationSection.Current.Tasks.Count > 0)
                {
                    log.Info("CP Task Module Starting designated tasks...");

                    foreach (TaskModuleTaskElement t in TaskModuleConfigurationSection.Current.Tasks)
                        try
                        {
                            PeriodicBackgroundTask instance = Activator.CreateInstance(Type.GetType(t.Type)) as PeriodicBackgroundTask;

                            if (instance != null)
                            {
                                instance.PeriodMs = t.Period;
                                instance.StartDelayMs = t.StartDelay;

                                StartTask(t.Name, instance);
                            }
                            else
                               log.Warn(string.Format("Invalid task type for \"{0}\": {1}", t.Name, t.Type));
                        }
                        catch (Exception e)
                        {
                            log.Error("Error running the task - ", e);
                        }
                }
			}
		}
		#endregion IHttpModule Implementation		

		#region Methods
		/// <summary>
		/// Tests if a task is running.
		/// </summary>
		/// <param name="name">The task name.</param>
		/// <returns><see langword="true"/> if the task is running, otherwise <see langword="false"/>.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="name"/> was <see langword="null"/> or <see cref="string.Empty."/>.</exception>
		public bool IsTaskRunning(string name)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");

			lock (_taskLock)
			{
				Task b = null;

				if (_tasks.TryGetValue(name, out b))
					return b.IsRunning;

				return false;
			}
		}

		/// <summary>
		/// Starts a task.
		/// </summary>
		/// <param name="name">The task name.</param>
		/// <param name="b">The <see cref="BackgroundTaskBase"/> implementation to start.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name"/> was <see langword="null"/> or <see cref="string.Empty."/> or <paramref name="b"/> was <see langword="null"/></exception>
		public void StartTask(string name, Task b)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");

			if (null == b)
				throw new ArgumentNullException("b");

			if (!_initialized)
				return;

			b.Context = _context;
			b.Run();

			lock (_taskLock)
			{
				if (_tasks.ContainsKey(name))
					_tasks[name] = b;
				else
					_tasks.Add(name, b);
			}

			log.Info(string.Format("Started task \"{0}\" ({1})", name, b.GetType().FullName));
		}

		/// <summary>
		/// Stops a task.
		/// </summary>
		/// <param name="name">The task name.</param>
		/// <exception cref="ArgumentNullException"><paramref name="name"/> was <see langword="null"/> or <see cref="string.Empty."/>.</exception>
		public void StopTask(string name)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");

			if (!_initialized)
				return;

			Task b = null;

			if (!_TryGetTask(name, out b))
				return;

			if (!(b.IsRunning && b.IsStoppable))
				return;

			lock (_taskLock)
			{
				if (_tasks.ContainsKey(name))
					_tasks.Remove(name);

				b.Dispose();

				log.Info(string.Format("Stopped task \"{0}\" ({1})", name, b.GetType().FullName));
			}
		}

		/// <summary>
		/// Tests if a task exists.
		/// </summary>
		/// <param name="name">The task name.</param>
		/// <returns><see langword="true"/> if the task is exists, otherwise <see langword="false"/>.</returns>
		/// <exception cref="ArgumentNullException"><paramref name="name"/> was <see langword="null"/> or <see cref="string.Empty."/>.</exception>
		public bool TaskExists(string name)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");

			lock (_taskLock)
				return _tasks.ContainsKey(name);
		}
		#endregion Methods

		#region Implementation
		private bool _TryGetTask(string name, out Task b)
		{
			if (string.IsNullOrEmpty(name))
				throw new ArgumentNullException("name");

			lock (_taskLock)
				return _tasks.TryGetValue(name, out b);
		}
		#endregion Implementation
	}

    public class TaskModuleConfigurationSection : ConfigurationSection
    {
        #region Constants
        public const string SectionName = "tctmdTaskModule";

        private const string TASKS = "tasks";
        #endregion Constants

        #region Static Properties
        static public TaskModuleConfigurationSection Current
        {
            get { return (TaskModuleConfigurationSection)ConfigurationManager.GetSection(SectionName); }
        }
        #endregion Static Properties

        #region Properties
        [ConfigurationProperty(TASKS, IsRequired = true)]
        public TaskModuleTaskElementCollection Tasks
        {
            get { return (TaskModuleTaskElementCollection)base[TASKS]; }
        }
        #endregion Properties
    }

    public class TaskModuleTaskElement : NamedConfigurationElement
    {
        #region Constants
        private const string TYPE = "type";
        private const string PERIOD = "period";
        private const string START_DELAY = "startDelay";
        #endregion Constants

        #region Properties
        [ConfigurationProperty(TYPE, IsRequired = true)]
        public string Type
        {
            get { return (string)base[TYPE]; }
            set { base[TYPE] = value; }
        }

        [ConfigurationProperty(PERIOD, IsRequired = false, DefaultValue = "0")]
        [DefaultSettingValue("0")]
        public long Period
        {
            get { return (long)base[PERIOD]; }
            set { base[TYPE] = value; }
        }

        [ConfigurationProperty(START_DELAY, IsRequired = false, DefaultValue = "0")]
        [DefaultSettingValue("0")]
        public long StartDelay
        {
            get { return (long)base[START_DELAY]; }
            set { base[START_DELAY] = value; }
        }
        #endregion Properties
    }

    public class TaskModuleTaskElementCollection : NamedConfigurationElementCollection<TaskModuleTaskElement>
    {
    }
}
