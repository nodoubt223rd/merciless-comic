﻿using System;
using System.Drawing;

using ComicMaster.CMS.Common;
using ComicMaster.CMS.Common.Logging;

namespace ComicMaster.CMS.Services.Images
{
    public static class GenerateThumbNail
    {
        public static bool Create(string sourcefile, string destinationfile)
        {
            Logger Log = new Logger();

            System.Drawing.Image image = System.Drawing.Image.FromFile(System.Web.HttpContext.Current.Server.MapPath(sourcefile));
            int srcWidth = image.Width;
            int srcHeight = image.Height;
            int thumbWidth = 47;
            int thumbHeight;
            Bitmap bmp;
            bool success = false;

            if (srcHeight > srcWidth)
            {
                thumbHeight = (srcHeight / srcWidth) * thumbWidth;
                bmp = new Bitmap(thumbWidth, thumbHeight);
            }
            else
            {
                thumbHeight = thumbWidth;
                thumbWidth = (srcWidth / srcHeight) * thumbHeight;
                bmp = new Bitmap(thumbWidth, thumbHeight);
            }

            try
            {
                System.Drawing.Graphics gr = System.Drawing.Graphics.FromImage(bmp);
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                gr.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                System.Drawing.Rectangle rectDestination = new System.Drawing.Rectangle(0, 0, thumbWidth, thumbHeight);
                gr.DrawImage(image, rectDestination, 0, 0, srcWidth, srcHeight, GraphicsUnit.Pixel);
                bmp.Save(System.Web.HttpContext.Current.Server.MapPath(destinationfile));

                success = true;
            }
            catch (Exception e)
            {
                Log.Error("[Error creating the thumbnail image - ", e);
            }
            finally
            {
                bmp.Dispose();
                image.Dispose();
            }

            return success;
        }
    }
}
