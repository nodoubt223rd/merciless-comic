﻿using System;

namespace ComicMaster.CMS.Services.Images
{
    public static class ImageType
    {
        public static string GetImageType(string mimeType)
        {
            switch (mimeType)
            {
                case "image/bmp":
                    {
                        mimeType ="bmp";
                        break;
                    }
                case "image/gif":
                    {
                        mimeType = "gif";
                        break;
                    }
                case "image/jpeg":
                    {
                        mimeType = "jpg";
                        break;
                    }
                case "image/tiff":
                    {
                        mimeType = "tif";
                        break;
                    }
                case "image/x-icon":
                    {
                        mimeType = "ico";
                        break;
                    }
                default:
                    {
                        mimeType = "png";
                        break;
                    }
            }

            return mimeType;
        }
    }
}
