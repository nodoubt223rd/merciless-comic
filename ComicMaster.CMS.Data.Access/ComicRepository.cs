﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class ComicRepository : RepositoryBase<Comic>, IComicRepository
    {
        public ComicRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ComicRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
