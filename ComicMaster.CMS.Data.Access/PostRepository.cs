﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class PostRepository : RepositoryBase<Post>, IPostRepository
    {
        public PostRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public PostRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
