﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class SiteSettingsRepository : RepositoryBase<SiteSetting>, ISiteSettingsRepository
    {
        public SiteSettingsRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public SiteSettingsRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
