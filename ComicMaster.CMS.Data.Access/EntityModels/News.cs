//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ComicMaster.CMS.Data.Access.EntityModels
{
    using System;
    using System.Collections.Generic;
    
    public partial class News
    {
        public int NewsId { get; set; }
        public Nullable<int> ComicId { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
        public System.DateTime NewsDate { get; set; }
        public string NewsSlug { get; set; }
    }
}
