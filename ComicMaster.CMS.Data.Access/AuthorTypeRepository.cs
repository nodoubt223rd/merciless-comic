﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class AuthorTypeRepository : RepositoryBase<AuthorType>, IAuthorTypeReposiotry
    {
        public AuthorTypeRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public AuthorTypeRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
