﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class MediaLibraryRepository : RepositoryBase<MediaLibrary>, IMediaLibraryRepository
    {
        public MediaLibraryRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public MediaLibraryRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {        
        }
    }
}
