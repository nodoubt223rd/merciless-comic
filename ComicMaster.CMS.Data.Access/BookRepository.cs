﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class BookRepository : RepositoryBase<Book>, IBookRepository
    {
        public BookRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BookRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
