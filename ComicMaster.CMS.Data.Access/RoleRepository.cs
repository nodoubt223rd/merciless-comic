﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class RoleRepository : RepositoryBase<aspnet_Roles>, IRoleRepository
    {
        public RoleRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public RoleRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
