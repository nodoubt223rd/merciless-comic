﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class MembershipRepository : RepositoryBase<aspnet_Membership>, IMembershipRepository
    {
        public MembershipRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public MembershipRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
