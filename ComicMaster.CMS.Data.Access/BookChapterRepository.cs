﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class BookChapterRepository : RepositoryBase<BookChapter>, IBookChapterRepository
    {
        public BookChapterRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BookChapterRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
