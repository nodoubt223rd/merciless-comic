﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;

using ComicMaster.CMS.Data.Access.Interfaces;
using ComicMaster.CMS.Data.Access.Infrastructure;

namespace ComicMaster.CMS.Data.Access
{
    public abstract class RepositoryBase<T> : IRepository<T>
        where T : class
    {        
        public IObjectSet<T> ObjectSet
        {
            get
            {
                return _objectSet;
            }
        }

        public RepositoryBase()
            : this (new ComicMasterRepositoryContext())
        {            
        }

        public RepositoryBase(IRepositoryContext repositoryContext)
        {
            repositoryContext = repositoryContext ?? new ComicMasterRepositoryContext();
            _objectSet = repositoryContext.GetObjectSet<T>();
        }

        #region Private Fields
        private readonly ComicMasterRepositoryContext _context;
        private readonly IObjectSet<T> _objectSet;
        #endregion Private Fields

        #region Properties

        public void Add(T entity) 
        {
            this.ObjectSet.AddObject(entity);
        }

        public void Attach(T entity) 
        {
            this.ObjectSet.Attach(entity);
        }

        public long Count(Expression<Func<T, bool>> whereCondition) 
        {
            return this.ObjectSet.Where(whereCondition).LongCount<T>();
        }

        public long Count() 
        {
            return this.ObjectSet.LongCount<T>();
        }

        public void Delete(T entity) 
        {
            this.ObjectSet.DeleteObject(entity);
        }

        public IList<T> GetAll() 
        {
            return this.ObjectSet.ToList<T>();
        }

        public IList<T> GetAll(Expression<Func<T, bool>> whereCondition) 
        {
            return this.ObjectSet.Where(whereCondition).ToList<T>();
        }

        public IEnumerable<T> GetAllEnumerated() 
        {
            return this.ObjectSet.AsEnumerable<T>();
        }

        public IEnumerable<T> GetAllEnumerated(Expression<Func<T, bool>> whereCondition) 
        {
            return this.ObjectSet.Where(whereCondition).AsEnumerable<T>();
        }

        public IQueryable<T> GetQuery() 
        {
            return this.ObjectSet.AsQueryable<T>();
        }    

        public T Single(Expression<Func<T, bool>> whereCondition) 
        {
            return this.ObjectSet.Where(whereCondition).FirstOrDefault<T>();
        }

        #endregion
    }
}
