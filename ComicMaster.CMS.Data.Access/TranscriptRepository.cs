﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class TranscriptRepository : RepositoryBase<Transcript>, ITranscriptRepository
    {
        public TranscriptRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public TranscriptRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
