﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class ComicTagRepository : RepositoryBase<ComicTag>, IComicTagRepository
    {
        public ComicTagRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ComicTagRepository(IRepositoryContext repositoryContext)
            :base(repositoryContext)
        {            
        }
    }
}
