﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class ChapterRepository : RepositoryBase<Chapter>, IChapterRepository
    {
        public ChapterRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public ChapterRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
