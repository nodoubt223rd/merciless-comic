﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class CastRepository : RepositoryBase<Cast>, ICastRepository
    {
        public CastRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public CastRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
