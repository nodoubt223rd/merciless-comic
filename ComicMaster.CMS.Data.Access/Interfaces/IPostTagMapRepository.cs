﻿using System;

using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Data.Access.Interfaces
{
    public interface IPostTagMapRepository : IRepository<PostTagMap>
    {
    }
}
