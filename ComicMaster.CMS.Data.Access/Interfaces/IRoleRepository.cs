﻿using System;

using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Data.Access.Interfaces
{
    public interface IRoleRepository : IRepository<aspnet_Roles>
    {
    }
}
