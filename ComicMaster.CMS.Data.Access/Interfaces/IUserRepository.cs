﻿using System;

using ComicMaster.CMS.Data.Access.EntityModels;

namespace ComicMaster.CMS.Data.Access.Interfaces
{
    public interface IUserRepository : IRepository<aspnet_Users>
    {
    }
}
