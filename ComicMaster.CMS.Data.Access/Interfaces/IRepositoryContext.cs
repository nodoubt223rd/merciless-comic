﻿using System;
using System.Data.Objects;
using System.Linq;

namespace ComicMaster.CMS.Data.Access.Interfaces
{
    /// <summary>
    /// Context across all repositories
    /// </summary>
    public interface IRepositoryContext
    {
        IObjectSet<T> GetObjectSet<T>() where T : class;

        ObjectContext ObjectContext { get; }

        /// <summary>
        /// Save all changes to all repositories
        /// </summary>
        /// <returns>Integer with number of objects affected</returns>
        int SaveChanges();

        /// <summary>
        /// Terminates the current repository context
        /// </summary>
        void Terminate();
    }
}
