﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class UsersInRoleRepository : RepositoryBase<aspnet_UsersInRoles>,  IUsersInRoleRepository
    {

        public UsersInRoleRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
