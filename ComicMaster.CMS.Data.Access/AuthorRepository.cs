﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class AuthorRepository : RepositoryBase<Author>, IAuthorRespoitory
    {
        public AuthorRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public AuthorRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
