﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class NewsRepository : RepositoryBase<News>, INewsRepository
    {
        public NewsRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public NewsRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
