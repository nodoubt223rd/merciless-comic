﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class ErrorLogRepository : RepositoryBase<Log4Net_Error>, IErrorLogRepository
    {
        public ErrorLogRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public ErrorLogRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
