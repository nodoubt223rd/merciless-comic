﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class UserRepository : RepositoryBase<aspnet_Users>, IUserRepository
    {
        public UserRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public UserRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
