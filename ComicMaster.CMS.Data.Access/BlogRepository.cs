﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class BlogRepository : RepositoryBase<Blog>, IBlogRepository
    {
        public BlogRepository()
            : this(new ComicMasterRepositoryContext())
        {            
        }

        public BlogRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {            
        }
    }
}
