﻿using System;
using System.Linq;
using System.Web;

using ComicMaster.CMS.Data.Access.Interfaces;

using Microsoft.Practices.ServiceLocation;

namespace ComicMaster.CMS.Data.Access.Infrastructure
{
    public class HttpRequestModule : IHttpModule
    {
        public string ModuleName
        {
            get { return "ComicMaster.CMS.Data.Accessl.EF.HttpRequestModule"; }
        }

        public void Init(HttpApplication application)
        {
            application.EndRequest += new EventHandler(this.Application_EndRequest);
        }

        private void Application_EndRequest(object source, EventArgs e)
        {
            ServiceLocator.Current.GetInstance<IRepositoryContext>().Terminate();
        }

        public void Dispose() { }
    }
}
