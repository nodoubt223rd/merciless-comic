﻿using System;
using System.Data.Objects;
using System.Linq;

using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access.Infrastructure
{
    public class ComicMasterRepositoryContext : IRepositoryContext
    {
        private const string OBJECT_CONTEXT_KEY = "ComciMaster.CMS.Data.Access.EF.ComicEntities";

        public IObjectSet<T> GetObjectSet<T>()
            where T : class
        {
            return ContextManager.GetObjectContext(OBJECT_CONTEXT_KEY).CreateObjectSet<T>();
        }

        /// <summary>
        /// Returns the active object context
        /// </summary>
        public ObjectContext ObjectContext
        {
            get
            {
                return ContextManager.GetObjectContext(OBJECT_CONTEXT_KEY);
            }
        }

        public int SaveChanges()
        {
            return this.ObjectContext.SaveChanges();
        }

        public void Terminate()
        {
            ContextManager.SetRepositoryContext(null, OBJECT_CONTEXT_KEY);
        }
    }
}
