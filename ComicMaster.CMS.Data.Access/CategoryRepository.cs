﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class CategoryRepository : RepositoryBase<Category>, ICategoryRepository
    {
        public CategoryRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public CategoryRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
