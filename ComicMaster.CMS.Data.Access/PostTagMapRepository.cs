﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class PostTagMapRepository : RepositoryBase<PostTagMap>, IPostTagMapRepository
    {
        public PostTagMapRepository()
            : this(new ComicMasterRepositoryContext())
        {
        }

        public PostTagMapRepository(IRepositoryContext repositoryContext)
            : base(repositoryContext)
        {
        }
    }
}
