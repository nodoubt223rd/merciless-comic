﻿using System;
using System.Linq;

using ComicMaster.CMS.Data.Access.EntityModels;
using ComicMaster.CMS.Data.Access.Infrastructure;
using ComicMaster.CMS.Data.Access.Interfaces;

namespace ComicMaster.CMS.Data.Access
{
    public class TagRepository : RepositoryBase<Tag>, ITagRepository
    {
        public TagRepository()
            :this(new ComicMasterRepositoryContext())
        {            
        }

        public TagRepository(IRepositoryContext repositoryContext)
            :base(repositoryContext)
        {            
        }
    }
}
