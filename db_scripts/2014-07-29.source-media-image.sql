use [tctmdcms400stage];
go

if not exists (select * from information_schema.columns where [table_schema] = 'dbo' and [table_name] = 'tct_content_meta' and [column_name] = 'source_media_image')
    alter table [dbo].[tct_content_meta]
    add [source_media_image] nvarchar(2000)
go

alter trigger [dbo].[tr_content_meta_iu] on [dbo].[content] after insert, update
as
begin
    select
	    [c].[content_id],
	    [c].[content_language],
	    [f].[folder_name],
	    [a].[pubFolderPath],
	    case
		    when isdate([pd].[meta_value]) = 1 then convert(datetime, [pd].[meta_value])
		    else [c].[date_created]
	    end [published_date],
	    convert(varchar(64), coalesce([t].[meta_value], '')) [tctmd_type],
	    convert(nvarchar(2000), coalesce([ma].[meta_value], '')) [author],
	    convert(nvarchar(2000), [h5].[meta_value]) [html5_video_url],
        convert(nvarchar(2000), [smi].[meta_value]) [source_media_image],
	    coalesce([cc].[comment_count], convert(int, 0)) [comment_count],
	    [cc].[last_comment_date],
        case
            when[cm].[content_id] is null then 0
            else 1
        end [exists]
    into #meta
    from [dbo].[content] [c] (nolock)
    inner join inserted [i]
        on [i].[content_id] = [c].[content_id] and [i].[content_language] = [c].[content_language]
    inner join [dbo].[content_folder_tbl] [f] (nolock)
	    on [f].[folder_id] = [c].[folder_id]
    left outer join [dbo].[content_meta_tbl] [pd] (nolock)
	    on [pd].[content_id] = [c].[content_id] and [pd].[meta_type_id] = 118 and [pd].[content_language] = 1033 -- published date
    left outer join [dbo].[content_meta_tbl] [t] (nolock)
	    on [t].[content_id] = [c].[content_id] and [t].[meta_type_id] = 124 and [t].[content_language] = 1033 -- tctmd type
    left outer join [dbo].[content_meta_tbl] [ma] (nolock)
	    on [ma].[content_id] = [c].[content_id] and [ma].[meta_type_id] = 112 and [ma].[content_language] = 1033 -- author
    left outer join [dbo].[content_meta_tbl] [h5] (nolock)
	    on [h5].[content_id] = [c].[content_id] and [h5].[meta_type_id] = 270 and [h5].[content_language] = 1033 -- html5 video URL
    left outer join [dbo].[content_meta_tbl] [smi] (nolock)
	    on [smi].[content_id] = [c].[content_id] and [smi].[meta_type_id] = 287 and [smi].[content_language] = 1033 -- source media image
    left outer join [dbo].[AssetDataTable] [a] (nolock)
	    on [a].[id] = [c].[asset_id]
    left outer join [dbo].[vw_tct_comment_counts] [cc] (nolock)
	    on [cc].[content_id] = [c].[content_id]
    left outer join [dbo].[tct_content_meta] [cm] (nolock)
        on [cm].[content_id] = [c].[content_id] and [cm].[content_language] = [c].[content_language]

    update [dbo].[tct_content_meta]
    set
        [folder_name] = [m].[folder_name],
        [pubFolderPath] = [m].[pubFolderPath],
        [published_date] = [m].[published_date],
        [tctmd_type] = [m].[tctmd_type],
        [author] = [m].[author],
        [html5_video_url] = [m].[html5_video_url],
        [comment_count] = [m].[comment_count],
        [last_comment_date] = [m].[last_comment_date]
    from [dbo].[tct_content_meta] [c]
    inner join #meta [m]
        on [m].[content_id] = [c].[content_id] and [m].[content_language] = [c].[content_language]
    where [m].[exists] = 1

    insert into [dbo].[tct_content_meta]
    (
        [content_id],
        [content_language],
        [folder_name],
        [pubFolderPath],
        [published_date],
        [tctmd_type],
        [author],
        [html5_video_url],
        [comment_count],
        [last_comment_date],
        [source_media_image]
    )
    select
        [content_id],
        [content_language],
        [folder_name],
        [pubFolderPath],
        [published_date],
        [tctmd_type],
        [author],
        [html5_video_url],
        [comment_count],
        [last_comment_date],
        [source_media_image]
    from #meta
    where [exists] = 0
end
go

alter view [dbo].[vw_tct_active_content]
as
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data], -- deprecated 9.0
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		convert(varchar(25), [c].[content_id]) + '_' + convert(varchar(4), [c].[content_language]) [contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date], -- deprecated 9.0
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[m].[folder_name],
		[m].[pubFolderPath],
		[m].[published_date],
		[m].[tctmd_type],
		[m].[author],
		[m].[html5_video_url],
		[m].[comment_count],
		[m].[last_comment_date],
        [l].[filename] [quick_link],
        convert(nvarchar(512), N'/' + [f].[FolderPath]) [folder_path],
        convert(nvarchar(512), N'/' + [f].[FolderIdPath]) [folder_id_path],
        [f].[hidden] [folder_hidden],
        [as].[meta_value] [article_source],
        [ac].[meta_value] [article_comment],
        convert(bit, case coalesce([cf].[meta_value], 'no') when 'yes' then 1 else 0 end) [featured_item],
        [m].[source_media_image]
	from [dbo].[content] [c] (nolock)
	inner join [dbo].[tct_content_meta] [m] (nolock)
		on [m].[content_id] = [c].[content_id] and [m].[content_language] = [c].[content_language]
    left outer join [dbo].[content_meta_tbl] [as] (nolock)
        on [as].[content_id] = [c].[content_id] and [as].[content_language] = [c].[content_language] and [as].[meta_type_id] = 240
    left outer join [dbo].[content_meta_tbl] [ac] (nolock)
        on [ac].[content_id] = [c].[content_id] and [ac].[content_language] = [c].[content_language] and [ac].[meta_type_id] = 244
    left outer join [dbo].[content_meta_tbl] [cf] (nolock)
        on [cf].[content_id] = [c].[content_id] and [cf].[content_language] = [c].[content_language] and [cf].[meta_type_id] = 242
    inner join [dbo].[content_folder_tbl] [f] (nolock)
        on [f].[folder_id] = [c].[folder_id]
	left outer join [library] [l] (nolock)
		on [l].[content_id] = [c].[content_id] and [l].[content_language] = [c].[content_language]
	where ([c].[content_status] = 'a' or exists (select 1 from [dbo].[content_history] [h] (nolock) where [h].[content_id] = [c].[content_id] and [h].[content_status] = 'a'))
		and ([c].[go_live] is null or [c].[go_live] < getdate())
go

alter view [dbo].[vw_tct_content]
as
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data], -- deprecated 9.0
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		convert(varchar(25), [c].[content_id]) + '_' + convert(varchar(4), [c].[content_language]) [contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date], -- deprecated 9.0
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[m].[folder_name],
		[m].[pubFolderPath],
		[m].[published_date],
		[m].[tctmd_type],
		[m].[author],
		[m].[html5_video_url],
		[m].[comment_count],
		[m].[last_comment_date],
        [l].[filename] [quick_link],
        convert(nvarchar(512), N'/' + [f].[FolderPath]) [folder_path],
        convert(nvarchar(512), N'/' + [f].[FolderIdPath]) [folder_id_path],
        [f].[hidden] [folder_hidden],
        [as].[meta_value] [article_source],
        [ac].[meta_value] [article_comment],
        convert(bit, case coalesce([cf].[meta_value], 'no') when 'yes' then 1 else 0 end) [featured_item],
        [m].[source_media_image]
	from [dbo].[content] [c] (nolock)
	inner join [dbo].[tct_content_meta] [m] (nolock)
		on [m].[content_id] = [c].[content_id] and [m].[content_language] = [c].[content_language]
    left outer join [dbo].[content_meta_tbl] [as] (nolock)
        on [as].[content_id] = [c].[content_id] and [as].[content_language] = [c].[content_language] and [as].[meta_type_id] = 240
    left outer join [dbo].[content_meta_tbl] [ac] (nolock)
        on [ac].[content_id] = [c].[content_id] and [ac].[content_language] = [c].[content_language] and [ac].[meta_type_id] = 244
    left outer join [dbo].[content_meta_tbl] [cf] (nolock)
        on [cf].[content_id] = [c].[content_id] and [cf].[content_language] = [c].[content_language] and [cf].[meta_type_id] = 242
    inner join [dbo].[content_folder_tbl] [f] (nolock)
        on [f].[folder_id] = [c].[folder_id]
	left outer join [library] [l] (nolock)
		on [l].[content_id] = [c].[content_id] and [l].[content_language] = [c].[content_language]
go

alter view [dbo].[vw_tct_active_content_index]
as
    select
        [content_id],
        [content_title],
        [content_html],
        [content_status],
		--[content_meta_data], -- deprecated 9.0
        [content_language],
        [date_created],
        [last_edit_lname],
        [last_edit_fname],
        [last_edit_comment],
        [last_edit_date],
        [user_id],
        [folder_id],
        [inherit_permissions],
        [inherit_permissions_from],
        [inherit_xml],
        [inherit_xml_from],
        [private_content],
        [content_teaser],
        [published],
        [go_live],
        [content_text],
        [end_date],
        [content_type],
        [approval_method],
        [searchable],
        [end_date_action],
        [alias_id],
        [contentfulltextkey],
        [asset_id],
        [asset_version],
        [xml_config_id],
        [template_id],
		--[last_replicated_date], -- deprecated 9.0
        [image],
        [private],
        [flag_def_id],
        [content_subtype],
        [ignore_tracking],
        [folder_name],
        [pubFolderPath],
        [published_date],
        [tctmd_type],
        [author],
        [html5_video_url],
        [comment_count],
        [last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
    from [dbo].[vw_tct_active_content]
    where
        [searchable] = 1
        and [folder_hidden] = 0
        and not [content_type] in (2, 7)
        and not [folder_id_path] like '/16/%'
        and not [folder_id_path] like '/80/%'
        and not [folder_id_path] like '/82/%'
        and not [folder_id_path] like '/88/%'
        and not [folder_id_path] like '/132/%'
        and not [folder_id_path] like '/608968/%'
        and not [folder_id_path] like '/716743/%'
        and not [tctmd_type] = 'introduction'
go

alter view [dbo].[vw_tct_favorite]
as
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname], 
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content], 
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id], 
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype], 
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image],
		[ti].[taxonomy_item_added_user], 
		[ti].[taxonomy_item_date_created],
		[u].[display_name],
		[ti].[taxonomy_item_id],
		[u].[avatar]
	from [dbo].[taxonomy_item_tbl] [ti] (nolock)
	inner join [dbo].[vw_tct_content] [c] (nolock)
		on [c].[content_id] = [ti].[taxonomy_item_id]
	inner join [dbo].[taxonomy_tbl] [t] (nolock)
		on [t].[taxonomy_id] = [ti].[taxonomy_id] and [t].[taxonomy_path] = '\' + convert(varchar, [ti].[taxonomy_item_added_user])  + '\Favorites'
	inner join [dbo].[users] [u] (nolock)
		on [u].[user_id] = [ti].[taxonomy_item_added_user]
go

exec sp_recompile 'vw_tct_active_content'
exec sp_recompile 'vw_tct_content'
exec sp_recompile 'vw_tct_active_content_index'
exec sp_recompile 'vw_tct_favorite'
go

alter procedure [dbo].[tct_site_content_search]
(
	@search nvarchar(32),
	@type nvarchar(2000) = null,
	@page int = 1,
	@page_size int = 50,
	@sort_field nvarchar(255) = 'published_date',
	@descending bit = 1,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if @search is null or len(@search) < 3
		begin
			raiserror('Search must be at least 3 characters', 16, 1)
			
			return
		end

	declare
		@my_search nvarchar(32),
		@my_type nvarchar(2000),
		@my_other_type nvarchar(2000),
		@my_page int,
		@my_page_size int,
		@my_sort_field nvarchar(255),
		@my_descending bit,
		@my_language_id int,
		@my_default_language_id int,
		@start int,
		@end int
	
	set @my_search = '%' + @search + '%'
	set @my_type = @type
	set @my_page = @page - 1
	set @my_page_size = @page_size
	set @my_sort_field = @sort_field
	set @my_descending = @descending
	set @my_other_type = null
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	if 'literature' = @my_type
		set @my_other_type = 'meeting coverage'
		
	if @my_page < 0
		set @my_page = 0
		
	if @my_page_size < 5
		set @my_page_size = 5
		
	if @my_page_size > 255
		set @my_page_size = 255
		
	set @start = @my_page_size * @my_page
	set @end = @start + @my_page_size
		
	if @my_sort_field <> 'published_date' and @my_sort_field <> 'content_title' and @my_sort_field <> 'date_created' and @my_sort_field <> 'last_edit_date' and @my_sort_field <> 'author'
		begin
			raiserror('Invalid sort field', 16, 1)
			
			return
		end
	
	select
		[content_id],
		[content_language]
	into #ids
	from [dbo].[vw_tct_active_content] [c] (nolock)
	where 1 = [c].[searchable]
		and (@my_type is null or [c].[tctmd_type] = @my_type)
		and [content_language] = @my_language_id and
		(
			(@my_type is null and [c].[author] like @my_search) or
			(not @my_type is null and 
				(
					[c].[content_title] like @my_search
					or [c].[content_teaser] like @my_search
					or [c].[author] like @my_search
					or [c].[content_html] like @my_search
				)
			)
		)
	
	if not @my_other_type is null
		insert into #ids
		select
			[content_id],
			[content_language]
		from [dbo].[vw_tct_active_content] [c] (nolock)
		where not [content_id] in (select [content_id] from #ids)
			and 1 = [c].[searchable]
			and [c].[tctmd_type] = @my_other_type
			and [content_language] = @my_language_id and
			(
				[c].[content_title] like @my_search
				or [c].[content_teaser] like @my_search
				or [c].[author] like @my_search
				or [c].[content_html] like @my_search
			)
	
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data],
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date],
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
		[c].[html5_video_url],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image]
	into #res
	from [dbo].[vw_tct_active_content] [c] (nolock)
	inner join #ids [i]
		on [i].[content_id] = [c].[content_id] and [i].[content_language] = [c].[content_language]
		
	select
		[content_id],
		[content_title],
		[content_html],
		[content_status],
		--[content_meta_data],
		[content_language],
		[date_created],
		[last_edit_lname],
		[last_edit_fname],
		[last_edit_comment],
		[last_edit_date],
		[user_id],
		[folder_id],
		[inherit_permissions],
		[inherit_permissions_from],
		[inherit_xml],
		[inherit_xml_from],
		[private_content],
		[content_teaser],
		[published],
		[go_live],
		[content_text],
		[end_date],
		[content_type],
		[approval_method],
		[searchable],
		[end_date_action],
		[alias_id],
		[contentfulltextkey],
		[asset_id],
		[asset_version],
		[xml_config_id],
		[template_id],
		--[last_replicated_date],
		[image],
		[private],
		[flag_def_id],
		[content_subtype],
		[ignore_tracking],
		[folder_name],
		[pubFolderPath],
		[published_date],
		[tctmd_type],
		[author],
		[html5_video_url],
		[comment_count],
		[last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
	from
	(
		select
			row_number() over
			(order by
				case 
					when 'author' = @my_sort_field and 0 = @my_descending then [author]
					when 'content_title' = @my_sort_field and 0 = @my_descending then [content_title]
					when 'date_created' = @my_sort_field and 0 = @my_descending then [date_created]
					when 'last_edit_date' = @my_sort_field and 0 = @my_descending then [date_created]
					when 'published_date' = @my_sort_field and 0 = @my_descending then [published_date]
				end asc,
				case 
					when 'author' = @my_sort_field and 1 = @my_descending then [author]
					when 'content_title' = @my_sort_field and 1 = @my_descending then [content_title]
					when 'date_created' = @my_sort_field and 1 = @my_descending then [date_created]
					when 'last_edit_date' = @my_sort_field and 1 = @my_descending then [date_created]
					when 'published_date' = @my_sort_field and 1 = @my_descending then [published_date]
				end desc
			) [row],
			[content_id],
			[content_title],
			[content_html],
			[content_status],
			--[content_meta_data],
			[content_language],
			[date_created],
			[last_edit_lname],
			[last_edit_fname],
			[last_edit_comment],
			[last_edit_date],
			[user_id],
			[folder_id],
			[inherit_permissions],
			[inherit_permissions_from],
			[inherit_xml],
			[inherit_xml_from],
			[private_content],
			[content_teaser],
			[published],
			[go_live],
			[content_text],
			[end_date],
			[content_type],
			[approval_method],
			[searchable],
			[end_date_action],
			[alias_id],
			[contentfulltextkey],
			[asset_id],
			[asset_version],
			[xml_config_id],
			[template_id],
			--[last_replicated_date],
			[image],
			[private],
			[flag_def_id],
			[content_subtype],
			[ignore_tracking],
			[folder_name],
			[pubFolderPath],
			[published_date],
			[tctmd_type],
			[author],
			[html5_video_url],
			[comment_count],
			[last_comment_date],
            [quick_link],
            [folder_path],
            [folder_id_path],
            [article_source],
            [article_comment],
            [featured_item],
            [source_media_image]
		from #res
	) [data]
	where [row] between @start and @end
	
	select
		[total_items] = count(*),
		case
			when 0 = count(*) % @my_page_size then count(*) / @my_page_size
			else (count(*) / @my_page_size) + 1
		end [total_pages]
	from #res
go

alter procedure [dbo].[tct_content_sitemap]
(
	@max_items int = 100000
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	if @max_items > 100000
		set @max_items = 100000
	
	if @max_items < 1
		set @max_items = 1
	
	set rowcount @max_items

	select
		[content_id],
		[content_title],
		[content_html],
		[content_status],
		--[content_meta_data],
		[content_language],
		[date_created],
		[last_edit_lname],
		[last_edit_fname],
		[last_edit_comment],
		[last_edit_date],
		[user_id],
		[folder_id],
		[inherit_permissions],
		[inherit_permissions_from],
		[inherit_xml],
		[inherit_xml_from],
		[private_content],
		[content_teaser],
		[published],
		[go_live],
		[content_text],
		[end_date],
		[content_type],
		[approval_method],
		[searchable],
		[end_date_action],
		[alias_id],
		[contentfulltextkey],
		[asset_id],
		[asset_version],
		[xml_config_id],
		[template_id],
		--[last_replicated_date],
		[image],
		[private],
		[flag_def_id],
		[content_subtype],
		[ignore_tracking],
		[folder_name],
		[pubFolderPath],
		[published_date],
		[tctmd_type],
		[author],
		[comment_count],
		[last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
	from [dbo].[vw_tct_active_content] (nolock)
	order by [published_date] desc, [last_edit_date] desc
		
	set rowcount 0
go

alter procedure [dbo].[tct_recurse_folder_content]
(
	@folder_id bigint,
	@tctmd_type varchar(64) = null,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;

    set rowcount 255;
	
	declare
		@my_folder_id bigint,
		@my_tctmd_type varchar(64),
		@my_language_id int,
		@my_default_language_id int
	
	set @my_folder_id = @folder_id
	set @my_tctmd_type = @tctmd_type
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	;with [f]([folder_id], [parent_id], [folder_name], [level]) as
	(
		select
			[folder_id],
			[parent_id],
			[folder_name],
			convert(int, 0) [level]
		from [dbo].[content_folder_tbl] (nolock)
		where [folder_id] = @my_folder_id
		union all
		select
			[t].[folder_id],
			[t].[parent_id],
			[t].[folder_name],
			[f].[level] + 1
		from [dbo].[content_folder_tbl] [t] (nolock)
		inner join [f]
			on [t].[parent_id] = [f].[folder_id]
	)
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data], -- deprecated 9.0
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date], -- deprecated 9.0
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
        [c].[html5_video_url],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image]
	from [dbo].[vw_tct_content] [c] (nolock)
	inner join [f]
		on [f].[folder_id] = [c].[folder_id]
	where
        [c].[content_language] = @my_language_id
        and (@my_tctmd_type is null or [c].[tctmd_type] = @my_tctmd_type)
        and [tctmd_type] <> 'Introduction'
    order by [c].[date_created] desc

    set rowcount 0;
go

alter procedure [dbo].[tct_folder_content]
(
	@folder_id bigint,
	@tctmd_type varchar(64) = null,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare
		@my_folder_id bigint,
		@my_tctmd_type varchar(64),
		@my_language_id int,
		@my_default_language_id int
	
	set @my_folder_id = @folder_id
	set @my_tctmd_type = @tctmd_type
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	select
		[content_id],
		[content_title],
		[content_html],
		[content_status],
		--[content_meta_data], -- deprecated 9.0
		[content_language],
		[date_created],
		[last_edit_lname],
		[last_edit_fname],
		[last_edit_comment],
		[last_edit_date],
		[user_id],
		[folder_id],
		[inherit_permissions],
		[inherit_permissions_from],
		[inherit_xml],
		[inherit_xml_from],
		[private_content],
		[content_teaser],
		[published],
		[go_live],
		[content_text],
		[end_date],
		[content_type],
		[approval_method],
		[searchable],
		[end_date_action],
		[alias_id],
		[contentfulltextkey],
		[asset_id],
		[asset_version],
		[xml_config_id],
		[template_id],
		--[last_replicated_date], -- deprecated 9.0
		[image],
		[private],
		[flag_def_id],
		[content_subtype],
		[ignore_tracking],
		[folder_name],
		[pubFolderPath],
		[published_date],
		[tctmd_type],
		[author],
        [html5_video_url],
		[comment_count],
		[last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
	from [dbo].[vw_tct_content] (nolock)
	where
        [folder_id] = @my_folder_id
        and [content_language] = @my_language_id
        and (@my_tctmd_type is null or [tctmd_type] = @my_tctmd_type)
	    and [tctmd_type] <> 'Introduction'
go

alter procedure [dbo].[tct_content_search]
(
	@search nvarchar(32),
	@type nvarchar(2000) = null,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	raiserror('Search is now done through Solr', 16, 1)

    return

	set nocount on;
	set transaction isolation level read uncommitted;
	
	if @search is null or len(@search) < 3
		begin
			raiserror('Search must be at least 3 characters', 16, 1)
			
			return
		end

	declare
		@my_search nvarchar(32),
		@my_type nvarchar(2000),
		@my_language_id int,
		@my_default_language_id int
	
	set @my_search = '%' + @search + '%'
	set @my_type = @type
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	select
		[content_id],
		[content_title],
		[content_html],
		[content_status],
		--[content_meta_data],
		[content_language],
		[date_created],
		[last_edit_lname],
		[last_edit_fname],
		[last_edit_comment],
		[last_edit_date],
		[user_id],
		[folder_id],
		[inherit_permissions],
		[inherit_permissions_from],
		[inherit_xml],
		[inherit_xml_from],
		[private_content],
		[content_teaser],
		[published],
		[go_live],
		[content_text],
		[end_date],
		[content_type],
		[approval_method],
		[searchable],
		[end_date_action],
		[alias_id],
		[contentfulltextkey],
		[asset_id],
		[asset_version],
		[xml_config_id],
		[template_id],
		--[last_replicated_date],
		[image],
		[private],
		[flag_def_id],
		[content_subtype],
		[ignore_tracking],
		[folder_name],
		[pubFolderPath],
		[published_date],
		[tctmd_type],
		[author],
		[html5_video_url],
		[comment_count],
		[last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
	from [dbo].[vw_tct_content] (nolock)
	where [content_language] = @my_language_id and (@my_type is null or [tctmd_type] = @my_type) and
	(
		(@my_type is null and [author] like @my_search) or
		(not @my_type is null and 
			(
				[content_title] like @my_search
				or [content_teaser] like @my_search
				or [author] like @my_search
			)
		)
	)
go

alter procedure [dbo].[tct_content_by_taxonomy]
(
	@taxonomy_id bigint,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare
		@my_taxonomy_id bigint,
		@my_language_id int,
		@my_default_language_id int
	
	set @my_taxonomy_id = @taxonomy_id
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data],
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date],
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
		[c].[html5_video_url],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image]
	from [dbo].[vw_tct_content] [c] (nolock)
	inner join [dbo].[taxonomy_item_tbl] [ti] (nolock)
		on [ti].[taxonomy_item_id] = [c].[content_id]
	where [ti].[taxonomy_id] = @my_taxonomy_id and [c].[content_language] = @my_language_id
go

alter procedure [dbo].[tct_content_by_type]
(
	@type nvarchar(2000),
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare
		@my_type nvarchar(2000),
		@my_language_id int,
		@my_default_language_id int
	
	set @my_type = @type
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	select
		[content_id],
		[content_title],
		[content_html],
		[content_status],
		--[content_meta_data],
		[content_language],
		[date_created],
		[last_edit_lname],
		[last_edit_fname],
		[last_edit_comment],
		[last_edit_date],
		[user_id],
		[folder_id],
		[inherit_permissions],
		[inherit_permissions_from],
		[inherit_xml],
		[inherit_xml_from],
		[private_content],
		[content_teaser],
		[published],
		[go_live],
		[content_text],
		[end_date],
		[content_type],
		[approval_method],
		[searchable],
		[end_date_action],
		[alias_id],
		[contentfulltextkey],
		[asset_id],
		[asset_version],
		[xml_config_id],
		[template_id],
		--[last_replicated_date],
		[image],
		[private],
		[flag_def_id],
		[content_subtype],
		[ignore_tracking],
		[folder_name],
		[pubFolderPath],
		[published_date],
		[tctmd_type],
		[author],
		[html5_video_url],
		[comment_count],
		[last_comment_date],
        [quick_link],
        [folder_path],
        [folder_id_path],
        [article_source],
        [article_comment],
        [featured_item],
        [source_media_image]
	from [dbo].[vw_tct_content] (nolock)
	where [tctmd_type] = @my_type and [content_language] = @my_language_id
go

alter procedure [dbo].[tct_content_by_taxonomy_recursive]
(
	@taxonomy_id bigint,
	@language_id int = 1033,
	@default_language_id int = 1033
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare
		@my_taxonomy_id bigint,
		@my_language_id int,
		@my_default_language_id int
	
	set @my_taxonomy_id = @taxonomy_id
	set @my_language_id = @language_id
	set @my_default_language_id = @default_language_id
	
	;with [f]([taxonomy_id], [taxonomy_parent_id]) as
	(
		select
			[taxonomy_id],
			[taxonomy_parent_id]
		from [dbo].[taxonomy_tbl] (nolock)
		where [taxonomy_id] = @my_taxonomy_id
		union all
		select
			[t].[taxonomy_id],
			[t].[taxonomy_parent_id]
		from [dbo].[taxonomy_tbl] [t] (nolock)
		inner join [f]
			on [t].[taxonomy_parent_id] = [f].[taxonomy_id]
	)
	select distinct
		[taxonomy_id]
	into #tids
	from [f]
	
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data],
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date],
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
		[c].[html5_video_url],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image]
	from [dbo].[vw_tct_content] [c] (nolock)
	inner join [dbo].[taxonomy_item_tbl] [ti] (nolock)
		on [ti].[taxonomy_item_id] = [c].[content_id]
	inner join #tids [f]
		on [f].[taxonomy_id] = [ti].[taxonomy_id]
	where [c].[content_language] = @my_language_id
go

alter procedure [dbo].[tct_content_by_group]
(
	@group_id bigint
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @my_group_id bigint
	declare @my_folder_id bigint
	declare @my_blog_folder_id bigint
	
	set @my_group_id = @group_id
	select @my_folder_id = [folder_id] from [dbo].[taxonomy_tbl] (nolock) where [taxonomy_name] = convert(varchar, @group_id) and [taxonomy_type] = 2
	select @my_blog_folder_id = [folder_id] from [dbo].[content_folder_tbl] (nolock) where [parent_id] = @my_folder_id and [folder_name] = 'Blog'
	
	select
		[c].[content_id],
		[c].[content_title],
		[c].[content_html],
		[c].[content_status],
		--[c].[content_meta_data], -- deprecated 9.0
		[c].[content_language],
		[c].[date_created],
		[c].[last_edit_lname],
		[c].[last_edit_fname],
		[c].[last_edit_comment],
		[c].[last_edit_date],
		[c].[user_id],
		[c].[folder_id],
		[c].[inherit_permissions],
		[c].[inherit_permissions_from],
		[c].[inherit_xml],
		[c].[inherit_xml_from],
		[c].[private_content],
		[c].[content_teaser],
		[c].[published],
		[c].[go_live],
		[c].[content_text],
		[c].[end_date],
		[c].[content_type],
		[c].[approval_method],
		[c].[searchable],
		[c].[end_date_action],
		[c].[alias_id],
		[c].[contentfulltextkey],
		[c].[asset_id],
		[c].[asset_version],
		[c].[xml_config_id],
		[c].[template_id],
		--[c].[last_replicated_date], -- deprecated 9.0
		[c].[image],
		[c].[private],
		[c].[flag_def_id],
		[c].[content_subtype],
		[c].[ignore_tracking],
		[c].[folder_name],
		[c].[pubFolderPath],
		[c].[published_date],
		[c].[tctmd_type],
		[c].[author],
		[c].[html5_video_url],
		[c].[comment_count],
		[c].[last_comment_date],
        [c].[quick_link],
        [c].[folder_path],
        [c].[folder_id_path],
        [c].[article_source],
        [c].[article_comment],
        [c].[featured_item],
        [c].[source_media_image]
	from [dbo].[vw_tct_content] [c] (nolock)
	where [folder_id] = @my_blog_folder_id
	order by [c].[date_created] desc
go

alter procedure [dbo].[tct_favorite_list]
(
	@user_id bigint
)
as
	set nocount on;
	set transaction isolation level read uncommitted;
	
	declare @my_user_id bigint
	
	set @my_user_id = @user_id
	
	select
		[f].[content_id],
		[f].[content_title],
		[f].[content_html],
		[f].[content_status],
		--[f].[content_meta_data],
		[f].[content_language],
		[f].[date_created],
		[f].[last_edit_lname],
		[f].[last_edit_fname],
		[f].[last_edit_comment],
		[f].[last_edit_date],
		[f].[user_id],
		[f].[folder_id],
		[f].[inherit_permissions],
		[f].[inherit_permissions_from],
		[f].[inherit_xml],
		[f].[inherit_xml_from],
		[f].[private_content],
		[f].[content_teaser],
		[f].[published],
		[f].[go_live],
		[f].[content_text],
		[f].[end_date],
		[f].[content_type],
		[f].[approval_method],
		[f].[searchable],
		[f].[end_date_action],
		[f].[alias_id],
		[f].[contentfulltextkey],
		[f].[asset_id],
		[f].[asset_version],
		[f].[xml_config_id],
		[f].[template_id],
		--[f].[last_replicated_date],
		[f].[image],
		[f].[private],
		[f].[flag_def_id],
		[f].[content_subtype],
		[f].[ignore_tracking],
		[f].[folder_name],
		[f].[pubFolderPath],
		[f].[published_date],
		[f].[tctmd_type],
		[f].[author],
		[f].[comment_count],
		[f].[last_comment_date],
        [f].[quick_link],
        [f].[folder_path],
        [f].[folder_id_path],
        [f].[article_source],
        [f].[article_comment],
        [f].[featured_item],
        [f].[source_media_image]
	from [dbo].[vw_tct_favorite] [f] (nolock)
	where [f].[taxonomy_item_added_user] = @my_user_id
go