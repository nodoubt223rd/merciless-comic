use [tctmdcms400stage];
go

alter table [dbo].[tct_oauth_token]
alter column [access_token] nvarchar(max) not null
go

alter table [dbo].[tct_oauth_token]
alter column [token_secret] nvarchar(1024) not null
go

alter table [dbo].[tct_oauth_token]
alter column [verifier_token] nvarchar(1024) not null
go

alter procedure [dbo].[tct_oauth_token_add]
(
    @user_id bigint,
    @social_media_type nvarchar(64),
    @access_token nvarchar(max),
    @token_secret nvarchar(1024),
    @verifier_token nvarchar(1024)
)
as
    set nocount on;
    set transaction isolation level read uncommitted;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)

    set @my_user_id = @user_id
    set @my_social_media_type = @social_media_type

    if exists (select [user_id] from [dbo].[tct_oauth_token] (nolock) where [user_id] = @user_id and [social_media_type] = @my_social_media_type)
        update [dbo].[tct_oauth_token]
        set
            [access_token] = @access_token,
            [token_secret] = @token_secret,
            [verifier_token] = @verifier_token,
            [modified] = getdate()
        where [user_id] = @user_id and [social_media_type] = @my_social_media_type
    else
        insert into [dbo].[tct_oauth_token]
        (
            [user_id],
            [social_media_type],
            [access_token],
            [token_secret],
            [verifier_token],
            [modified]
        )
        values
        (
            @user_id,
            @social_media_type,
            @access_token,
            @token_secret,
            @verifier_token,
            getdate()
        )
go

alter procedure [dbo].[tct_oauth_token_get]
(
    @user_id bigint,
    @social_media_type nvarchar(64)
)
as
    set nocount on;
    set transaction isolation level read uncommitted;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)

    set @my_user_id = @user_id
    set @my_social_media_type = @social_media_type

    select
        [user_id],
        [social_media_type],
        [access_token],
        [token_secret],
        [verifier_token],
        [modified]
    from [dbo].[tct_oauth_token] (nolock)
    where [user_id] = @my_user_id and [social_media_type] = @my_social_media_type
go

alter procedure [dbo].[tct_oauth_token_remove]
(
    @user_id bigint,
    @social_media_type nvarchar(64)
)
as
    set nocount on;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)

    set @my_user_id = @user_id
    set @my_social_media_type = @social_media_type

    delete from [dbo].[tct_oauth_token]
    where [user_id] = @my_user_id and [social_media_type] = @my_social_media_type
go