use[dev_comic_master]
go
if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'cp_add_new_aspnet_users')
	drop procedure [dbo].[cp_add_new_aspnet_users]
go
create procedure [dbo].[cp_add_new_aspnet_users]
(
	@user_id uniqueidentifier,
	@application_id uniqueidentifier,
	@user_name nvarchar(256),
	@is_user_anonymous bit,
	@last_activity_date DATETIME
)
as
	set nocount on;
	set transaction isolation level read uncommitted;

	declare @my_user_id uniqueidentifier
	set @my_user_id = @user_id

	declare @my_application_id uniqueidentifier
	set @my_application_id = @application_id

	declare @my_user_name nvarchar(256)
	set @my_user_name = @user_name

	declare @my_is_user_anonymous bit
	set @my_is_user_anonymous = @is_user_anonymous

	declare @my_last_activity_date DATETIME
	set @my_last_activity_date = @last_activity_date


	insert into [dbo].[aspnet_Users]
		(
			[userid],
			[applicationid],			 
			[username], 
			[isanonymous], 
			[lastactivitydate]
		)
		values
		(
			@my_user_id,
			@my_application_id,
			@my_user_name,
			@my_is_user_anonymous,
			@last_activity_date
		)

		return 0