use[dev_comic_master]
go
if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'cp_add_author_membership')
	drop procedure [dbo].[cp_add_author_membership]
go
create procedure [dbo].[cp_add_author_membership]
(
	@application_id uniqueidentifier,
	@user_id uniqueidentifier,
	@author_id int,
    @password nvarchar(128),
	@password_format int,
    @password_salt nvarchar(128),
    @email nvarchar(256),
	@lowered_email nvarchar(256),
	@first_name nvarchar(150),
	@last_name nvarchar(150),
    @is_approved bit,
	@is_locked_out bit    
)
as
	set nocount on;
	set transaction isolation level read uncommitted;

	declare @my_application_id uniqueidentifier
	set @my_application_id = @application_id

	declare @my_user_id uniqueidentifier
	set @my_user_id = @user_id

	declare @my_author_id int
	set @my_author_id = @author_id

	declare @my_password nvarchar(128)
	set @my_password = @password

	declare @my_password_format int
	set @my_password_format = @password_format

	declare @my_password_salt nvarchar(128)
	set @my_password_salt = @password_salt

	declare @my_email nvarchar(256)
	set @my_email = @email

	declare @my_lowered_email nvarchar(256)
	set @my_lowered_email = @lowered_email

	declare @my_first_name nvarchar(150)
	set @my_first_name = @first_name

	declare @my_last_name nvarchar(150)
	set @my_last_name = @last_name

	declare @my_is_approved bit
	set @my_is_approved = @is_approved

	declare @my_is_locked_out bit
	set @my_is_locked_out = @is_locked_out

	insert into [dbo].[aspnet_Membership]
		(
			[applicationid],
			[userid],
			[authorid],
			[password],
			[passwordformat],
			[passwordsalt],
			[email],
			[loweredemail],
			[firstname],
			[lastname],
			[isapproved],
			[islockedout]
		)
		values
		(
			@my_application_id,
			@my_user_id,
			@my_author_id,
			@my_password,
			@my_password_format,
			@my_password_salt,
			@my_email,
			@my_lowered_email,
			@my_first_name,
			@my_last_name,
			@my_is_approved,
			@my_is_locked_out 
		)