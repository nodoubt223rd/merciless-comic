use[dev_comic_master]
go
if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'cp_add_author_to_role')
	drop procedure [dbo].[cp_add_author_to_role]
go
create procedure [dbo].[cp_add_author_to_role]
(
	@user_id uniqueidentifier,
	@role_id uniqueidentifier
)
as
	set nocount on;
	set transaction isolation level read uncommitted;

	declare @my_user_id uniqueidentifier
	set @my_user_id = @user_id

	declare @my_role_id uniqueidentifier
	set @my_role_id = @role_id


	insert into [dbo].[aspnet_UsersInRoles]
		(
			[userid],
			[roleid]
		)
		values
		(
			@my_user_id,
			@my_role_id
		)