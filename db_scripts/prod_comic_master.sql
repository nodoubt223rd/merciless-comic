USE [master]
GO
/****** Object:  Database [comicMaster]    Script Date: 11/06/2013 11:08:48 ******/
CREATE DATABASE [comicMaster] ON  PRIMARY 
( NAME = N'comicMaster', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.BRETTTEST\MSSQL\DATA\comicMaster.mdf' , SIZE = 2048KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'comicMaster_log', FILENAME = N'c:\Program Files\Microsoft SQL Server\MSSQL10_50.BRETTTEST\MSSQL\DATA\comicMaster_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [comicMaster] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [comicMaster].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [comicMaster] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [comicMaster] SET ANSI_NULLS OFF
GO
ALTER DATABASE [comicMaster] SET ANSI_PADDING OFF
GO
ALTER DATABASE [comicMaster] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [comicMaster] SET ARITHABORT OFF
GO
ALTER DATABASE [comicMaster] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [comicMaster] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [comicMaster] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [comicMaster] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [comicMaster] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [comicMaster] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [comicMaster] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [comicMaster] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [comicMaster] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [comicMaster] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [comicMaster] SET  DISABLE_BROKER
GO
ALTER DATABASE [comicMaster] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [comicMaster] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [comicMaster] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [comicMaster] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [comicMaster] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [comicMaster] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [comicMaster] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [comicMaster] SET  READ_WRITE
GO
ALTER DATABASE [comicMaster] SET RECOVERY FULL
GO
ALTER DATABASE [comicMaster] SET  MULTI_USER
GO
ALTER DATABASE [comicMaster] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [comicMaster] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'comicMaster', N'ON'
GO
USE [comicMaster]
GO
/****** Object:  Table [dbo].[Chapter]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Chapter](
	[ChapterID] [int] IDENTITY(1,1) NOT NULL,
	[ChapterTitle] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_Chapter] PRIMARY KEY CLUSTERED 
(
	[ChapterID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cast]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cast](
	[CastID] [int] IDENTITY(1,1) NOT NULL,
	[Bio] [ntext] NULL,
	[Height] [int] NULL,
	[Weight] [int] NULL,
	[BloodType] [nchar](10) NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](200) NOT NULL,
 CONSTRAINT [PK_Cast] PRIMARY KEY CLUSTERED 
(
	[CastID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Author]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Author](
	[AuthorID] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](150) NOT NULL,
	[LastName] [nvarchar](150) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](8) NOT NULL,
	[Bio] [ntext] NULL,
	[Email] [nvarchar](250) NOT NULL,
 CONSTRAINT [PK_Author] PRIMARY KEY CLUSTERED 
(
	[AuthorID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Comic]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comic](
	[ComicID] [int] IDENTITY(1,1) NOT NULL,
	[AuthorID] [int] NOT NULL,
	[ChapterID] [int] NOT NULL,
	[BookID] [int] NOT NULL,
	[Title] [nvarchar](300) NOT NULL,
	[LongDescription] [ntext] NOT NULL,
	[ShortDescription] [ntext] NOT NULL,
	[ComicImage] [nvarchar](150) NOT NULL,
	[ThumbImage] [nvarchar](150) NOT NULL,
	[BannerImage] [nvarchar](150) NULL,
	[IsArchived] [bit] NOT NULL,
	[IsLastComic] [bit] NOT NULL,
	[OnHomePage] [bit] NOT NULL,
	[ComicDate] [datetime] NOT NULL,
 CONSTRAINT [PK_Comic] PRIMARY KEY CLUSTERED 
(
	[ComicID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transcript]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transcript](
	[TranscriptId] [int] IDENTITY(1,1) NOT NULL,
	[ScriptText] [ntext] NOT NULL,
	[ParentComic] [int] NOT NULL,
	[AuthorId] [int] NOT NULL,
 CONSTRAINT [PK_Transcript] PRIMARY KEY CLUSTERED 
(
	[TranscriptId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[News]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[NewsId] [int] NOT NULL,
	[ComicId] [int] NOT NULL,
	[Title] [nvarchar](250) NOT NULL,
	[Text] [ntext] NOT NULL,
	[NewsDate] [datetime] NOT NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ComicCast]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComicCast](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[CastID] [int] NOT NULL,
	[ComicID] [int] NOT NULL,
 CONSTRAINT [PK_ComicCast] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Book]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Book](
	[BookID] [int] IDENTITY(1,1) NOT NULL,
	[BookTitle] [nvarchar](300) NOT NULL,
 CONSTRAINT [PK_Book] PRIMARY KEY CLUSTERED 
(
	[BookID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookChapter]    Script Date: 11/06/2013 11:08:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookChapter](
	[RowID] [int] IDENTITY(1,1) NOT NULL,
	[BookID] [int] NOT NULL,
	[ChapterID] [int] NOT NULL,
 CONSTRAINT [PK_BookChapter] PRIMARY KEY CLUSTERED 
(
	[RowID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Default [DF_Comic_ComicDate]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[Comic] ADD  CONSTRAINT [DF_Comic_ComicDate]  DEFAULT (getdate()) FOR [ComicDate]
GO
/****** Object:  ForeignKey [FK_Comic_Author]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[Comic]  WITH CHECK ADD  CONSTRAINT [FK_Comic_Author] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Author] ([AuthorID])
GO
ALTER TABLE [dbo].[Comic] CHECK CONSTRAINT [FK_Comic_Author]
GO
/****** Object:  ForeignKey [FK_Comic_Chapter]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[Comic]  WITH CHECK ADD  CONSTRAINT [FK_Comic_Chapter] FOREIGN KEY([ChapterID])
REFERENCES [dbo].[Chapter] ([ChapterID])
GO
ALTER TABLE [dbo].[Comic] CHECK CONSTRAINT [FK_Comic_Chapter]
GO
/****** Object:  ForeignKey [FK_Transcript_Comic]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[Transcript]  WITH CHECK ADD  CONSTRAINT [FK_Transcript_Comic] FOREIGN KEY([ParentComic])
REFERENCES [dbo].[Comic] ([ComicID])
GO
ALTER TABLE [dbo].[Transcript] CHECK CONSTRAINT [FK_Transcript_Comic]
GO
/****** Object:  ForeignKey [FK_News_Comic]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[News]  WITH CHECK ADD  CONSTRAINT [FK_News_Comic] FOREIGN KEY([ComicId])
REFERENCES [dbo].[Comic] ([ComicID])
GO
ALTER TABLE [dbo].[News] CHECK CONSTRAINT [FK_News_Comic]
GO
/****** Object:  ForeignKey [FK_ComicCast_Cast]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[ComicCast]  WITH CHECK ADD  CONSTRAINT [FK_ComicCast_Cast] FOREIGN KEY([CastID])
REFERENCES [dbo].[Cast] ([CastID])
GO
ALTER TABLE [dbo].[ComicCast] CHECK CONSTRAINT [FK_ComicCast_Cast]
GO
/****** Object:  ForeignKey [FK_ComicCast_Comic]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[ComicCast]  WITH CHECK ADD  CONSTRAINT [FK_ComicCast_Comic] FOREIGN KEY([ComicID])
REFERENCES [dbo].[Comic] ([ComicID])
GO
ALTER TABLE [dbo].[ComicCast] CHECK CONSTRAINT [FK_ComicCast_Comic]
GO
/****** Object:  ForeignKey [FK_Book_Comic]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Comic] FOREIGN KEY([BookID])
REFERENCES [dbo].[Comic] ([ComicID])
GO
ALTER TABLE [dbo].[Book] CHECK CONSTRAINT [FK_Book_Comic]
GO
/****** Object:  ForeignKey [FK_BookChapter_Book]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[BookChapter]  WITH CHECK ADD  CONSTRAINT [FK_BookChapter_Book] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([BookID])
GO
ALTER TABLE [dbo].[BookChapter] CHECK CONSTRAINT [FK_BookChapter_Book]
GO
/****** Object:  ForeignKey [FK_BookChapter_Chapter]    Script Date: 11/06/2013 11:08:49 ******/
ALTER TABLE [dbo].[BookChapter]  WITH CHECK ADD  CONSTRAINT [FK_BookChapter_Chapter] FOREIGN KEY([ChapterID])
REFERENCES [dbo].[Chapter] ([ChapterID])
GO
ALTER TABLE [dbo].[BookChapter] CHECK CONSTRAINT [FK_BookChapter_Chapter]
GO
