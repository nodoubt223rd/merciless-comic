use[dev_comic_master]
go
if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'cp_add_new_author')
	drop procedure [dbo].[cp_add_new_author]
go
create procedure [dbo].[cp_add_new_author]
(
	@application_id uniqueidentifier,
	@application_name nvarchar(256),
	@lowered_application_name nvarchar(256),
	@avatar nvarchar(256)
)
as
	set nocount on;
	set transaction isolation level read uncommitted;

	declare @authorId int

	declare @my_application_id uniqueidentifier
	set @my_application_id = @application_id

	declare @my_application_name nvarchar(256)
	set @my_application_name = @application_name

	declare @my_lowered_application_name nvarchar(256)
	set @my_lowered_application_name = @lowered_application_name

	declare @my_avatar nvarchar(256)
	set @my_avatar = @avatar


	insert into [dbo].[Author]
		(
			[applicationid],
			[applicationname],
			[loweredapplicationname],
			[avatar]
		)
		values
		(
			@my_application_id,
			@my_application_name,
			@my_lowered_application_name,
			@my_avatar
		)

		set @authorId = @@identity
		select @authorId