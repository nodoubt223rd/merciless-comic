use [tctmdcms400stage];
go

if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'tct_oauth_token_remove')
    drop procedure [dbo].[tct_oauth_token_remove]
go

if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'tct_oauth_token_get')
    drop procedure [dbo].[tct_oauth_token_get]
go

if exists (select * from information_schema.routines where [routine_schema] = 'dbo' and [routine_name] = 'tct_oauth_token_add')
    drop procedure [dbo].[tct_oauth_token_add]
go

if exists (select * from information_schema.tables where [table_schema] = 'dbo' and [table_name] = 'tct_oauth_token')
    drop table [dbo].[tct_oauth_token]
go

create table [dbo].[tct_oauth_token]
(
    [user_id] bigint not null constraint [FK_tct_oauth_token_users] foreign key references [dbo].[users] ([user_id]) on delete cascade,
    [social_media_type] nvarchar(64) not null,
    [access_token] nvarchar(64) not null,
    [token_secret] nvarchar(64) not null,
    [verifier_token] nvarchar(64) not null,
    [modified] datetime not null,
    constraint [PK_tct_oauth_token] primary key clustered ([user_id], [social_media_type])
) on [primary]
go

create procedure [dbo].[tct_oauth_token_add]
(
    @user_id bigint,
    @social_media_type nvarchar(64),
    @access_token nvarchar(64),
    @token_secret nvarchar(64),
    @verifier_token nvarchar(64)
)
as
    set nocount on;
    set transaction isolation level read uncommitted;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)

    set @my_user_id = @user_id
    set @my_social_media_type = @social_media_type

    if exists (select [user_id] from [dbo].[tct_oauth_token] (nolock) where [user_id] = @user_id and [social_media_type] = @my_social_media_type)
        update [dbo].[tct_oauth_token]
        set
            [access_token] = @access_token,
            [token_secret] = @token_secret,
            [verifier_token] = @verifier_token,
            [modified] = getdate()
        where [user_id] = @user_id and [social_media_type] = @my_social_media_type
    else
        insert into [dbo].[tct_oauth_token]
        (
            [user_id],
            [social_media_type],
            [access_token],
            [token_secret],
            [verifier_token],
            [modified]
        )
        values
        (
            @user_id,
            @social_media_type,
            @access_token,
            @token_secret,
            @verifier_token,
            getdate()
        )
go

create procedure [dbo].[tct_oauth_token_get]
(
    @user_id bigint,
    @social_media_type nvarchar(64)
)
as
    set nocount on;
    set transaction isolation level read uncommitted;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)
    
    -- Brett Murphy hotfix 10/25/2013    
    set @my_user_id = @user_id
    set @my_social_media_type = @social_media_type

    select
        [user_id],
        [social_media_type],
        [access_token],
        [token_secret],
        [verifier_token],
        [modified]
    from [dbo].[tct_oauth_token] (nolock)
    where [user_id] = @my_user_id and [social_media_type] = @my_social_media_type
go

create procedure [dbo].[tct_oauth_token_remove]
(
    @user_id bigint,
    @social_media_type nvarchar(64)
)
as
    set nocount on;

    declare
        @my_user_id bigint,
        @my_social_media_type nvarchar(64)

    delete from [dbo].[tct_oauth_token]
    where [user_id] = @my_user_id and [social_media_type] = @my_social_media_type
go